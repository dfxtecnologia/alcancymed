    </section>
    <div class="modal fade" id="modal-alcancy-med" tabindex="-1" role="dialog" aria-labelledby="modal-alcancy-medTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-alcancy-medTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <a href="#" class="btn btn-primary">Ação</a>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
  <footer>
    <!-- <a href="<?=base_url()?>">
      <img src="<?=base_url()?>/assets/images/logo.png" alt="AlcancyMED" class="logo">
    </a> -->
    <ul>
      <li>
        <h4>Atendimento</h4>
        <ul>
          <li>suporte@alcancymed.com.br</li>
        </ul>
      </li>
      <!-- <li>
        <h4>Serviços</h4>
        <ul>
          <li><a href="#">PACIENTE</a></li>
          <li><a href="#">MÉDICO</a></li>
        </ul>
      </li> -->
      <li>
        <h4>Legal</h4>
        <ul>
          <li><a href="<?=base_url()?>/politica-privacidade.php"">Política de Privacidade</a></li>
          <li><a href="<?=base_url()?>/politica-cancelamento.php">Política de Cancelamento</a></li>
          <li><a href="<?=base_url()?>/termo-uso.php">Termos de Uso</a></li>
        </ul>
      </li>
    </ul>
  </footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js" charset="UTF-8"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.3/cropper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
  <script src="<?=base_url()?>/assets/js/select2.pt-BR.js" charset="UTF-8"></script>
  <script src="<?=base_url()?>/assets/js/serializefiles.js"></script>
  <script src="<?=base_url()?>/assets/js/mascaras.js"></script>
  <script src="<?=base_url()?>/assets/js/forms.js"></script>
  <script src="<?=base_url()?>/assets/js/select2.js"></script>
  <script src="<?=base_url()?>/assets/js/receitas.js"></script>
  <script src="<?=base_url()?>/assets/js/validar.js"></script>
  <script src="<?=base_url()?>/assets/js/files.js"></script>
  <script src="<?=base_url()?>/assets/js/cep.js"></script>
  <script src="<?=base_url()?>/assets/js/visualizar-documentos-medico.js"></script>
  <script src="<?=base_url()?>/assets/js/assinatura-digital.js"></script>
  <script src="<?=base_url()?>/assets/js/site.js"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129416046-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-129416046-1');
  </script>

</body>
</html>
