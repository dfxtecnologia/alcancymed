<?php
require(dirname(__DIR__).'/sys/functions.php');
require(dirname(__DIR__).'/qrcode/qrlib.php');
require('pdf_rotate.php');

class PDF extends PDF_Rotate {
  private $documento;

  function __construct($orientation='P', $unit='mm', $size='A4', $documento) {
    $this->documento = $documento;
    parent::__construct($orientation, $unit, $size);
  }

  function RotatedText($x,$y,$txt,$angle) {
      $this->Rotate($angle,$x,$y);
      $this->Text($x,$y,$txt);
      $this->Rotate(0);
  }

  private function QRCode() {
    $header = [
      'alg' => 'HS256',
      'typ' => 'JWT'
    ];

    $header = json_encode($header);
    $header = base64_encode($header);

    $tipo_documento = '';
    if (isSet($this->documento->tipo)) {
      if ($this->documento->tipo == 'C') {
        $tipo_documento = 'R';
      } else if ($this->documento->tipo == 'S') {
        $tipo_documento = 'R';
      } else {
        $tipo_documento = 'A';
      }
    }

    $payload = [
      'medico'         => $this->documento->medico_nome,
      'paciente'       => $this->documento->paciente_nome,
      'data'           => date('d/m/Y', strtotime($this->documento->data)),
      'tipo'           => $this->documento->tipo,
      'tipo_documento' => $tipo_documento,
      'chave'          => $this->documento->chave,
    ];
    $payload = json_encode($payload);
    $payload = base64_encode($payload);
    
    $signature = hash_hmac('sha256',"$header.$payload",'minha-senha',true);
    $signature = base64_encode($signature);

    if ($tipo_documento == 'R') {
      QRcode::png(base_url().'/painel/receitas/pdf.php?chave='.$this->documento->chave.'&tipo='.$this->documento->tipo.'&_='.substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, 8), dirname(__DIR__).'/arquivos/qr-'.$this->documento->id.'.png', QR_ECLEVEL_L, 2);
    } else {
      QRcode::png(base_url().'/painel/atestados/pdf.php?chave='.$this->documento->chave.'&tipo='.$this->documento->tipo.'&_='.substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, 8), dirname(__DIR__).'/arquivos/qr-'.$this->documento->id.'.png', QR_ECLEVEL_L, 2);
    }  }

  function Header() {    
    $this->QRCode();
    if ($this->documento->consultorio_logo == '') {
      $this->Image(dirname(__DIR__).'/assets/images/logo-medicina.png', 5, 10, -500);
    } else {
      $this->Image(get_thumbnail_s3($this->documento->consultorio_logo), 5, 10);
    }
    $this->SetFillColor(0, 196, 189);
    $this->Rect(50, 10, 0.5, 20, 'F');
    $this->Image(dirname(__DIR__).'/arquivos/qr-'.$this->documento->id.'.png',110,5);
    $this->Image(dirname(__DIR__).'/assets/images/logo.png',150,5,-1600);
    $this->SetFont('Arial','',8);
    $this->Text(160, 19, date('d/m/Y', strtotime($this->documento->data)));
    $this->SetTextColor(255, 255, 255);
    $this->SetFillColor(0, 0, 0);
    $this->SetY(20);
    $this->SetX(135);
    $this->Cell(65, 5, $this->documento->chave, 0, 0, 'C', true);
    $this->SetTextColor(0, 0, 0);
    $this->Text(150, 28, 'Verifique a autenticiade no site');
    $this->SetY(28);
    $this->SetX(149);
    $this->Write(5, 'http://www.alcancymed.com.br', 'http://www.alcancymed.com.br');
    $this->SetTextColor(0, 196, 189);
    $this->SetY(45);
    $this->SetFont('Arial','B',12);
    if (isSet($this->documento->tipo)) {
      if ($this->documento->tipo == 'C') {
        $this->SetTitle(utf8_decode('RECEITUÁRIO DE CONTROLE ESPECIAL'));
        $this->Cell(190, 5, utf8_decode('RECEITUÁRIO DE CONTROLE ESPECIAL'), 0, 0, 'C');
      } else if ($this->documento->tipo == 'S') {
        $this->SetTitle(utf8_decode('RECEITUÁRIO MÉDICO'));
        $this->Cell(190, 5, utf8_decode('RECEITUÁRIO MÉDICO'), 0, 0, 'C');
      } else {
        $this->SetTitle(utf8_decode('ATESTADO MÉDICO'));
        $this->Cell(190, 5, utf8_decode('ATESTADO MÉDICO'), 0, 0, 'C');
      }
    }

    if (!$this->documento->ativo) {
      $this->SetFont('Arial','B',40);
      $this->SetTextColor(255,192,203);
      $this->RotatedText(35,190,'DOCUMENTO CANCELADO',45);
    }
  }

  function Footer() {    
    if (isSet($this->documento->tipo) && ($this->documento->tipo == 'C')) {
      $this->Rect(10, 220, 90, 40);
      $this->SetFont('Arial','B',8);
      $this->Text(25, 224, utf8_decode('IDENTIFICAÇÃO DO COMPRADOR'));
      $this->Line(10, 227, 100, 227);
      $this->SetLineWidth(0.1);
      $this->SetDrawColor(208, 208, 208);
      $this->Text(11, 232, utf8_decode('Nome'));
      $this->Line(20, 233, 99, 233);
      $this->Line(11, 238, 99, 238);
      $this->Text(11, 242, utf8_decode('Identificação'));
      $this->Line(29, 243, 69, 243);
      $this->Text(70, 242, utf8_decode('Orgão Emissor'));
      $this->Line(91, 243, 99, 243);
      $this->Text(11, 247, utf8_decode('Endereço'));
      $this->Line(25, 248, 99, 248);
      $this->Text(11, 252, utf8_decode('Cidade'));
      $this->Line(22, 253, 69, 253);
      $this->Text(70, 252, utf8_decode('UF'));
      $this->Line(74, 253, 99, 253);
      $this->Text(11, 257, utf8_decode('Telefone'));
      $this->Line(23, 258, 99, 258);

      $this->SetDrawColor(0, 0, 0);
      $this->Rect(110, 220, 90, 40);
      $this->Text(135, 224, utf8_decode('IDENTIFICAÇÃO DO FORNECEDOR'));
      $this->Line(110, 227, 200, 227);
      $this->Line(112, 253, 175, 253);
      $this->Text(120, 257, utf8_decode('ASSINATURA DO FARMACÊUTICO'));
      $this->Line(178, 253, 197, 253);
      $this->Text(184, 257, utf8_decode('DATA'));
  
      if ($this->documento->medico_assinatura_digital != '') {
        $this->Image($this->documento->medico_assinatura_digital, 110, 180);
      }
      
      $this->SetY(200);
      $this->SetX(120);
      $this->Cell(70, 5, utf8_decode($this->documento->medico_nome), 'T', 0, 'C');
      $this->SetY(205);
      $this->SetX(120);
      $this->Cell(70, 5, utf8_decode('CRM '. $this->documento->medico_crm), 0, 0, 'C');

      if ($this->documento->medico_assinatura_digital != '') {
        $this->SetY(210);
        $this->SetX(120);
        $this->Cell(70, 5, 'assinado digitalmente', 0, 0, 'C');
      }
    } else {
      if ($this->documento->medico_assinatura_digital != '') {
        $this->Image($this->documento->medico_assinatura_digital, 110, 230);
      }

      $this->SetY(250);
      $this->SetX(120);
      $this->Cell(70, 5, utf8_decode($this->documento->medico_nome), 'T', 0, 'C');
      $this->SetY(255);
      $this->SetX(120);
      $this->Cell(70, 5, utf8_decode('CRM '. $this->documento->medico_crm), 0, 0, 'C');

      if ($this->documento->medico_assinatura_digital != '') {
        $this->SetY(260);
        $this->SetX(120);
        $this->Cell(70, 5, 'assinado digitalmente', 0, 0, 'C');
      }
    }

    if (isSet($this->documento->tipo) && ($this->documento->tipo != 'C') && ($this->documento->tipo != 'S'))  { 
      $this->SetY(260);
      $this->SetX(9);
      $this->SetFont('Arial','',8);
      $this->Cell(70, 5, utf8_decode('Autorizo a divulgação do diagnóstico (CID) listado neste documento.'));
    }

    $this->SetFont('Arial','',8);
    $this->SetFillColor(0, 196, 189);
    $this->Rect(10, 265, 190, 0.5, 'F');
    $this->SetFont('Arial','',8);
    $this->Text(10, 270, utf8_decode($this->documento->medico_nome));
    $this->Text(10, 275, $this->documento->medico_email);
    $this->Text(10, 280, 'CPF '. $this->documento->medico_cpf);
    $this->Text(10, 285, 'CRM '. $this->documento->medico_crm);
    $this->Text(60, 270, utf8_decode($this->documento->consultorio_logradouro .', '. $this->documento->consultorio_numero));
    $this->Text(60, 275, utf8_decode($this->documento->consultorio_bairro. ' - '. $this->documento->consultorio_cidade.'/'.$this->documento->consultorio_estado));
    $this->Text(60, 280, $this->documento->consultorio_cep);
    $this->Text(60, 285, $this->documento->consultorio_telefone);
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
  }
}