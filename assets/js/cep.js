(function () {
  "use strict";
  function limpa_formulário_cep() {
    $('.busca-cep-rua').val('');
    $('.busca-cep-bairro').val('');
    $('.busca-cep-cidade').val('');
    $('.busca-cep-uf').val('');
  }

  $('.busca-cep').blur(function (e) {
    e.preventDefault();
    var cep = $(this).val().replace(/\D/g, '');

    if (cep != '') {
      var validacep = /^[0-9]{8}$/;

      if (validacep.test(cep)) {
        $.getJSON('https://viacep.com.br/ws/' + cep + '/json/?callback=?', function (dados) {
          if (!('erro' in dados)) {
            $('.busca-cep-rua').val(dados.logradouro);
            $('.busca-cep-bairro').val(dados.bairro);
            // $('.busca-cep-cidade').val($('.busca-cep-cidade').find('option').filter(function () { return $.trim($(this).text()) === dados.localidade; }).val());
            // $('.busca-cep-cidade').trigger('change');

            var option = new Option(dados.localidade, dados.ibge, true, true);
            $('.busca-cep-cidade').append(option).trigger('change');
            $('.busca-cep-cidade').trigger({
              type: 'select2:select',
              params: {
                data: {
                  "id": dados.ibge, "text": dados.localidade, "descricao": dados.localidade
                }
              }
            });

            $('.busca-cep-estado').val(dados.uf); 
            $('.busca-cep-estado').trigger('change')
            // console.log(dados);
          } else {
            limpa_formulário_cep();
            alert('CEP não encontrado.');
          }
        });
      } else {
        limpa_formulário_cep();
        alert('Formato de CEP inválido.');
      }
    } else {
      limpa_formulário_cep();
    }
  });
})(jQuery);