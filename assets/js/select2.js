(function () {
  "use strict";
  
  $('.select2-basic').select2({
    language: "pt-BR",
    theme: 'bootstrap'
  });

  $('.select2-basic-search').select2({
    language: "pt-BR",
    theme: 'bootstrap',
    ajax: {
      url: $('.select2-basic-search').data('search-url'),
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          search: params.term
        };
      },
      processResults: function (data, params) {
        return {
          results: data
        };
      },
      cache: true
    },
    placeholder: $('.select2-form-search').attr('placeholder'),
    minimumInputLength: 3
  });

  $('.select2-form-search').select2({
    language: "pt-BR",
    theme: 'bootstrap',    
    ajax: {
      url: $('.select2-form-search').data('search-url'),
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          search: params.term
        };
      },
      processResults: function (data, params) {
        return {
          results: data
        };
      },
      cache: true
    },
    placeholder: $('.select2-form-search').attr('placeholder'),
    escapeMarkup: function (markup) { return markup; },
    minimumInputLength: 3,
    templateResult: formatSelect2,
    templateSelection: formatSelect2Selection    
  });


  function formatSelect2(data) {
    if (data.loading) {
      return data.text;
    }

    var markup = "<div class='select2-result clearfix'>" +
      "<div class='select2'>" +
      "<div class='select2'>" + data.descricao + "</div>";

    if (data.apresentacao) {
      markup += "<div class='select2-result'><small style='color: #cecece'>" + data.apresentacao + "</small></div>";
    }

    markup += "</div></div>";

    return markup;
  }

  function formatSelect2Selection(data) {
    return data.descricao;
  }

})(jQuery);
