(function () {
  "use strict";

  $('#btn-adicionar-receita-medicamento').click(function (e) {
    e.preventDefault();

    var addMedicamento = true;

    var $form = $('#btn-adicionar-receita-medicamento').closest('form');
    var $table = $('#table-receita-medicamentos');
    var $tbody = $table.children('tbody');

    var medicamento = $form.find("#paciente-medicamento").select2('data');
    var quantidade = $form.find("#paciente-quantidade").val();
    var orientacao = $form.find("#paciente-orientacoes").val();

    $('span.select2').removeClass('is-invalid');
    $form.find('#paciente-quantidade').removeClass('is-invalid');
    $form.find('#paciente-orientacoes').removeClass('is-invalid');
  
    if (medicamento.length === 0) {
      $('span.select2').addClass('is-invalid');
      addMedicamento = false;
    }

    if (quantidade === '') {
      $form.find('#paciente-quantidade').addClass('is-invalid');
      addMedicamento = false;
    }

    if (orientacao === '') {
      $form.find('#paciente-orientacoes').addClass('is-invalid');
      addMedicamento = false;
    }

    if (!addMedicamento) {
      return;      
    }

    $('#receita-nenhum-medicamento').remove();

    var table_row = '';

    table_row += '<tr>';
    table_row += '  <input type="hidden" name="medicamento_id[]" value="' + medicamento[0].id + '">';
    table_row += '  <input type="hidden" name="quantidade[]" value="' + quantidade + '">';
    table_row += '  <input type="hidden" name="forma_uso[]" value="' + orientacao + '">';
    table_row += '  <td class="d-none d-sm-block text-center">' + ($('tr', $tbody).length + 1) + '</td>';
    table_row += '  <td>' + medicamento[0].descricao + ' <small style="color: #a2a2a2">' + medicamento[0].apresentacao + '</small></td>';
    table_row += '  <td class="text-center">' + quantidade + '</td>';
    table_row += '  <td>' + orientacao + '</td>';
    table_row += '  <td><a href="#" class="btn-remover-receita-medicamento"><i class="far fa-trash-alt"></i></a></td>';
    table_row += '</tr>';

    $tbody.append(table_row);

    $form.find("#paciente-medicamento").val(null).trigger('change');
    $form.find("#paciente-quantidade").val('');
    $form.find("#paciente-orientacoes").val('');

    $(this).tooltip('hide');
  });

  $('body').on('click', '.btn-remover-receita-medicamento', function (e) {
    e.preventDefault();
    $(this).tooltip('hide');
    $(this).closest('tr').remove();
  });

  $('.receita-detalhes').click(function (e) {
    e.preventDefault();
    const $icon = $(this).find('i');
    const $tr = $(this).closest('tr');
    const $tbody = $(this).closest('tbody');
    
    if ($icon.hasClass('fa-plus-square')) {

      $.ajax({
        type: 'POST',
        url: $(this).attr('href'),
        data: { id: $(this).data('id') },
        beforeSend: function () {
          $('#overlay').css('display', 'block');
        }
      })
      .done(function (data) {
        $('#overlay').css('display', 'none');
        var table_row = '';

        table_row += '<tr class="receita-detalhes-row">';
        table_row += '  <td></td>';
        table_row += '  <td colspan="5">';
        table_row += '    <table class="table table-sm table-borderless">';
        table_row += '      <thead>';
        table_row += '        <th>Medicamento</th>';
        table_row += '        <th>Quantidade</th>';
        table_row += '        <th>Forma de Uso</th>';
        table_row += '      </thead>';
        table_row += '      <tbody>';
        
        $.each(data, function (index, medicamento) {
          table_row += '<tr>';
          table_row += '  <td>' + medicamento.nome_comercial + '</td>';
          table_row += '  <td>' + medicamento.quantidade + '</td>';
          table_row += '  <td>' + medicamento.forma_uso + '</td>';
          table_row += '</tr>';
        });
        
        table_row += '      </tbody>';
        table_row += '    </table>';
        table_row += '  </td>';
        table_row += '</tr>';

        $(table_row).insertAfter($tr)        
      });            

      $icon.removeClass('fa-plus-square');
      $icon.addClass('fa-minus-square');
    } else {
      const $table_row = $tr.next('tr.receita-detalhes-row');
      $icon.addClass('fa-plus-square');
      $icon.removeClass('fa-minus-square');
      $table_row.remove();
    }
  });

  $('.documento-cancelar').click(function (e) {
    e.preventDefault();
    const $tr = $(this).closest('tr');

    $.ajax({
      type: 'POST',
      url: $(this).attr('href'),
      data: { id: $(this).data('id') },
      beforeSend: function () {
        $('#overlay').css('display', 'block');
      }
    })
    .done(function (data) {
      $('#overlay').css('display', 'none');
      $tr.find("a.documento-cancelar").detach();
    });
  });

  $('#modelo-receita').change(function (e) {
    e.preventDefault();

    $.ajax({
      type: 'POST',
      url: $(this).data('search-url'),
      data: { id: $("#modelo-receita option:selected").val() },
      beforeSend: function () {
        $('#overlay').css('display', 'block');
      }
    })
      .done(function (data) {
        $('#overlay').css('display', 'none');

        const $table = $('#table-receita-medicamentos');
        const $tbody = $table.children('tbody');
        
        $('#receita-nenhum-medicamento').remove();
        
        $.each(data, function (i, item) {
          var table_row = '';
          table_row += '<tr>';
          table_row += '  <input type="hidden" name="medicamento_id[]" value="' + item.id + '">';
          table_row += '  <input type="hidden" name="quantidade[]" value="' + item.quantidade + '">';
          table_row += '  <input type="hidden" name="forma_uso[]" value="' + item.forma_uso + '">';
          table_row += '  <td class="d-none d-sm-block text-center">' + ($('tr', $tbody).length + 1) + '</td>';
          table_row += '  <td>' + item.nome_comercial + ' <small style="color: #a2a2a2">' + item.apresentacao + '</small></td>';
          table_row += '  <td class="text-center">' + item.quantidade + '</td>';
          table_row += '  <td>' + item.forma_uso + '</td>';
          table_row += '  <td><a href="#" class="btn-remover-receita-medicamento"><i class="far fa-trash-alt"></i></a></td>';
          table_row += '</tr>';
  
          $tbody.append(table_row);
        });
      });    
  });

})(jQuery);
