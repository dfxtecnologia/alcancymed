(function () {
  "use strict";
  $('body').on('click', '#btn-validar-documento', function (e) {
    e.preventDefault();
    $.ajax({
      type: 'POST',
      url: $(this).data('link'),
      data: { chave: $('#chave-validacao-documento').val() },
      beforeSend: function () {
        $('#overlay').css('display', 'block');
      }
    })
    .done(function (data) {
      $('#overlay').css('display', 'none');
      if (data.status == 'OK') {       
        var modalbody = '';
        modalbody += '<dl class="row">';
        modalbody += '  <dt class="col-sm-3">Emissão</dt>';
        modalbody += '  <dd class="col-sm-9 emissao-validacao"></dd>';

        modalbody += '  <dt class="col-sm-3">Médico</dt>';
        modalbody += '  <dd class="col-sm-9 medico-validacao"></dd>';

        modalbody += '  <dt class="col-sm-3">Paciente</dt>';
        modalbody += '  <dd class="col-sm-9 paciente-validacao"></dd>';

        modalbody += '  <dt class="col-sm-3">Status</dt>';
        modalbody += '  <dd class="col-sm-9 status-validacao"></dd>';
        modalbody += '</dl>';

        $('#modal-validacao-documento .modal-body').html(modalbody);
        $('#modal-validacao-documento .modal-title').text(data.tipo);
        $('#modal-validacao-documento .emissao-validacao').text(data.emissao);
        $('#modal-validacao-documento .medico-validacao').text(`${data.crm} - ${data.medico}`);
        $('#modal-validacao-documento .paciente-validacao').text(`${data.cpf} - ${data.paciente}`);        
        $('#modal-validacao-documento .status-validacao').text(`${data.status_documento}`);
        if (data.status_documento == 'Cancelado') {
          $('#modal-validacao-documento .status-validacao').removeClass('text-success');
          $('#modal-validacao-documento .status-validacao').addClass('text-danger');
        } else {
          $('#modal-validacao-documento .status-validacao').removeClass('text-danger');
          $('#modal-validacao-documento .status-validacao').addClass('text-success');
        }

        $('#modal-validacao-documento .modal-footer .btn-success').attr('href', $('#modal-validacao-documento .modal-footer .btn-success').attr('href') + data.tipo.toLowerCase() + 's/pdf.php?chave=' + data.chave + '&tipo=' + data.tipo_documento + '&_=' + Math.random().toString(36).substring(7));
        $('#modal-validacao-documento .modal-footer .btn-success').addClass('d-inline');
        $('#modal-validacao-documento').modal('show');
      } else {
        $('#modal-validacao-documento .modal-title').text('Validar Documento');
        $('#modal-validacao-documento .modal-body').text('Chave informada não pertence a nenhum documento emitido pelo AlcancyMED');
        $('#modal-validacao-documento .modal-footer .btn-success').addClass('d-none');
        $('#modal-validacao-documento').modal('show');
      }
      $('#chave-validacao-documento').val('');
    });
  });
})(jQuery);