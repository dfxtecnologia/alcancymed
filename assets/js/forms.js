(function () {
  "use strict";
    
  $("form").submit(function (e) {
    
    if ($(this).attr('method').toUpperCase() == 'POST')  {
      e.preventDefault();
      $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: $(this).serializefiles(),
        dataType: 'json',
        encode: true,
        beforeSend: function () {
          $('#overlay').css('display', 'block');
        }
      })
      .done(function (data) {
        $('#overlay').css('display', 'none');
        if (data.status == 'OK') {
          const redirect_url = $('form').data('redirect');

          if (redirect_url !== undefined) {
            window.location.href = redirect_url;
          }

          $('form').each(function () { this.reset(); });
          $('.alert-success').html(`<strong>Sucesso!</strong> ${data.message}`);
          $('.alert-danger').addClass('d-none');
          $('.alert-success').removeClass('d-none');

          if ($("#table-receita-medicamentos").length) {
            $("#table-receita-medicamentos tbody tr").each(function () {
              this.parentNode.removeChild(this);
            });
            
            $("#table-receita-medicamentos tbody").append('<tr id="receita-nenhum-medicamento"><td colspan="5" class="text-center">Nenhum medicamento informado</td></tr>');
          }

        } else {
          $('.alert-danger').html(`<strong>Falha!</strong> ${data.message}`);
          $('.alert-danger').removeClass('d-none');
          $('.alert-success').addClass('d-none');
        }
      });
    }
  });

  $('[data-toggle="tooltip"]').tooltip();

  $("body").tooltip({
    selector: '[data-toggle="tooltip"]'
  });


  $('#paciente-cpf').blur(function (e) {
    e.preventDefault();
    const $input = $(this);

    if ($input.val() === '') { return; }

    $.ajax({
      type: 'GET',
      url: $input.data('search-url') + '?cpf=' + $input.val(),
      processData: false,
      contentType: false,
      cache: false,
      dataType: 'json',
      encode: true,
      beforeSend: function () {
        $('#overlay').css('display', 'block');
      }
    })
    .done(function (data) {
      $('#overlay').css('display', 'none');
      
      if (data.status == 'FAIL') {
        $('#paciente-cpf').removeClass('is-valid');
        $('#paciente-cpf').addClass('is-invalid');
      } else {
        $('#paciente-cpf').removeClass('is-invalid');
        $('#paciente-cpf').addClass('is-valid');
      }

      $('#paciente-nome').val(data.nome);
      $('#paciente-cep').val(data.cep);
      $('#paciente-endereco').val(data.endereco);
      $('#paciente-numero').val(data.numero);
      $('#paciente-bairro').val(data.bairro);
      $('#paciente-celular').val(data.celular);
      $('#paciente-email').val(data.email);
      $('#paciente-cidade').val(data.cidade).trigger('change');
      $('#paciente-estado').val(data.estado).trigger('change');
    });
  });

  $('.button__alcancy__close__filtro').click(function (e) {
    $(this).closest('form').find('input:text, input:password, select, textarea').val('');
    $(this).closest('form').submit();
  });
})(jQuery);
