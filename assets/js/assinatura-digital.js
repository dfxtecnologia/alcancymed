(function () {
  "use strict";

  if ($('#assinatura').length > 0) {
    const assinatura = $('#assinatura').data('image');
    var canvas       = document.querySelector("canvas");
    var signaturePad = new SignaturePad(canvas, { penColor: "rgb(66, 133, 244)" });  

    function resizeCanvas() {
      var ratio = Math.max(window.devicePixelRatio || 1, 1);
      canvas.width = canvas.offsetWidth * ratio;
      canvas.height = canvas.offsetHeight * ratio;
      canvas.getContext("2d").scale(ratio, ratio);
      signaturePad.clear();
    }

    signaturePad.fromDataURL(assinatura);

    window.addEventListener("resize", resizeCanvas);
    resizeCanvas();
  }

  function dataURLToBlob(dataURL) {
    var parts = dataURL.split(';base64,');
    var contentType = parts[0].split(":")[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;
    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) { uInt8Array[i] = raw.charCodeAt(i); }

    return new Blob([uInt8Array], { type: contentType });
  }

  $("#btn-assinatura-digital-clear").click(function (e) {
    signaturePad.clear();
  });

  $("#btn-assinatura-digital-on").click(function(e) {
    const formData = new FormData();

    formData.append('assinatura', dataURLToBlob(signaturePad.toDataURL()), 'assinatura.png');

    // Use `jQuery.ajax` method
    $.ajax($("#btn-assinatura-digital-on").data('url'), {
      type: "POST",
      enctype: 'multipart/form-data',
      data: formData,
      processData: false,
      contentType: false,
      beforeSend: function () {
        $('#overlay').css('display', 'block');
      },
      success() {
        $('#overlay').css('display', 'none');
        window.location.reload();
      },
      error() {
        $('#overlay').css('display', 'none');
      },
    }); 
  });

})(jQuery);