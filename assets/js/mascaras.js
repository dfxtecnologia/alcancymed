(function () {
  "use strict";
  var maskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
  },
  options = {
    onKeyPress: function (val, e, field, options) {
      field.mask(maskBehavior.apply({}, arguments), options);
    }
  };

  $('.telefone-mask').mask(maskBehavior, options)
  $('.cpf-mask').mask('000.000.000-00', { reverse: true });
  $('.data-mask').mask('00/00/0000');
  $('.cep-mask').mask('00000-000');
})(jQuery);  