(function () {
  "use strict";
  $('body').on('click', '.link-medicos-documentos', function (e) {
    e.preventDefault();

    let body_html = '';
    body_html += `<div class='text-center'>`;
    body_html += `<img src="${$(this).attr('href')}">`;
    body_html += `</div>`;

    $('#modal-alcancy-med .modal-title').text($(this).data('title'));
    $('#modal-alcancy-med .modal-body').html(body_html);

    if ($(this).data('role')) {
      $('#modal-alcancy-med .modal-footer a.btn').attr('href', $(this).data('url'));
      $('#modal-alcancy-med .modal-footer a.btn').addClass('btn-danger');
      $('#modal-alcancy-med .modal-footer a.btn').addClass('alcany__root__excluir__documento');
      $('#modal-alcancy-med .modal-footer a.btn').removeClass('btn-primary');
      $('#modal-alcancy-med .modal-footer a.btn').text('Recusar Documento');
    } else {
      $('#modal-alcancy-med .modal-footer').remove();
    }

    $('#modal-alcancy-med').modal('show');
  });

  $('body').on('click', '.alcany__root__excluir__documento', function (e) { 
    e.preventDefault();
    const motivo = prompt('Qual o motivo da recusa do documento?');
    const link = $(this).attr('href').split('?')[0];
    const variaveis = $(this).attr('href').split('?')[1].split('&');

    if (motivo) {
      $.ajax({
        type: 'POST',
        url: link,
        data: { medico: variaveis[0].split('=')[1], documento: variaveis[1].split('=')[1], motivo: motivo },
        beforeSend: function () {
          $('#overlay').css('display', 'block');
        }
      })
      .done(function (data) {
        $('#overlay').css('display', 'none');
        window.location.reload();
        $('#modal-alcancy-med').modal('hide');
      });
    }
  });

  $('body').on('click', '.alcancy__confirmar__documento', function (e) {
    e.preventDefault();
    const link = $(this).data('url');
    const medico = $(this).data('medico');

    $.ajax({
      type: 'POST',
      url: link,
      data: { medico: medico },
      beforeSend: function () {
        $('#overlay').css('display', 'block');
      }
    })
    .done(function (data) {
      $('#overlay').css('display', 'none');
      window.location.reload();
      $('#modal-alcancy-med').modal('hide');
    });
  });
  
})(jQuery);