(function () {
  "use strict";
  $('input[type="file"].file-style').change(function (e) {
    let filename = $(this)[0].files[0].name;
    $('input[type="file"].file-style ~ label > span').html(filename);
  });

  $('input[type="file"].documento__input__file').change(function (e) {
    e.preventDefault();
    // const $form = $(this).closest("form");

    // $.ajax({
    //   type: $($form).attr('method'),
    //   url: $($form).attr('action'),
    //   enctype: 'multipart/form-data',
    //   processData: false,
    //   contentType: false,
    //   cache: false,
    //   data: $($form).serializefiles(),
    //   dataType: 'json',
    //   encode: true,
    //   beforeSend: function () {
    //     console.log('enviar imagem');
    //     $('#overlay').css('display', 'block');
    //   }
    // })
    // .done(function (data) {
    //   console.log('enviado', data);
    //   $('#overlay').css('display', 'none');
    //   if (data.status == 'OK') {
    //     const redirect_url = $('form').data('redirect');

    //     if (redirect_url !== undefined) {
    //       window.location.href = redirect_url;
    //     }

    //     $('form').each(function () { this.reset(); });
    //     $('.alert-danger').addClass('d-none');
    //     $('.alert-success').removeClass('d-none');
    //   } else {
    //     $('.alert-danger').removeClass('d-none');
    //     $('.alert-success').addClass('d-none');
    //   }
    // });

    $(this).closest("form").submit();
    // console.log($(this).closest("form"));
  });
})(jQuery);