if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js')
    .then(function () {
      console.log('SW registered');
    });
}

(function () {
  "use strict";
  $('[data-toggle="popover"]').popover();
  $('.hamburguer').click(function (e) {
    $(this).toggleClass('open');
    $('header ul').toggleClass('open');
    $('body').toggleClass('open');
  });

  $('.alcancy__wrapper').click(function (e) {
    $('.hamburguer').removeClass('open');
    $('header ul').removeClass('open');
    $('body').removeClass('open');
  });
})(jQuery);
