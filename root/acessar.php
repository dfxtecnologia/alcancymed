<?php require_once(dirname(__DIR__).'/header/index.php') ?>
  <div class="main__wrapper">
    <div class="acessar__sistema">
      <h1>Acessar ROOT</h1>
      <?=show_alert('FAIL', 'CPF e/ou senha não são válidos.')?>
      <form action="<?=base_url()?>/root/autenticar.php" method="POST" data-redirect="<?=base_url()?>/root/index.php">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label" for="root-cpf">E-mail</label>
              <input type="email" class="form-control" name="email">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label" for="root-senha">Senha</label>
              <input type="password" class="form-control" name="senha">
            </div>
          </div>        
        </div>
        <div class="float-right">
          <button type="submit" class="btn btn-primary">Acessar</button>
        </div>
      </form>
    </div>
  </div>
<?php require_once(dirname(__DIR__).'/footer/index.php') ?>
