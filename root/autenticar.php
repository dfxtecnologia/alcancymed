<?php
  session_start();
  require_once(dirname(__DIR__).'/sys/conexao.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $validaRoot = $conexao->prepare('SELECT * FROM usuarios WHERE email = :email and senha = :senha');
    $validaRoot->bindParam(':email', $_POST['email']);
    $validaRoot->bindParam(':senha', md5($_POST['senha']));
    $validaRoot->execute();

    if ($validaRoot->rowCount() == 0) {
      echo json_encode(Array('status' => 'FAIL', 'message' => 'E-mail e/ou senha não são válidos.'));
      exit();
    }
    
    $root = $validaRoot->fetchALL(PDO::FETCH_ASSOC);
    $_SESSION['root_id'] = $root[0]['id'];
    $_SESSION['root_nome'] = $root[0]['nome'];
    $_SESSION['root_email'] = $root[0]['email'];
    echo json_encode(Array('status' => 'OK'));
  }