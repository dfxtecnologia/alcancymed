<?php
  session_start();
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();
    $conexao->beginTransaction();
    try {
      $sql  = ' UPDATE medicos SET ';
      $sql .= '  confirmacao_status = \'C\' ';
      $sql .= ' WHERE id = :id ';
      
      $atualizaMedico = $conexao->prepare($sql);
      $atualizaMedico->bindParam(':id', $_POST['medico']);
      $atualizaMedico->execute();

      $historico = $conexao->prepare('INSERT INTO documento_historicos (medico_id, acao, documento, usuario_id, motivo) VALUES (:medico_id, \'C\', \'TODOS\', :usuario_id, \'Documentos Confirmados\')');
      $historico->bindParam(':medico_id',  $_POST['medico']);
      $historico->bindParam(':usuario_id', $_SESSION['root_id']);
      $historico->execute();

      $conexao->commit();

      $email  = '';
      $email .= '<p>Parabéns,</p>';
      $email .= '<br/><br/>';
      $email .= '<p>Seus documentos foram aprovados.</p>';
      // $email .= '<p>Agora basta você confirmar a assinatura no Paypal para liberar a emissão dos atestados e receitas.</p>';
      // $email .= '<p>Acesse seu painel e entre na opção <a href="'.base_url().'/painel/medicos/meus_dados.php">Meus Dados</a> para confirmar a assinatura.</p>';
      $email .= '<br/><br/>';
      $email .= '<p>Um abraço</p>';
      $email .= '<p>Equipe AlcancyMED</p>';

      $resultset = $conexao->prepare('SELECT * FROM medicos WHERE id = :id');
      $resultset->bindParam(':id', $_POST['medico']);
      $resultset->execute();
      
      $medico = $resultset->fetch(PDO::FETCH_OBJ);
            
      $response = sendEmail($medico->email, $medico->nome, 'Documentos Confirmados AlcancyMED', $email);
      echo json_encode(Array('status' => 'OK'));
    }catch (PDOException $e) {
      $conexao->rollBack();
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }