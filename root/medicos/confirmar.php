<?php
  $conexao = Conexao::getInstance();
  $medicos = $conexao->prepare('SELECT * FROM medicos WHERE ativo = 1 and confirmado = 1 and (confirmacao_status != \'C\' or confirmacao_status is null)');
  $medicos->execute();
?>
<div class="dashboard__wrapper">
  <div class="table-responsive">
    <table class="table table-borderless table-striped datatable__alcancy">
      <thead>
        <th>Médico</th>
        <th>Selfie</th>
        <th>RG/CNH</th>
        <th>CRM - Frente</th>
        <th>CRM - Verso</th>
      </thead>
      <tbody>
      <?php while($medico = $medicos->fetch(PDO::FETCH_OBJ)) { ?>
        <tr>
          <td>
            <p><?=$medico->nome?></p>
            <?php if (($medico->file_selfie != '') && ($medico->file_cnh != '') && ($medico->file_cpf != '') && ($medico->file_crm != '') &&
                      ($medico->motivo_selfie == '') && ($medico->motivo_cnh == '') && ($medico->motivo_cpf == '') && ($medico->motivo_crm == '')) { ?>
              <button class="btn btn-outline-success alcancy__confirmar__documento" data-url="<?=base_url()?>/root/medicos/confirmar_documentos.php" data-medico="<?=$medico->id?>">Confirmar Documentos</button>
            <?php } ?>
          </td>
          <td>
            <?php if ($medico->file_selfie != '') { ?>
              <a href="<?=$medico->file_selfie?>" data-title="SELFIE" class="text-center d-block link-medicos-documentos" data-role="admin" data-url="<?=base_url()?>/root/medicos/remover_documento.php?medico=<?=$medico->id?>&tipo=selfie" target="_blank">
                <img src="<?=get_thumbnail_s3($medico->file_selfie)?>">
              </a>
              <?php if ($medico->motivo_selfie != '') { ?>
                <span class="alcancy__alerta alcancy__alerta__top mt-3">
                  <?=$medico->motivo_selfie?>
                </span>
              <?php } ?>
            <?php } else { ?>
              <div class="text-center alert alert-warning" role="alert">
                Documento não enviado
              </div>
            <?php }  ?>
          </td>
          <td>
            <?php if ($medico->file_cnh != '') { ?>
              <a href="<?=$medico->file_cnh?>" data-title="RG/CNH" class="text-center d-block link-medicos-documentos" data-role="admin" data-url="<?=base_url()?>/root/medicos/remover_documento.php?medico=<?=$medico->id?>&tipo=cnh" target="_blank">
                <img src="<?=get_thumbnail_s3($medico->file_cnh)?>">
              </a>
              <?php if ($medico->motivo_cnh != '') { ?>
                <span class="alcancy__alerta alcancy__alerta__top mt-3">
                  <?=$medico->motivo_cnh?>
                </span>
              <?php } ?>              
            <?php } else { ?>
              <div class="text-center alert alert-warning" role="alert">
                Documento não enviado
              </div>
            <?php } ?>
          </td>
          <td>
            <?php if ($medico->file_crm != '') { ?>
              <a href="<?=$medico->file_crm?>" data-title="CRM -Frente" class="text-center d-block link-medicos-documentos" data-role="admin" data-url="<?=base_url()?>/root/medicos/remover_documento.php?medico=<?=$medico->id?>&tipo=crm - frente" target="_blank">
                <img src="<?=get_thumbnail_s3($medico->file_crm)?>">
              </a>
              <?php if ($medico->motivo_crm != '') { ?>
                <span class="alcancy__alerta alcancy__alerta__top mt-3">
                  <?=$medico->motivo_crm?>
                </span>
              <?php } ?>
            <?php } else { ?>
              <div class="text-center alert alert-warning" role="alert">
                Documento não enviado
              </div>
            <?php } ?>
          </td>
          <td>
            <?php if ($medico->file_cpf != '') { ?>
              <a href="<?=$medico->file_cpf?>" data-title="CRM - Verso" class="text-center d-block link-medicos-documentos" data-role="admin" data-url="<?=base_url()?>/root/medicos/remover_documento.php?medico=<?=$medico->id?>&tipo=crm - verso" target="_blank">
                <img src="<?=get_thumbnail_s3($medico->file_cpf)?>">
              </a>
              <?php if ($medico->motivo_cpf != '') { ?>
                <span class="alcancy__alerta alcancy__alerta__top mt-3">
                  <?=$medico->motivo_cpf?>
                </span>
              <?php } ?>
            <?php } else { ?>
              <div class="text-center alert alert-warning" role="alert">
                Documento não enviado
              </div>
            <?php } ?>
          </td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
  </div>
</div>