<?php
  session_start();
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $validaMedico = $conexao->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
    $validaMedico->bindParam(':cpf', $_POST['cpf']);
    $validaMedico->execute();

    if ($validaMedico->rowCount() == 0) {
      echo json_encode(Array('status' => 'FAIL', 'message' => 'CPF não encontrado.'));
      exit();
    }
    
    $conexao->beginTransaction();
    try {          
      $medico = $conexao->prepare('UPDATE medicos set paypal_agreemeent_id = \'FREE\', paypal_data_assinatura = CURDATE() WHERE cpf = :cpf');      
      $medico->bindParam(':cpf', $_POST['cpf']);
      $medico->execute();

      $medico = $validaMedico->fetch(PDO::FETCH_OBJ);
      
      $historico = $conexao->prepare('INSERT INTO documento_historicos (medico_id, acao, documento, usuario_id, motivo) VALUES (:medico_id, \'F\', \'CORTESIA\', :usuario_id, \'Cortesia Liberada\')');
      $historico->bindParam(':medico_id',  $medico->id);
      $historico->bindParam(':usuario_id', $_SESSION['root_id']);
      $historico->execute();

      $conexao->commit();

      $email  = '';
      $email .= '<p>Parabéns,</p>';
      $email .= '<br/><br/>';
      $email .= '<p>Você foi contemplado com uma assinatura cortesia.</p>';
      $email .= '<p>Acesse o seu <a href="'.base_url().'/painel/index.php">Painel</a> e comece já a emitir seus documentos.</p>';
      $email .= '<br/><br/>';
      $email .= '<p>Um abraço</p>';
      $email .= '<p>Equipe AlcancyMED</p>';

      $response = sendEmail($medico->email, $medico->nome, 'Cortesia AlcancyMED', $email);
      echo json_encode(Array('status' => "OK", 'message' => 'Cortesia liberada com sucesso.'));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }