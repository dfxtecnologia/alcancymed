<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__medico.png" alt="">
    <h4>CORTESIA</h4>
  </div>
  <div class="dashboard__wrapper">
    <?=show_alert('OK', 'CPF e/ou senha não são válidos.')?>
    <?=show_alert('FAIL', 'CPF e/ou senha não são válidos.')?>
    <form class="form__alcancy" action="<?=base_url()?>/root/medicos/gerar_cortesia.php" method="POST">    
      <div class="row">
        <div class="col-md-2">
          <div class="form-group">
            <label for="medico-cpf">CPF</label>
            <input type="text" class="form-control cpf-mask" name="cpf" id="medico-cpf">
          </div>
        </div>
      </div>
      <button type="submit" class="btn btn-primary button__alcancy"><i class="fas fa-check"></i> Salvar</button>
    </form>
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
