<?php
  session_start();
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();
    $conexao->beginTransaction();
    $msg_email = '';
    try {
      $sql  = ' UPDATE medicos SET ';
      
      if ($_POST['documento'] == 'selfie') {
        $sql .= ' motivo_selfie = :motivo ';
        $msg_email = 'a sua selfie não foi aprovada';
      } 
      if ($_POST['documento'] == 'cnh') {
        $sql .= ' motivo_cnh = :motivo ';
        $msg_email = 'o seu RG/CNH não foi aprovado';
      } 
      if ($_POST['documento'] == 'crm - verso') {
        $sql .= ' motivo_cpf = :motivo ';
        $msg_email = 'o seu CRM - Verso não foi aprovado';
      }
      if ($_POST['documento'] == 'crm - frente') {
        $sql .= ' motivo_crm = :motivo ';
        $msg_email = 'o seu CRM - Frente não foi aprovado';
      }

      $sql .= ' WHERE id = :id ';
      
      $atualizaMedico = $conexao->prepare($sql);
      $atualizaMedico->bindParam(':id', $_POST['medico']);
      $atualizaMedico->bindParam(':motivo', $_POST['motivo']);
      $atualizaMedico->execute();

      $historico = $conexao->prepare('INSERT INTO documento_historicos (medico_id, acao, documento, usuario_id, motivo) VALUES (:medico_id, \'R\', :documento, :usuario_id, :motivo)');
      $historico->bindParam(':medico_id',  $_POST['medico']);
      $historico->bindParam(':documento',  $_POST['documento']);  
      $historico->bindParam(':usuario_id', $_SESSION['root_id']);
      $historico->bindParam(':motivo', $_POST['motivo']);
      $historico->execute();

      $conexao->commit();

      $email  = '';
      $email .= '<p>Más notícias,</p>';
      $email .= '<br/><br/>';
      $email .= '<p>Infelizmente '. $msg_email .' pelo seguinte motivo: "'.$_POST['motivo'].'"</p>';
      $email .= '<p>Por favor não desista, acesse seu painel e entre na opção <a href="'.base_url().'/painel/medicos/enviar_documentos.php">Visualizar Documentos</a> e reenvie o documento.</p>';
      $email .= '<br/><br/>';
      $email .= '<p>Um abraço</p>';
      $email .= '<p>Equipe AlcancyMED</p>';

      $resultset = $conexao->prepare('SELECT * FROM medicos WHERE id = :id');
      $resultset->bindParam(':id', $_POST['medico']);
      $resultset->execute();
      
      $medico = $resultset->fetch(PDO::FETCH_OBJ);
            
      $response = sendEmail($medico->email, $medico->nome, 'Documento Reprovado AlcancyMED', $email);

      echo json_encode(Array('status' => 'OK'));
    }catch (PDOException $e) {
      $conexao->rollBack();
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }