<?php 
  session_start();
  require_once(dirname(__DIR__).'/sys/functions.php');

  unset($_SESSION['root_email']);

  header('location: ' . base_url());