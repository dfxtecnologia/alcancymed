<?php
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
?>
<?php require_once(dirname(__DIR__).'/sys/conexao.php') ?>
<?php require_once(dirname(__DIR__).'/sys/functions.php') ?>
<?php require(dirname(__DIR__).'/vendor/autoload.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>alcancyMED</title>

  <link rel="manifest" href="/manifest.json">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.3/cropper.min.css">
  <link href="<?=base_url()?>/assets/css/site.css" rel="stylesheet">
  <link href="<?=base_url()?>/assets/css/select2-bootstrap.css" rel="stylesheet">
  <link href="<?=base_url()?>/assets/css/index.css" rel="stylesheet">
  <link href="<?=base_url()?>/assets/css/utilities.css" rel="stylesheet">
  <link href="<?=base_url()?>/assets/css/floating-label.css" rel="stylesheet">
  <link href="<?=base_url()?>/assets/css/css-loader.css" rel="stylesheet">
  <link href="<?=base_url()?>/assets/css/loader-default.css" rel="stylesheet">
</head>
<body>
  <div id="overlay">
    <div class="loader loader-default is-active"></div>
  </div>
  <div class="alcancy__wrapper"></div>
  <header>
    <div class="hamburguer">
      <span></span>
    </div>     
    <a href="<?=base_url()?><?=(isSet($_SESSION['medico_cpf']) && $_SESSION['medico_cpf'])?'/painel/index.php': ''?>">
      <img src="<?=base_url()?>/assets/images/logo.png" alt="AlcancyMED" class="logo">
    </a>
    <ul>
      <?php if (isSet($_SESSION['medico_cpf']) && $_SESSION['medico_cpf']) { ?>
        <li class="alcancy__menu">
          <a href="#"><img src="<?=base_url()?>/assets/images/menu__icone__atestados.png"> Atestados</a>
          <ul class="alcancy__dropdown">
            <li><a href="<?=base_url()?>/painel/atestados/emitir.php">Emitir</a></li>
            <li><a href="<?=base_url()?>/painel/atestados/consultar.php">Consultar</a></li>
          </ul>
        </li>
        <li class="alcancy__menu">
          <a href="#"><img src="<?=base_url()?>/assets/images/menu__icone__receitas.png"> Receitas</a>
          <ul class="alcancy__dropdown">
            <li><a href="<?=base_url()?>/painel/receitas/emitir.php">Emitir</a></li>
            <li><a href="<?=base_url()?>/painel/receitas/consultar.php">Consultar</a></li>
            <li><a href="<?=base_url()?>/painel/receitas/modelo.php">Modelos</a></li>
          </ul>
        </li>
        <li class="alcancy__menu">
          <a href="#"><img src="<?=base_url()?>/assets/images/menu__icone__medico.png"><?=explode(' ', $_SESSION['medico_nome'])[0]?></a>
          <ul class="alcancy__dropdown">
            <li><a href="<?=base_url()?>/painel/medicos/meus_dados.php">Meus Dados</a></li>
            <li><a href="<?=base_url()?>/painel/medicos/assinatura_digital.php">Assinatura Digital</a></li>
            <li><a href="<?=base_url()?>/painel/medicos/alterar_senha.php">Alterar Senha</a></li>
            <li><a href="<?=base_url()?>/painel/local_atendimento/consultar.php">Locais de Atendimento</a></li>
            <li>
              <hr>
            </li>
            <li><a href="<?=base_url()?>/painel/sair.php">Sair</a></li>
          </ul>
        </li>      
      <?php } else if (isSet($_SESSION['root_email']) && $_SESSION['root_email']) { ?>
        <li class="alcancy__menu">
          <a href="#"><?=explode(' ', $_SESSION['root_nome'])[0]?></a>
          <ul class="alcancy__dropdown">
            <li><a href="<?=base_url()?>/root/index.php">Confirmar Documentos</a></li>
            <li><a href="<?=base_url()?>/root/medicos/cortesia.php">Cortesia</a></li>
            <li>
              <hr>
            </li>
            <li><a href="<?=base_url()?>/root/sair.php">Sair</a></li>
          </ul>
        </li>
      <?php } else { ?>
        <li><a href="<?=base_url()?>/contratar.php" class="contratar">Contratar</a></li>
        <!-- <li><a href="#" class="sou-medico">Sou Médico</a></li>
        <li><a href="#" class="sou-paciente">Sou Paciente</a></li> -->
        <li><a href="<?=base_url()?>/painel/" class="area-medico">Área do Médico</a></li>
      <?php }?>
    </ul>
  </header>
  <section class="main">