<?php
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
?>
<?php require_once(dirname(__DIR__).'/sys/conexao.php') ?>
<?php require_once(dirname(__DIR__).'/sys/functions.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>alcancyMED</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link href="<?=base_url()?>/assets/css/site.css" rel="stylesheet">
</head>
<body>
