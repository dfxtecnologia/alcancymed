<?php 
  header('Content-Type: application/json');
  
  require_once(dirname(__DIR__).'/sys/conexao.php');  
  
  $conexao = Conexao::getInstance();

  $search = 'UPPER(\'%'.$_GET['search'].'%\')';
  $cidadeQry = $conexao->prepare('SELECT * FROM cidades WHERE nome LIKE '. $search .' ORDER BY nome ASC');
  $cidadeQry->execute();

  $response = array();

  while($cidade = $cidadeQry->fetch(PDO::FETCH_OBJ)) { 
    $response[] = Array('id' => $cidade->id, 'text' => $cidade->nome);
  }

  echo json_encode($response);