<?php 
  header('Content-Type: application/json');
  
  require_once(dirname(__DIR__).'/sys/conexao.php');  
  
  $conexao = Conexao::getInstance();

  $search = 'UPPER(\'%'.$_GET['search'].'%\')';
  $cidQry = $conexao->prepare('SELECT * FROM cids WHERE cid like '. $search .' OR UPPER(descricao) like '. $search);
  $cidQry->bindParam(':busca', $_GET['search']);  
  $cidQry->execute();

  $response = array();

  while($cid = $cidQry->fetch(PDO::FETCH_OBJ)) { 
    $response[] = Array('id' => $cid->cid, 'descricao' => $cid->cid, 'apresentacao' => $cid->descricao);
  }

  echo json_encode($response);