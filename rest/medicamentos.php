<?php
  header('Content-Type: application/json');
  
  require_once(dirname(__DIR__).'/sys/conexao.php');  
  
  $conexao = Conexao::getInstance();

  $search = 'UPPER(\'%'.$_GET['search'].'%\')';

  $medicamentos = $conexao->prepare('SELECT * FROM medicamentos WHERE UPPER(nome_comercial) LIKE '. $search . ' ORDER BY nome_comercial ASC');
  $medicamentos->execute();

  $response = array();

  while($medicamento = $medicamentos->fetch(PDO::FETCH_OBJ)) { 
    $response[] = Array('id' => $medicamento->id, 'descricao' => $medicamento->nome_comercial, 'apresentacao' => $medicamento->apresentacao);
  }

  echo json_encode($response);