<?php
  header('Content-Type: application/json');
  
  require_once(dirname(__DIR__).'/sys/conexao.php');

  if ($_POST) {
    $response = Array();
    $conexao = Conexao::getInstance();

    $query = ' SELECT IFNULL(receitas.tipo, \'S\') as tipo, receitas.data, receitas.ativo, medicos.crm, medicos.nome medico_nome, pacientes.nome paciente_nome, pacientes.cpf '.
             '   FROM receitas '.
             '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = receitas.consultorio_medico_id)'.
             '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id)'.
             '  INNER JOIN pacientes ON (pacientes.id = receitas.paciente_id) '.
             '  WHERE receitas.chave =  :chave';
              
    $receitas = $conexao->prepare( $query );
    $receitas->bindParam(':chave', $_POST['chave']);
    $receitas->execute();
     
    if ($receitas->rowCount() == 0) {
      $query = ' SELECT \'A\' as tipo, atestados.data, atestados.ativo, medicos.crm, medicos.nome medico_nome, pacientes.nome paciente_nome, pacientes.cpf '.
              '   FROM atestados '.
              '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = atestados.consultorio_medico_id) '.
              '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
              '  INNER JOIN pacientes ON (pacientes.id = atestados.paciente_id) '.
              '  WHERE atestados.chave =  :chave ';

      $atestados = $conexao->prepare( $query );
      $atestados->bindParam(':chave', $_POST['chave']);
      $atestados->execute();

      if ($atestados->rowCount() == 0) {
        echo json_encode(Array('status' => 'FAIL'));
        exit();
      } else {
        $atestado = $atestados->fetch(PDO::FETCH_OBJ);
        $response = Array('status' => 'OK', 'tipo_documento' => $atestado->tipo, 'chave' => $_POST['chave'], 'emissao' => date('d/m/Y H:i:s', strtotime($atestado->data)), 'crm' => $atestado->crm, 'medico' => $atestado->medico_nome, 'paciente' => $atestado->paciente_nome, 'cpf' => $atestado->cpf, 'tipo' => 'Atestado', 'status_documento' => $atestado->ativo ? 'Válido' : 'Cancelado');
      }
    } else {
      $receita = $receitas->fetch(PDO::FETCH_OBJ);
      $response = Array('status' => 'OK', 'tipo_documento' => $receita->tipo, 'chave' => $_POST['chave'], 'emissao' => date('d/m/Y H:i:s', strtotime($receita->data)), 'crm' => $receita->crm, 'medico' => $receita->medico_nome, 'paciente' => $receita->paciente_nome, 'cpf' => $receita->cpf, 'tipo' => 'Receita', 'status_documento' => $receita->ativo ? 'Válido' : 'Cancelado' );
    }      

    echo json_encode($response);
  }