<?php 
  header('Content-Type: application/json');
  
  require_once(dirname(__DIR__).'/sys/conexao.php');  
  require_once(dirname(__DIR__).'/sys/functions.php');

  $valido = validaCPF($_GET['cpf']);

  if ($valido == true) {
  
    $conexao = Conexao::getInstance();

    $pacienteQry = $conexao->prepare('SELECT * FROM pacientes WHERE cpf = :cpf');
    $pacienteQry->bindParam(':cpf', $_GET['cpf']);  
    $pacienteQry->execute();

    $paciente = $pacienteQry->fetch(PDO::FETCH_OBJ);

    $response = Array();

    if ($paciente) {
      $response = Array('status' => 'OK', 'id' => $paciente->id, 'nome' => $paciente->nome, 'cep' => $paciente->cep, 'endereco' => $paciente->endereco, 'numero' => $paciente->numero, 'bairro' => $paciente->bairro, 'cidade' => $paciente->cidade_id, 'estado' => $paciente->estado, 'celular' => $paciente->celular, 'email' => $paciente->email);
    }
} else if ($valido == false) {
  $response = Array('status' => 'FAIL', 'message' => 'FAIL');
}

  echo json_encode($response);