<?php
  header('Content-Type: application/json');
  
  require_once(dirname(__DIR__).'/sys/conexao.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $query = ' SELECT * '.
             '   FROM receita_medicamentos '.
            //  '  INNER JOIN receitas ON (receitas.id = receita_medicamentos.receitas_id) '.
             '  INNER JOIN medicamentos ON (medicamentos.id = receita_medicamentos.medicamentos_id) '.
             '  WHERE receita_medicamentos.receitas_id =  :id '.
             '  ORDER BY medicamentos.nome_comercial DESC ';
              
    $resultset = $conexao->prepare( $query );
    $resultset->bindParam(':id', $_POST['id']);
    $resultset->execute();
     
    $response = Array();

    while($receita = $resultset->fetch(PDO::FETCH_OBJ)) {
      $response[] = Array('id' => $receita->medicamentos_id, 'nome_comercial' => $receita->nome_comercial, 'apresentacao' => $receita->apresentacao, 'quantidade' => $receita->quantidade, 'forma_uso' => $receita->forma_uso);      
    }

    echo json_encode($response);
  }