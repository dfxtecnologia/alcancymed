<?php 
  session_start();
  require_once(dirname(__DIR__).'/sys/functions.php');
  if (isset($_SESSION['medico_cpf'])) {
    header('location: ' . base_url() . '/painel/index.php');
  }
?>
<?php require_once(dirname(__DIR__).'/header/index.php') ?>
  <div class="main__wrapper">
    <div class="acessar__sistema">
      <h1>Acessar Conta</h1>
      <?=show_alert('FAIL', 'CPF e/ou senha não são válidos.')?>
      <form action="<?=base_url()?>/medicos/autenticar.php" method="POST" data-redirect="<?=base_url()?>/painel/index.php">
        <div class="form-group">
          <label for="medico-cpf">CPF</label>
          <input type="text" class="form-control cpf-mask" placeholder="CPF" name="cpf" id="medico-cpf" required
            autofocus>
        </div>
        <div class="form-group">
          <label for="medico-senha">Senha</label>
          <input type="password" class="form-control" placeholder="Senha" name="senha" id="medico-senha" required>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary button__alcancy">Acessar</button>
        </div>
      </form>
      <a href="<?=base_url()?>/medicos/esqueci-senha.php" class="d-block text-center">esqueci a senha</a>
    </div>
  </div>
<?php require_once(dirname(__DIR__).'/footer/index.php') ?>
