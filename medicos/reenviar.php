<?php
  require_once(dirname(__DIR__).'/sys/functions.php');
  require_once(dirname(__DIR__).'/sys/conexao.php');

  $id = isset($_GET['id']) ? $_GET['id'] : '';
  $conexao = Conexao::getInstance();

  $resultset = $conexao->prepare('SELECT * FROM medicos WHERE md5(id) = :id');
  $resultset->bindParam(':id', $id);
  $resultset->execute();
  
  $medico = $resultset->fetch(PDO::FETCH_OBJ);
  
  if ($medico) {
    $link = base_url().'/medicos/validar.php?id='. md5($medico->id);
    $response = sendEmail($medico->email, $medico->nome, 'Confirmação de Cadastro AlcancyMED', '<p>Clique no link para confirmar o seu cadastro. <a href="'.$link.'">'.$link.'</a></p>');
  }
  
  header('location: '. base_url() .'/medicos/acessar.php');
?>