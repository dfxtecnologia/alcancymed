<?php
  require_once(dirname(__DIR__).'/sys/functions.php');
  require_once(dirname(__DIR__).'/sys/conexao.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $resultset = $conexao->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
    $resultset->bindParam(':cpf', $_POST['cpf']);
    $resultset->execute();
    
    $medico = $resultset->fetch(PDO::FETCH_OBJ);
    
    if ($medico) {
      $conexao->beginTransaction();
      try {
        $randomletter = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@!#$%&"), 0, 8);
        $senha = md5($randomletter);

        $medicoUpdate = $conexao->prepare('UPDATE medicos set senha = :senha WHERE id = :id');
        $medicoUpdate->bindParam(':senha', $senha);
        $medicoUpdate->bindParam(':id', $medico->id);
        $medicoUpdate->execute();
        $conexao->commit();

        $response = sendEmail($medico->email, $medico->nome, 'Nova Senha AlcancyMED', '<p>Nova senha de acesso: <strong>'.$randomletter.'</strong></p>');
        echo json_encode(Array('status' => 'OK', 'message' => 'Nova senha enviada para e-mail de cadastro.'));
      }catch (PDOException $e) {
        $conexao->rollBack();    
        echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));      
      }
    }
  }
?>