<?php
  require_once(dirname(__DIR__).'/sys/conexao.php');
  require_once(dirname(__DIR__).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $validaMedico = $conexao->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
    $validaMedico->bindParam(':cpf', $_POST['cpf']);
    $validaMedico->execute();

    if ($validaMedico->rowCount() > 0) {
      echo json_encode(Array('status' => 'FAIL', 'message' => 'CPF já está cadastrado em nossa base de dados.'));
      exit();
    }

    $valida_cpf = validaCPF($_POST['cpf']);
    if ($valida_cpf == false) {
      echo json_encode(Array('status' => 'FAIL', 'message' => 'CPF inválido.'));
      exit(); 
    }

    $vetParametros = array (
        "secret" => "6LdQu3YUAAAAACU3yaD60n9NE9VuxuB1H-rTjrcR",
        "response" => $_POST["g-recaptcha-response"],
        "remoteip" => $_SERVER["REMOTE_ADDR"]
    );

    $curlReCaptcha = curl_init();
    curl_setopt($curlReCaptcha, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($curlReCaptcha, CURLOPT_POST, true);
    curl_setopt($curlReCaptcha, CURLOPT_POSTFIELDS, http_build_query($vetParametros));
    curl_setopt($curlReCaptcha, CURLOPT_RETURNTRANSFER, true);

    $vetResposta = json_decode(curl_exec($curlReCaptcha), true);

    curl_close($curlReCaptcha);

    if (!$vetResposta["success"]) {
      $erros = '';
      foreach ($vetResposta["error-codes"] as $strErro) { $erros .= $strErro; }
      echo json_encode(Array('status' => 'FAIL', 'message' => 'Caixa de verificação "reCaptcha" não selecionada'));
      // echo json_encode(Array('status' => 'FAIL', 'message' => $erros));
      exit();        
    }

    $conexao->beginTransaction();
    try {      
      $paypal_agreemeent_id = 'FREE';
      $plano_id = 1;

      $randomletter = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@!#$%&"), 0, 8);
      $senha = md5($randomletter);
      $medicos = $conexao->prepare('INSERT INTO medicos (nome, cpf, crm, crm_uf, senha, email, telefone_residencial, telefone_celular, cep, logradouro, bairro, cidade_id, estado, numero, paypal_agreemeent_id, paypal_data_assinatura, plano_id) VALUES (:nome, :cpf, :crm, :crm_uf, :senha, :email, :telefone_residencial, :telefone_celular, :cep, :logradouro, :bairro, :cidade_id, :estado, :numero, :paypal_agreemeent_id, :paypal_data_assinatura, :plano_id)');
      $medicos->bindParam(':nome', $_POST['nome']);
      $medicos->bindParam(':cpf', $_POST['cpf']);
      $medicos->bindParam(':crm', $_POST['crm']);
      $medicos->bindParam(':crm_uf', $_POST['crm_uf']);
      $medicos->bindParam(':senha', $senha);
      $medicos->bindParam(':email', $_POST['email']);
      $medicos->bindParam(':telefone_residencial', $_POST['telefone_residencial']);
      $medicos->bindParam(':telefone_celular', $_POST['telefone_celular']);
      $medicos->bindParam(':cep', $_POST['cep']);
      $medicos->bindParam(':logradouro', $_POST['logradouro']);
      $medicos->bindParam(':bairro', $_POST['bairro']);
      $medicos->bindParam(':cidade_id', $_POST['cidade']);
      $medicos->bindParam(':estado', $_POST['estado']);
      $medicos->bindParam(':numero', $_POST['numero']);
      $medicos->bindParam(':paypal_agreemeent_id', $paypal_agreemeent_id);
      $medicos->bindParam(':paypal_data_assinatura', date('Y-m-d'));
      $medicos->bindParam(':plano_id', $plano_id);
      $medicos->execute();

      $medico_id = $conexao->lastInsertId();

      $consultorio = $conexao->prepare('INSERT INTO consultorios (descricao, telefone, logradouro, cidade_id, cep, bairro, estado, numero) VALUES (\'Consultório\', :telefone, :logradouro, :cidade_id, :cep, :bairro, :estado, :numero)');
      $consultorio->bindParam(':telefone', $_POST['telefone_consultorio']);
      $consultorio->bindParam(':cep', $_POST['cep']);
      $consultorio->bindParam(':logradouro', $_POST['logradouro']);
      $consultorio->bindParam(':bairro', $_POST['bairro']);
      $consultorio->bindParam(':cidade_id', $_POST['cidade']);
      $consultorio->bindParam(':estado', $_POST['estado']);
      $consultorio->bindParam(':numero', $_POST['numero']);      
      $consultorio->execute();

      $consultorio_id = $conexao->lastInsertId();

      $medico_consultorio = $conexao->prepare('INSERT INTO consultorio_medicos (medico_id, consultorio_id) VALUES (:medico_id, :consultorio_id)');
      $medico_consultorio->bindParam(':medico_id', $medico_id);
      $medico_consultorio->bindParam(':consultorio_id', $consultorio_id);
      $medico_consultorio->execute();

      $conexao->commit();

      $link = base_url().'/medicos/validar.php?id='. md5($medico_id);
      $email = '';
      $email .= '<p>Olá,</p>';
      $email .= '<br/><br/>';
      $email .= '<p>Obrigado por se cadastrar no AlcancyMED.</p>';
      $email .= '<p>Clique no link para confirmar o seu cadastro. <a href="'.$link.'">'.$link.'</a></p>';
      $email .= '<p>Após validar seu cadastro utilize a senha de acesso para acessar o sistema.</p>';
      $email .= '<p>Senha de acesso: <strong>'.$randomletter.'</strong></p>';
      $email .= '<p>Altere sua senha regularmente.</p>';
      $email .= '<p>É necessário enviar seus documentos e aguardar a aprovação dos mesmos. Este processo pode levar até 7 dias.</p>';
      $email .= '<p>Após enviar seus documentos, não deixe de configurar sua conta PayPal com seus dados de pagamento para o débito da mensalidade.</p>';
      $email .= '<p>Não esqueça, aqui é tudo simples, você pode cancelar quando quiser se não gostar.</p>';
      $email .= '<p>Feito isso, basta aguardar nossa equipe validar seus documentos e liberar para seu uso.</p>';
      $email .= '<br/><br/>';
      $email .= '<p>Um abraço</p>';
      $email .= '<p>Equipe AlcancyMED</p>';

      $response = sendEmail($_POST['email'], $_POST['nome'], 'Confirmação de Cadastro AlcancyMED', $email);

      echo json_encode(Array('status' => 'OK', 'message' => 'Cadastrado com sucesso'));
    }catch (Exception $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }
