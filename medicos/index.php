<?php require_once(dirname(__DIR__).'/header/index.php') ?>
  <h4 class="d-inline">Sou Médico</h4> <a href="<?=base_url()?>/painel/" class="badge badge-primary">Acessar Painel</a>
  <hr/>
  <p>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc consequat nulla non libero scelerisque, pulvinar porta eros vehicula. Nullam placerat dui vel molestie rutrum. Donec eget tincidunt mauris. Donec eros libero, aliquam at luctus id, condimentum sed mauris. Nunc condimentum, risus sit amet finibus bibendum, nunc justo tempor arcu, in placerat tortor nibh a justo. Quisque mattis nunc augue, non luctus sem rhoncus non. Morbi eu facilisis metus, non feugiat est. Aliquam in tristique sapien.
  </p>
  <p>
  Aenean ut diam diam. Nullam sed faucibus nisi, vitae aliquet nulla. Phasellus faucibus facilisis urna, at ultricies eros faucibus vitae. Nunc vitae fringilla ex. Nulla pellentesque pretium sapien, nec varius lacus accumsan nec. Maecenas purus tellus, placerat sit amet pulvinar vitae, vestibulum fringilla magna. Proin eleifend dui at nisi efficitur congue. Vivamus dui neque, pulvinar feugiat leo vel, lobortis dapibus est. Suspendisse fringilla pulvinar mi, ac mattis neque tristique sed. Morbi tempus eros lorem, vitae tristique augue congue sed. Quisque id dui eu augue accumsan bibendum eget in est. Quisque facilisis mattis enim, ac porta mi fringilla vitae.
  </p>
  <p>
  Duis ullamcorper aliquam cursus. Morbi nec vestibulum lorem, ac posuere elit. Phasellus scelerisque sed libero eu fringilla. Aenean pharetra congue dui, sed tristique eros dictum ac. Nulla ante lectus, bibendum ac justo id, vulputate posuere risus. Etiam ornare at metus quis tempor. Suspendisse potenti. Pellentesque ac laoreet diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel erat pellentesque, eleifend justo ut, euismod felis. Fusce mattis nisi nec rutrum finibus. Aenean ornare imperdiet rutrum. Donec lorem nunc, pellentesque quis ultricies eu, maximus vel enim.
  </p>
  <p>
  Donec fermentum auctor metus, in tincidunt ante tempor in. Praesent sit amet aliquam metus, id venenatis dolor. Vestibulum mollis ac ligula eget gravida. Nulla semper sit amet est nec rutrum. Fusce hendrerit vel ante a efficitur. Praesent viverra mi eu felis gravida, in vestibulum ipsum rhoncus. Maecenas molestie urna efficitur mi ultricies, eget facilisis tellus sagittis.
  </p>
  <p>
  Pellentesque sodales molestie arcu, ac tempus urna cursus commodo. Nullam nunc mauris, ultrices id dictum id, pulvinar a nunc. Vestibulum eu turpis vitae felis hendrerit sodales non sit amet augue. Cras blandit, odio faucibus maximus fermentum, lectus metus convallis est, in maximus ex tortor et ipsum. Ut vel tristique dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sed pretium neque, vel varius orci. Aliquam rhoncus vulputate erat quis convallis. Nunc sagittis luctus purus, vel fermentum lacus faucibus quis. In turpis tellus, tincidunt quis lectus sit amet, consectetur malesuada tortor. Nam nulla justo, commodo a purus vitae, pretium fermentum erat. Maecenas ultrices magna vel feugiat commodo.
  </p>

  <h4>Tenho Interesse</h4>
  <hr/>

  <div class="container">
    <?=show_alert('OK', 'Cadastrado com sucesso.')?>
    <?=show_alert('FAIL', 'CPF já está cadastrado em nossa base de dados.')?>
    <form action="<?=base_url()?>/medicos/salvar.php" method="POST">
      <div class="row">
        <div class="col-md-8">
          <div class="form-group">
            <label class="control-label" for="medico-nome">Nome</label>
            <input type="text" class="form-control" name="nome">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label class="control-label" for="medico-cpf">CPF</label>
            <input type="text" class="form-control cpf-mask" name="cpf">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label class="control-label" for="medico-crm">CRM</label>
            <input type="text" class="form-control" name="crm">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label" for="medico-nome">E-mail</label>
            <input type="text" class="form-control" name="email">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label class="control-label" for="medico-cpf">Telefone Consultório</label>
            <input type="text" class="form-control telefone-mask" name="telefone_consultorio">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label class="control-label" for="medico-cpf">Telefone Residêncial</label>
            <input type="text" class="form-control telefone-mask" name="telefone_residencial">
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label class="control-label" for="medico-crm">Telefone Celular</label>
            <input type="text" class="form-control telefone-mask" name="telefone_celular">
          </div>
        </div>
      </div>
      <div class="float-right">
        <button type="submit" class="btn btn-primary">Cadastrar Interesse</button>
      </div>
    </form>
  </div>

<?php require_once(dirname(__DIR__).'/footer/index.php') ?>
