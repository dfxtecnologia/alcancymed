<?php require_once(dirname(__DIR__).'/header/index.php') ?>
  <div class="main__wrapper">
    <div class="acessar__sistema">
      <h1>Esqueci a Senha</h1>
      <?=show_alert('FAIL', 'CPF e/ou senha não são válidos.')?>
      <form action="<?=base_url()?>/medicos/senha.php" method="POST" data-redirect="<?=base_url()?>/painel/index.php">
        <div class="form-group">
          <label for="medico-cpf">CPF</label>
          <input type="text" class="form-control cpf-mask" placeholder="CPF" name="cpf" id="medico-cpf" required
            autofocus>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary button__alcancy">Reenviar</button>
        </div>
      </form>
    </div>
  </div>
<?php require_once(dirname(__DIR__).'/footer/index.php') ?>
