<?php
  session_start();
  require_once(dirname(__DIR__).'/sys/functions.php');
  require_once(dirname(__DIR__).'/sys/conexao.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $senha = md5($_POST['senha']);

    $validaMedico = $conexao->prepare('SELECT * FROM medicos WHERE cpf = :cpf and senha = :senha');
    $validaMedico->bindParam(':cpf', $_POST['cpf']);
    $validaMedico->bindParam(':senha', $senha);
    $validaMedico->execute();

    if ($validaMedico->rowCount() == 0) {
      echo json_encode(Array('status' => 'FAIL', 'message' => 'CPF e/ou senha não são válidos.'));
      exit();
    }

    $medico = $validaMedico->fetchALL(PDO::FETCH_ASSOC);

    if (!$medico[0]['confirmado']) {
      echo json_encode(Array('status' => 'FAIL', 'message' => 'Cadastro não confirmado. <a href="'.base_url().'/medicos/reenviar.php?id='.md5($medico[0]['id']).'" class="alert-link">Reenviar e-mail de confirmação</a>.'));
      exit();
    }

    $_SESSION['medico_id'] = $medico[0]['id'];
    $_SESSION['medico_cpf'] = $medico[0]['cpf'];
    $_SESSION['medico_nome'] = $medico[0]['nome'];
    $_SESSION['local_atendimento'] = '';
    echo json_encode(Array('status' => 'OK'));
  }