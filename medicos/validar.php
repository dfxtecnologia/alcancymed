<?php
  require_once(dirname(__DIR__).'/sys/functions.php');
  require_once(dirname(__DIR__).'/sys/conexao.php');

  $id = isset($_GET['id']) ? $_GET['id'] : '';
    $conexao = Conexao::getInstance();

    $conexao->beginTransaction();
    try {          
      $medico = $conexao->prepare('UPDATE medicos set confirmado = 1 WHERE md5(id) = :id');      
      $medico->bindParam(':id', $id);
      $medico->execute();


      $conexao->commit();

      header('location: '. base_url() .'/medicos/acessar.php');
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
?>