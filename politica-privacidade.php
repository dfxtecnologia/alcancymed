<?php require_once(__DIR__.'/header/index.php') ?>
  <div class="main__wrapper">
    <h1>Política de Privacidade</h1>
    <p>AlcancyMED tem o compromisso de zelar pela segurança e privacidade dos dados pessoais dos profissionais liberais, pacientes e empresas, protegendo as informações de perda, uso impróprio ou acesso não autorizado.</p>
    <p>Usamos a tecnologia mais avançada sempre seguindo as melhores práticas de desenvolvimento de software disponíveis.</p>
    <p>Ao se cadastrar o profissional informa seus dados pessoais e imagens de seus documentos. A AlcancyMED se compromete em não compartilhar com nenhum terceiro sem a devida autorização do titular e proprietário dos documentos. Todas as informações são armazenadas de acordo com os mais rígidos padrões de confiança e confiabilidade.</p>
    <p>A AlcancyMED não irá vender, alugar ou transferir estes dados para terceiros em hipótese alguma. Entretanto, essas informações podem ser agrupadas e utilizadas, internamente, como estatísticas genéricas, visando obter um melhor entendimento do perfil dos usuários apenas para propriedade da AlcancyMED ou em caso de divulgação destes dados somente sem nenhuma exposição particular, e somente por amostragem genérica agrupadas.</p>
    <p>Os dados em nenhuma hipótese terão regime comercial.</p>
    <p>O seu endereço de e-mail também é tratado com sigilo, não sendo fornecido a nenhum terceiro. Ao se cadastrar você poderá receber informações, campanhas, promoções, novidades da plataforma AlcancyMED, melhorias e atualizações, entre outros, desde que enviados pela AlcancyMED.</p>
    <p>A AlcancyMED recebe e armazena automaticamente, por meio de cookies, informações sobre as atividades do navegador, incluindo o endereço IP e as páginas acessadas. Estes registros de atividade serão utilizados apenas para reconhecer um visitante constante, melhorar a experiência do usuário e viabilizar recursos personalizados.</p>
    <p>Os cookies são pequenos arquivos de dados transferidos de um site da web para o disco do seu computador e NÃO armazenam dados pessoais como nome, endereço ou o número do seu cartão de crédito. Você também pode desabilitar o salvamento de cookies, apagá-los e gerenciar sua utilização através do seu navegador.</p>
    <p>É de total responsabilidade do usuário guardar as informações de senha e login de acesso. Evite o uso de senhas de fácil acesso como datas de nascimento, nomes ou sequências numéricas como 123456.</p>
    <p>Recomendamos a alteração de senha regularmente.</p>
    <p>Todas as informações contidas neste site são de propriedade da AlcancyMED. Não poderão ser alteradas, copiadas, extraídas ou de qualquer forma utilizadas sem a prévia e expressa autorização por escrito da AlcancyMED. O usuário fica ciente que a utilização indevida das informações aqui contidas pode acarretar em sanções civis e criminais.</p>
    <p>Esta política de privacidade está sujeita a constantes melhorias e aprimoramentos, por isso, recomendamos sua regular leitura.</p>
  </div>
<?php require_once(__DIR__.'/footer/index.php') ?>
