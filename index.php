<?php require_once('header/index.php') ?>
  <section class="full__banner">
    <h1>SEU RECEITUÁRIO E ATESTADO MÉDICO, SEM FRAUDE</h1>
    <p>Garanta a emissão dos seus documentos através da plataforma AlcancyMED com validação de autenticidade e evite surpresas. A tranquilidade e controle que faltava para você cuidar do que realmente importa, seu paciente.</p>
    <div class="validar__documento">
      <h4>VALIDAR DOCUMENTO</h4>
      <form>
        <div class="form-group">
          <label for="chave-documento">CHAVE</label>
          <input type="text" class="form-control chave-validacao" id="chave-validacao-documento"/>
        </div>
        <?=show_alert('FAIL', 'Nenhum documento encontrado em nossa base com a chave informada.')?>
        <button type="submit" id="btn-validar-documento" class="btn btn-primary btn-block button__alcancy" data-link="<?=base_url()?>/rest/validar.php">VALIDAR</button>
      </form>
    </div>
  </section>
  <section class="porque__contratar">
    <div class="texto__porque__contratar">
      <h1>POR QUE CONTRATAR ALCANCYMED?</h1>
      <p>A melhor gestão de receituário e atestados, sem burocracia e sem complicação. Confiabilidade e segurança para o MÉDICO. Garantia de autenticidade para as EMPRESAS.</p>    
      <p>Emita e valide seus documentos quando e onde quiser.</p>
      <a href="<?=base_url()?>/contratar.php" class="btn btn-primary button__alcancy">Comece Já</a>
    </div>
    <div class="imagem__porque___contratar">
      <img src="assets/images/pessoa_porque_contratar.png" alt="Porque Contratar">
    </div>
    <div class="clearfix"></div>
  </section>
  <section class="o__que__fazemos">
    <h1>O QUE FAZEMOS?</h1>
    <div class="circulo__externo"></div>
    <div class="circulo__interno"></div>
    <div class="circulos emissao_receitas">
      <p>EMISSÃO DE RECEITAS</p>
    </div>
    <div class="circulos emissao_atestados">
      <p>EMISSÃO DE ATESTADOS</p>
    </div>
    <div class="circulos validacao_documentos">
      <p>VALIDAÇÃO DE DOCUMENTOS</p>
    </div>
    <div class="circulos gestao_documentos">
      <p>GESTÃO DE DOCUMENTOS</p>
    </div>
    <div class="circulos gestao_pacientes">
      <p>GESTÃO DE PACIENTES</p>
    </div>
    <div class="circulos sem_burocracia">
      <p>SEM BUROCRACIA</p>
    </div>
    <div class="circulos cancele_quando_quiser">
      <p>CANCELE QUANDO QUISER</p>
    </div>

    <div class="circulos comece__ja">
      <p>COMECE JÁ</p>
    </div>
    <div class="circulos preco">
      <div class="preco__wrapper">
        <span class="preco__text">a partir de</span>
        <span>29<sup>,00</sup></span>
        <span class="preco__text">mensais</span>
      </div>
    </div>
    <div class="clearfix"></div>
  </section>
  <section class="depoimentos">
    <h1>DEPOIMENTOS</h1>
    <div class="depoimento__wrapper">
      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="depoimento__texto">
              "Sou Plantonista e já emitiram meu atestado falso por diversas vezes. Gastamos tempo e haja paciência em frequentar delegacia, Cremesp, diretoria da instituição. iDOC me protege desses problemas pois contém a segurança de autenticidade de que eu mesmo omiti o documento. Recomendo. Órgão de classe poderiam adotar a ferramenta, bem como Upas, hospitais etc."
            </div>
            <div class="depoimento__autor">
              <p>Dr. Eduardo Marques</p>
              <small>CRM - SP 191690</small>
            </div>
          </div>
          <div class="carousel-item">
            <div class="depoimento__texto">
              "Ja tive problemas em esquecer carimbo e receitar antibiótico apenas assinando e colocando o CRM. A farmácia não aceitou. Tive que pedir ao colega do plantão para carimbar essa receita. Agora, com o iDOC, não tenho esse problema. Já deixei o carimbo de lado"
            </div>
            <div class="depoimento__autor">
              <p>Dra. Larissa Modesto </p>
              <small>CRM - SP 151249</small>
            </div>
          </div>
          <div class="carousel-item">
            <div class="depoimento__texto">
              "Uso iDOC em UPA, consultório, enfermaria e maternidade. Renovo receitas via celular e envio aos pacientes via whatsapp. Muito prático e seguro."
            </div>
            <div class="depoimento__autor">
              <p>Dr. Salum Bueno</p>
              <small>CRM - SP 191917</small>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="modal fade" id="modal-validacao-documento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <dl class="row">
            <dt class="col-sm-3">Emissão</dt>
            <dd class="col-sm-9 emissao-validacao"></dd>

            <dt class="col-sm-3">Médico</dt>
            <dd class="col-sm-9 medico-validacao"></dd>

            <dt class="col-sm-3">Paciente</dt>
            <dd class="col-sm-9 paciente-validacao"></dd>

            <dt class="col-sm-3">Status</dt>
            <dd class="col-sm-9 status-validacao"></dd>            
          </dl>
        </div>
        <div class="modal-footer">
          <a class="btn btn-success" href="<?=base_url()?>/painel/" target="_blank">Visualizar</a>
          <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button> -->
        </div>
      </div>
    </div>
  </div>
<?php require_once('footer/index.php') ?>