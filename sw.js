self.addEventListener('install', function (event) {
  console.log('SW Installed');
  event.waitUntil(
    caches.open('alcancy-med-v12')
      .then(function (cache) {
        cache.addAll([
          '/',
          '/index.php',
          '/assets/images/alcancy_med_final-geral.png',
          '/assets/images/alcancymed-96x96.png',
          '/assets/images/alcancymed-144x144.png',
          '/assets/images/alcancymed-192x192.png',
          '/assets/images/alcancymed-256x256.png',
          '/assets/images/alcancymed-512x512.png',
          '/assets/images/cancele_quando_quiser.png',
          '/assets/images/dashboard__documento__CNH.png',
          '/assets/images/dashboard__documento__CPF.png',
          '/assets/images/dashboard__documento__CRM.png',
          '/assets/images/dashboard__documento__selfie.png',
          '/assets/images/emissao_atestados.png',
          '/assets/images/emissao_receitas.png',
          '/assets/images/fullbanner.png',
          '/assets/images/gestao_documentos.png',
          '/assets/images/gestao_pacientes.png',
          '/assets/images/header__atestados.png',
          '/assets/images/header__locais__atendimento.png',
          '/assets/images/header__medico.png',
          '/assets/images/header__receitas.png',
          '/assets/images/logo.png',
          '/assets/images/menu__dashboard__atestados.png',
          '/assets/images/menu__dashboard__medico.png',
          '/assets/images/menu__dashboard__receitas.png',
          '/assets/images/menu__icone__atestados.png',
          '/assets/images/menu__icone__medico.png',
          '/assets/images/menu__icone__receitas.png',
          '/assets/images/paypal-logo.png',
          '/assets/images/pessoa_porque_contratar.png',
          '/assets/images/sem_burocracia.png',
          '/assets/images/validacao_documentos.png',
          'https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700',
          'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css',
          'https://use.fontawesome.com/releases/v5.4.1/css/all.css',
          'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css',
          'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.3/cropper.min.css',
          '/assets/css/site.css',
          '/assets/css/select2-bootstrap.css',
          '/assets/css/index.css',
          '/assets/css/utilities.css',
          '/assets/css/floating-label.css',
          '/assets/css/css-loader.css',
          '/assets/css/loader-default.css',
          'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
          'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
          'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',
          'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js',
          'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js',
          'https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.3/cropper.min.js',
          'https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js',
          '/assets/js/select2.pt-BR.js',
          '/assets/js/serializefiles.js',
          '/assets/js/mascaras.js',
          '/assets/js/forms.js',
          '/assets/js/select2.js',
          '/assets/js/receitas.js',
          '/assets/js/validar.js',
          '/assets/js/files.js',
          '/assets/js/cep.js',
          '/assets/js/visualizar-documentos-medico.js',
          '/assets/js/assinatura-digital.js',
          '/assets/js/site.js'
        ]);
      })
  );
});

self.addEventListener('activate', function () {
  console.log('SW Activated');
});

self.addEventListener('fetch', (event) => {
  event.respondWith(async function () {
    try {
      return await fetch(event.request);
    } catch (err) {
      return caches.match(event.request);
    }

    // caches.match(event.request)
    //   .then(function (res) {
    //     if (res) {
    //       return res;
    //     } else {
    //       return fetch(event.request);
    //     }
    //   })
  }());
});