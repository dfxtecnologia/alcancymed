<?php
  require_once(dirname(__DIR__).'/sys/functions.php');
  require(dirname(__DIR__).'/vendor/autoload.php');

// Sandbox
// 'AXLIS2oMyIwIW5VfeirbRdNdXWa2RbeTEN-jVJ5Dg-uxKpMSOS77DvplTmTANWjZAX7gU0p9NSkSA0n4',     // ClientID
// 'EH4-etVWCLruMyqy9F57Z4h-WxtXRwZd3q_m8ZCsKLgRAU_EN42BD0UN6V_FcxWmPedW5CIaB-xXDDXa'      // ClientSecret

// Live
// 'ATQosxeuayZ0TuWqdwm_O0rw2OchYHWC5DOWmTTqKekIlmLRGkxXDdRkvtBHcHj7aLdIRXwBdk8FhSGL',     // ClientID
// 'EHtuJde_30DFw40f--ScHQjXaVRn6DAiQ2hE3w-prH3CL3vPiXkr0LrfzX0S3rbagur0VdTJh05gqL_K'      // ClientSecret
  function paypalApiAuth() {
    $apiContext = new \PayPal\Rest\ApiContext(
      new \PayPal\Auth\OAuthTokenCredential(
        $_ENV['PAYPAL_CLIENT_ID'],     // ClientID
        $_ENV['PAYPAL_CLIENT_SECRET']      // ClientSecret
      )
    );
    
    $apiContext->setConfig(array('mode' => $_ENV['PAYPAL_MODE']));

    return $apiContext;
  }

  function paypalCreatePlan($apiContext, $plano, $mes_gratis = true) {
    $metodos_pagamento = Array();

    $plan = new \PayPal\Api\Plan();
    $plan->setName('AlcancyMED')
         ->setDescription('Mensalidade do direitos de uso do sistema AlcancyMED')
         ->setType('INFINITE');

    $paymentDefinition = new \PayPal\Api\PaymentDefinition();
    $paymentDefinition->setName('Mensalidade')
        ->setType('REGULAR')
        ->setFrequency('Month')
        ->setFrequencyInterval("1")
        ->setCycles("0")
        ->setAmount(new \PayPal\Api\Currency(array('value' => $plano['valor_plano'], 'currency' => 'BRL')));  

    array_push($metodos_pagamento, $paymentDefinition);

    // if ($plano['dias_gratuidade'] > 0) {
    //   $paymentDefinitionTrial = new \PayPal\Api\PaymentDefinition();
    //   $paymentDefinitionTrial->setName('Trial')
    //       ->setType('TRIAL')
    //       ->setFrequency('DAY')
    //       ->setFrequencyInterval($plano['dias_gratuidade'])
    //       ->setCycles("1")
    //       ->setAmount(new \PayPal\Api\Currency(array('value' => 0, 'currency' => 'BRL')));
    //   array_push($metodos_pagamento, $paymentDefinitionTrial);
    // }

    $merchantPreferences = new \PayPal\Api\MerchantPreferences();
    $baseUrl = base_url();
    $planoid = $plano['id_plano'];
    $mudanca = $plano['mudanca_plano'] ? '1' : '0';

    $merchantPreferences->setReturnUrl("$baseUrl/painel/medicos/paypal.php?success=true&plano_id=$planoid&alteracao=$mudanca")
        ->setCancelUrl("$baseUrl/painel/medicos/paypal.php?success=false")
        ->setAutoBillAmount("yes")
        ->setInitialFailAmountAction("CONTINUE")
        ->setMaxFailAttempts("0");

    $plan->setPaymentDefinitions($metodos_pagamento);
    $plan->setMerchantPreferences($merchantPreferences);

    $createdPlan = $plan->create($apiContext);

    $patch = new \PayPal\Api\Patch();

    $value = new \PayPal\Common\PayPalModel('{
	       "state":"ACTIVE"
	     }');

    $patch->setOp('replace')
          ->setPath('/')
          ->setValue($value);

    $patchRequest = new \PayPal\Api\PatchRequest();
    $patchRequest->addPatch($patch);

    $createdPlan->update($patchRequest, $apiContext);

    $activatedPlan = \PayPal\Api\Plan::get($createdPlan->getId(), $apiContext);

    return $activatedPlan;
  }

  function paypalDeletePlan($apiContext, $paypal_plan_id) {
    $createdPlan = \PayPal\Api\Plan::get($paypal_plan_id, $apiContext);

    try {
      $result = $createdPlan->delete($apiContext);

      return $createdPlan;
    } catch (Exception $ex) {
      var_export($ex);
      die();
    }    
  }
  
  function paypalCreateAgreement($apiContext, $createdPlan, $medico, $proximo_vencimento) {
    $agreement = new \PayPal\Api\Agreement();
    $agreement->setName('AlcancyMED')
              ->setDescription('Direitos de uso do sistema de automação AlcancyMED')
              ->setStartDate( date("Y-m-d", strtotime($proximo_vencimento)) . 'T00:00:00Z'  );

    $plan = new \PayPal\Api\Plan();
    $plan->setId($createdPlan->getId());
    $agreement->setPlan($plan);

    $payer = new \PayPal\Api\Payer();
    $payer->setPaymentMethod('paypal');
    $agreement->setPayer($payer);

    $shippingAddress = new \PayPal\Api\ShippingAddress();
    $shippingAddress->setLine1($medico->logradouro)
                    ->setCity($medico->cidade)
                    ->setState($medico->estado)
                    ->setPostalCode($medico->cep)
                    ->setCountryCode('BR');
    $agreement->setShippingAddress($shippingAddress);

    $requestAgreement = clone $agreement;

    $agreement = $agreement->create($apiContext);    

    return $agreement;
  }

  function paypalExecuteAgreement($apiContext, $token) {
    $agreement = new \PayPal\Api\Agreement();
    $agreement->execute($token, $apiContext);
    $agreement = \PayPal\Api\Agreement::get($agreement->getId(), $apiContext);

    return $agreement;
  }

  function paypalGetAgreement($apiContext, $agreementID) {
    return \PayPal\Api\Agreement::get($agreementID, $apiContext);
  }

  function paypalGetAgreementTransactions($apiContext, $agreement, $data_inicial = '', $data_final = '') {
    $inicio = $data_inicial == '' ? date('Y-m-d', strtotime('-1 year')) : $data_inicial;
    $fim    = $data_final == '' ? date('Y-m-d', strtotime('+5 days')) : $data_final;

    try {
      return \PayPal\Api\Agreement::searchTransactions($agreement->getId(), array('start_date' => $inicio, 'end_date' => $fim), $apiContext);      
    } catch (Exception $e) {
      echo '<pre>';
      var_export($e);
      echo '</pre>';
      return;
    }
  }

  function paypalCancelAgreement($apiContext, $agreementID) { 
    try {
      $agreement = paypalGetAgreement($apiContext,$agreementID);

      $agreementStateDescriptor = new \PayPal\Api\AgreementStateDescriptor();
      $agreementStateDescriptor->setNote("Cancelamento da assinatura do AlcancyMED");

      $agreement->suspend($agreementStateDescriptor, $apiContext);

      return \PayPal\Api\Agreement::get($agreement->getId(), $apiContext);
    } catch (Exception $e) {
      echo '<pre>';
      var_export($e);
      echo '</pre>';
      return;      
    }
  }