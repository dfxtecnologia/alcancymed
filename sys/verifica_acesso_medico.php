<?php
  session_start();
  require_once(__DIR__.'/conexao.php');
  require_once(__DIR__.'/functions.php');

  if ( $_SESSION['medico_cpf'] == '' ) {
    header('location: ' . base_url() . '/medicos/acessar.php');
  }
  
  if (!isMedicoConfirmacaoCadastro(Conexao::getInstance())) {
    if ((!strpos($_SERVER['REQUEST_URI'], 'painel/index.php')) && (!strpos($_SERVER['REQUEST_URI'], 'enviar_documentos.php')) && (!strpos($_SERVER['REQUEST_URI'], 'meus_dados.php'))) {
      $direitosUsoAposVencimento = isPeriodoUsoAposCancelamento(Conexao::getInstance());
      if (!$direitosUsoAposVencimento['direto_uso']) {
        if (!strpos($_SERVER['REQUEST_URI'], 'consultar.php'))  {
          header('location: ' . base_url() . '/painel/index.php');
        }
      }
    }
  }