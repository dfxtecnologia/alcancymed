<?php
require(dirname(__DIR__).'/vendor/autoload.php');

if (($_SERVER['HTTP_HOST'] != 'alcancymed.com.br') && ($_SERVER['HTTP_HOST'] != 'alcancy-med.herokuapp.com')) {
  $dotenv = new Dotenv\Dotenv(dirname(__DIR__));
  $dotenv->load();
}

define('PAGE_LIMIT', 10);
define('STATUS_MEDICO_ANALISE', 'A'); //Análise
define('STATUS_MEDICO_COMPLETO', 'C'); //Completo
define('STATUS_MEDICO_INCOMPLETO', 'I'); //Incompleto
define('STATUS_MEDICO_PAGAMENTO', 'P'); //Aguardando Forma Pagamento
define('SENDGRID_API_KEY', $_ENV['SENDGRID_API_KEY']);

function base_url() {
  if (@$_SERVER['HTTP_X_FORWARDED_PROTO']) {
    $protocol = $_SERVER['HTTP_X_FORWARDED_PROTO'];
  } else {
    if (isset($_SERVER['HTTPS'])) {
      $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    } else {
      $protocol = 'http';
    }
  }    

  return $protocol . "://" . $_SERVER['HTTP_HOST'];
}

function make_thumb($src, $dest, $desired_width) {
  $arr_image_details = getimagesize($src);

  if ($arr_image_details[2] == IMAGETYPE_GIF) {
    $source_image = ImageCreateFromGIF($src);
  }
  if ($arr_image_details[2] == IMAGETYPE_JPEG) {
    $source_image = ImageCreateFromJPEG($src);
  }
  if ($arr_image_details[2] == IMAGETYPE_PNG) {
    $source_image = ImageCreateFromPNG($src);
  }

  $width = imagesx($source_image);
  $height = imagesy($source_image);

  $desired_height = floor($height * ($desired_width / $width));

  $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

  if(($arr_image_details[2] == IMAGETYPE_GIF) || ($arr_image_details[2]==IMAGETYPE_PNG)){
    imagealphablending($virtual_image, false);
    imagesavealpha($virtual_image,true);
    $transparent = imagecolorallocatealpha($virtual_image, 255, 255, 255, 127);
    imagefilledrectangle($virtual_image, 0, 0, $desired_width, $desired_height, $transparent);
  }

  imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

  if ($arr_image_details[2] == IMAGETYPE_GIF) {
    imagegif($virtual_image, $dest);
  }
  if ($arr_image_details[2] == IMAGETYPE_JPEG) {
    imagejpeg($virtual_image, $dest);
  }
  if ($arr_image_details[2] == IMAGETYPE_PNG) {
    imagepng($virtual_image, $dest);
  }  
}

function create_guid($namespace = '') {     
  static $guid = '';
  $uid = uniqid("", true);
  $data = $namespace;
  $data .= $_SERVER['REQUEST_TIME'];
  $data .= $_SERVER['HTTP_USER_AGENT'];
  // $data .= $_SERVER['LOCAL_ADDR'];
  // $data .= $_SERVER['LOCAL_PORT'];
  $data .= $_SERVER['REMOTE_ADDR'];
  $data .= $_SERVER['REMOTE_PORT'];
  $md5 = md5($data);
  $hash = strtoupper(hash('ripemd128', $uid . $guid . $md5));
  $guid = substr($hash,  0,  8) . 
          '-' .
          substr($hash,  8,  4) .
          '-' .
          substr($hash, 12,  4) .
          '-' .
          substr($hash, 16,  4) .
          '-' .
          substr($hash, 20, 12);
  return $guid;
}

function show_alert($tipo, $mensagem) {
  $alerta_tipo = 'info';
  $mensagem_tipo = 'Informativo!';

  if ($tipo == 'FAIL') {
      $alerta_tipo = 'danger';
      $mensagem_tipo = 'Falha!';
  } else if ($tipo == 'OK') {
      $alerta_tipo = 'success';
      $mensagem_tipo = 'Sucesso!';
  }

  echo '  <div class="alert alert-'. $alerta_tipo .' alert-dismissible fade show d-none" role="alert">';
  echo '    <strong>'. $mensagem_tipo .'</strong> '. $mensagem;
  echo '  </div>';
}

function verifica_confirmacao($conn, $tipo, $cpf) {
  $resultset = $conn->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
  $resultset->bindParam(':cpf', $cpf);
  $resultset->execute();

  $medico = $resultset->fetchALL(PDO::FETCH_ASSOC);
  
  return $medico[0]['confirmado'] === 1;
}

function arquivos_enviados($conn, $tipo, $cpf) {
  $resultset = $conn->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
  $resultset->bindParam(':cpf', $cpf);
  $resultset->execute();

  $medico = $resultset->fetchALL(PDO::FETCH_ASSOC);
  
  return ($medico[0]['file_selfie'] !== '') || 
        ($medico[0]['file_crm'] !== '') || 
        ($medico[0]['file_cpf'] !== '') || 
        ($medico[0]['file_cnh'] !== '');
}

function isImage($arquivo){
  $type = $arquivo['type'];
  $extensions=array('image/jpg','image/jpe','image/jpeg','image/jfif','image/png','image/bmp','image/dib','image/gif');
  return in_array($type, $extensions);
}

function getStatusConfirmacao($conn, $tipo, $cpf) {
  $resultset = $conn->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
  $resultset->bindParam(':cpf', $cpf);
  $resultset->execute();

  $medico = $resultset->fetch(PDO::FETCH_OBJ);
  $status_class = 'incompleto';
  $status_decricao = 'Incompleto';
  $status = $medico->confirmacao_status;
  switch ($medico->confirmacao_status) {
    case STATUS_MEDICO_ANALISE:
        $status_class = 'incompleto';
        $status_decricao = 'Em Análise';        
      break;
    case STATUS_MEDICO_COMPLETO:
        $status_class = 'completo';
        $status_decricao = 'Completo';

        if (($medico->paypal_agreemeent_id == '') || ($medico->paypal_data_cancelamento != '') || ($medico->paypal_data_cancelamento == '0000-00-00')) {
          $status_class = 'incompleto';
          $status_decricao = 'Aguardando método de pagamento';
          $status = STATUS_MEDICO_PAGAMENTO;
        }
      break;
    case STATUS_MEDICO_INCOMPLETO:
        $status_class = 'incompleto';
        $status_decricao = 'Incompleto';
      break;
    case STATUS_MEDICO_PAGAMENTO:
        $status_class = 'incompleto';
        $status_decricao = 'Aguardando método de pagamento';
      break;
  }
  
  return Array('class' => $status_class, 'descricao' => $status_decricao, 'status' => $status);
}

function showConsultorioSelect($conn, $cpf) {
  $resultset = $conn->prepare('SELECT count(*) quantidade FROM consultorio_medicos WHERE medico_id = (SELECT id FROM medicos WHERE cpf = :cpf)');
  $resultset->bindParam(':cpf', $cpf);
  $resultset->execute();

  $medico = $resultset->fetch(PDO::FETCH_OBJ);

  return ($medico->quantidade > 1);
}

function getConsultorioID($conn, $cpf) {
  $resultset = $conn->prepare('SELECT * FROM consultorio_medicos WHERE medico_id = (SELECT id FROM medicos WHERE cpf = :cpf)');
  $resultset->bindParam(':cpf', $cpf);
  $resultset->execute();

  $medico = $resultset->fetch(PDO::FETCH_OBJ);
  
  return $medico->id;
}

function sim_nao($value) {
  if ($value =="0") {
    $retorno = "Não";
  } elseif ($value == "1") {
    $retorno = "Sim";
  }

  return $retorno;
}

function converte_data($data) {
  $datanova = implode('-',array_reverse(explode('/', $data)));
  return $datanova;
}

function upload_to_s3($filename) {
  $access_key = $_ENV['AMAZON_S3_ACCESS_KEY'];
  $secret_key = $_ENV['AMAZON_S3_SECRET_KEY'];
  $bucket = $_ENV['AMAZON_S3_BUCKET'];
  $s3 = Aws\S3\S3Client::factory(
    array(
      'credentials' => array(
        'key' => $access_key,
        'secret' => $secret_key
      ),
      'version' => $_ENV['AMAZON_S3_VERSION'],
      'region'  => $_ENV['AMAZON_S3_REGION']
    )
  );        

  try {
    $upload = $s3->putObject(
      array(
        'Bucket'=>$bucket,
        'Key' =>  basename($filename),
        'SourceFile' => $filename,
        'StorageClass' => 'REDUCED_REDUNDANCY'
      )
    );
  } catch (Aws\S3\Exception\S3Exception $e) {
    echo $e->getMessage();
  } catch (Exception $e) {
    echo $e->getMessage();
  }        
  // unlink($filename);    
  return htmlspecialchars($upload->get('ObjectURL'));
}

function get_thumbnail_s3($url) {
  $file = basename($url);
  $url = explode($file, $url)[0];

  $ext = pathinfo($file, PATHINFO_EXTENSION);
  $filename = pathinfo($file, PATHINFO_FILENAME);
  $thumbnail = $filename . '_t.' . $ext;

  return $url . $thumbnail;
}

function getTotais($conn, $cpf) {
  $totais = Array();
  
  $query = ' SELECT COUNT(atestados.id) total '.
           '  FROM atestados '.
           '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = atestados.consultorio_medico_id) '.
           '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
           '  WHERE medicos.cpf = :cpf';
  $resultset = $conn->prepare($query);
  $resultset->bindParam(':cpf', $cpf);
  $resultset->execute();

  $atestado = $resultset->fetch(PDO::FETCH_OBJ);
  $totais['atestado'] = $atestado->total;

  $query = ' SELECT COUNT(receitas.id) total '.
            '  FROM receitas '.
            '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = receitas.consultorio_medico_id) '.
            '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
            '  WHERE medicos.cpf = :cpf';
  $resultset = $conn->prepare($query);
  $resultset->bindParam(':cpf', $cpf);
  $resultset->execute();

  $receita = $resultset->fetch(PDO::FETCH_OBJ);
  $totais['receita'] = $receita->total;

  return $totais;
}

function isMedicoConfirmacaoCadastro($conn) {
  $medicoQry = $conn->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
  $medicoQry->bindParam(':cpf', $_SESSION['medico_cpf']);
  $medicoQry->execute();
  $medico = $medicoQry->fetch(PDO::FETCH_OBJ);

  if ($medico->confirmacao_status != STATUS_MEDICO_COMPLETO) {
    return false;
  } else {
    if ($medico->paypal_agreemeent_id == '') {
      return false;
    } else {
      if ($medico->paypal_data_cancelamento != '') { 
        if ($medico->paypal_data_cancelamento == '0000-00-00') {
          return false;
        } else {
          $atual_vencimento = date('Y-m-d', strtotime( date('Y'). date('-m-', strtotime($medico->paypal_data_cancelamento)). date('d', strtotime($medico->paypal_data_assinatura))));
          $proximo_vencimento = date('Y-m-d', strtotime('+1 month', strtotime( date('Y'). date('-m-', strtotime($medico->paypal_data_cancelamento)). date('d', strtotime($medico->paypal_data_assinatura)))));

          if (($medico->paypal_data_cancelamento >= $atual_vencimento) && ($medico->paypal_data_cancelamento <= $proximo_vencimento)) {
            return true;
          } else {
            return false;
          }
        }
      }
    }
  }

  return true;
}

function isPeriodoUsoAposCancelamento($conn) {
  $medicoQry = $conn->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
  $medicoQry->bindParam(':cpf', $_SESSION['medico_cpf']);
  $medicoQry->execute();
  $medico = $medicoQry->fetch(PDO::FETCH_OBJ);

  $atual_vencimento = date('Y-m-d', strtotime( date('Y'). date('-m-', strtotime($medico->paypal_data_cancelamento)). date('d', strtotime($medico->paypal_data_assinatura))));
  $proximo_vencimento = date('Y-m-d', strtotime('+1 month', strtotime( date('Y'). date('-m-', strtotime($medico->paypal_data_cancelamento)). date('d', strtotime($medico->paypal_data_assinatura)))));

  return array('direto_uso' => (($medico->paypal_data_cancelamento >= $atual_vencimento) && ($medico->paypal_data_cancelamento <= $proximo_vencimento)), 'proximo_vencimento' => $proximo_vencimento);
}

function setPaypalHistorico($conn, $medico, $acao, $paypal_agreemeent_id) {
  $paypal_historico = $conn->prepare('INSERT INTO paypal_historico (medico_id, acao, paypal_agreemeent_id) VALUES (:medico_id, :acao, :paypal_agreemeent_id)');
  $paypal_historico->bindParam(':medico_id', $medico);
  $paypal_historico->bindParam(':acao', $acao);
  $paypal_historico->bindParam(':paypal_agreemeent_id', $paypal_agreemeent_id);
  $paypal_historico->execute();
}

function sendEmail($destinatario_email, $destinatario_nome, $assunto, $mensagem) {
  $html_email  = '<html>';
  $html_email .= '  <body>';
  $html_email .= '    <h3>'. $assunto .'</h3>';
  $html_email .= '    <br/>';
  $html_email .= '    <br/>';
  $html_email .= '    '. $mensagem;
  $html_email .= '    <br/>';
  $html_email .= '    <br/>';
  $html_email .= '    <img src="'. base_url() .'/assets/images/logo.png" width="200px">';
  $html_email .= '  </body>';
  $html_email .= '</html>';

  $from = new \SendGrid\Email('AlcancyMED', 'contato@dfxtecnologia.com.br');
  $to = new \SendGrid\Email($destinatario_nome, $destinatario_email);  
  $content = new \SendGrid\Content("text/html", $html_email);
  $mail = new \SendGrid\Mail($from, $assunto, $to, $content);

  $sg = new \SendGrid(SENDGRID_API_KEY);

  return $sg->client->mail()->send()->post($mail);
}

function get_req($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);    
    curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
    $return = curl_exec($curl);
    curl_close($curl);
    return $return;
}

function validaCPF($cpf) {
 
    // Extrai somente os números
    $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
     
    // Verifica se foi informado todos os digitos corretamente
    if (strlen($cpf) != 11) {
        return false;
    }

    // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
    if (preg_match('/(\d)\1{10}/', $cpf)) {
        return false;
    }

    // Faz o calculo para validar o CPF
    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf{$c} * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ($cpf{$c} != $d) {
            return false;
        }
    }
    return true;

}

function getPlanoAtivo($conn) {
 $query = ' SELECT planos.* '.
           '   FROM planos '.
           '  INNER JOIN medicos ON (medicos.plano_id = planos.id) '.
           '  WHERE medicos.id =  :medico ';
  $resultset = $conn->prepare($query);
  $resultset->bindParam(':medico', $_SESSION['medico_id']);
  $resultset->execute();

  $plano = $resultset->fetch(PDO::FETCH_OBJ);
  
  return Array('plano_quantidade_documentos' => $plano->quantidade_documentos, 'plano_descricao' => $plano->descricao, 'plano_titulo' => $plano->titulo, 'plano_valor' => $plano->valor);
}

function getQuantidadeDocumentosMes($conn) {
  $totais = Array();
  $total_geral = 0;

  $query = ' SELECT COUNT(atestados.id) total '.
           '  FROM atestados '.
           '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = atestados.consultorio_medico_id) '.
           '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
           '  WHERE medicos.id = :medico '.
           '    AND EXTRACT(MONTH FROM data) = :mes '.
           '    AND EXTRACT(YEAR FROM data) = :ano ';
  $resultset = $conn->prepare($query);
  $resultset->bindParam(':medico', $_SESSION['medico_id']);
  $resultset->bindParam(':mes', date('m'));
  $resultset->bindParam(':ano', date('Y'));
  $resultset->execute();

  $atestado = $resultset->fetch(PDO::FETCH_OBJ);
  $total_geral += $atestado->total;
  $totais['atestado'] = $atestado->total;

  $query = ' SELECT COUNT(receitas.id) total '.
           '  FROM receitas '.
           '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = receitas.consultorio_medico_id) '.
           '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
           '  WHERE medicos.id = :medico '.
           '    AND EXTRACT(MONTH FROM data) = :mes '.
           '    AND EXTRACT(YEAR FROM data) = :ano ';
  $resultset = $conn->prepare($query);
  $resultset->bindParam(':medico', $_SESSION['medico_id']);
  $resultset->bindParam(':mes', date('m'));
  $resultset->bindParam(':ano', date('Y'));  
  $resultset->execute();

  $receita = $resultset->fetch(PDO::FETCH_OBJ);
  $totais['receita'] = $receita->total;

  $total_geral += $receita->total;
  $totais['geral'] = $total_geral; 
  
  return $totais;
}