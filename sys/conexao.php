<?php
require(dirname(__DIR__).'/vendor/autoload.php');


if (($_SERVER['HTTP_HOST'] != 'alcancymed.com.br') && ($_SERVER['HTTP_HOST'] != 'alcancy-med.herokuapp.com')) {
  $dotenv = new Dotenv\Dotenv(dirname(__DIR__));
  $dotenv->load();
}

class Conexao extends PDO {
 
  private static $instancia;

  public function Conexao($dsn, $username = "", $password = "") {
    // O construtro abaixo é o do PDO
    parent::__construct($dsn, $username, $password);
  }

  public static function getInstance() {
    // Se o a instancia não existe eu faço uma
    if(!isset( self::$instancia )){
      try {
        self::$instancia = new Conexao('mysql:host='. $_ENV['DB_HOST'] .';dbname='. $_ENV['DB_DATABASE'] .';charset=utf8', $_ENV['DB_USER'], $_ENV['DB_PASS']);
      } catch ( Exception $e ) {
        echo $e->getMessage();
        exit ();
      }
    }
    // Se já existe instancia na memória eu retorno ela
    //http://objota.com.br/web/php/usando-classe-de-conexao-com-pdo-exemplos-praticos.html
    return self::$instancia;
  }
}