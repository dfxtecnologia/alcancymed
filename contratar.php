<?php require_once(__DIR__.'/header/index.php') ?>
  <div class="main__wrapper">
    <div class="oque__fazemos">
      <h1>O que fazemos</h1>
      <p>A melhor gestão de receituário e atestados, sem burocracia e sem complicação. Confiabilidade e segurança para o MÉDICO. Garantia de autenticidade para as EMPRESAS.</p>
      <p>Emita e valide seus documentos quando e onde quiser.</p>
      <ul>
        <li>
          <i class="fas fa-check"></i>
          <span>emissão de receitas</span>
        </li>
        <li>
          <i class="fas fa-check"></i>
          <span>emissão de atestados</span>
        </li>
        <li>
          <i class="fas fa-check"></i>
          <span>validação de documentos</span>
        </li>
        <li>
          <i class="fas fa-check"></i>
          <span>gestão de documentos</span>
        </li>
        <li>
          <i class="fas fa-check"></i>
          <span>gestão de pacientes</span>
        </li>
        <li>
          <i class="fas fa-check"></i>
          <span>sem burocracia</span>
        </li>
        <li>
          <i class="fas fa-check"></i>
          <span>cancele quando quiser</span>
        </li>
      </ul>
      <div class="preco">
        <div class="preco__wrapper">
          <span class="preco__text">a partir de</span>
          <span>29<sup>,00</sup></span>
          <span class="preco__text">mensais</span>
        </div>
      </div>
    </div>
    <div class="tenho__interesse">
      <h1>Tenho Interesse</h1>
      <form action="<?=base_url()?>/medicos/salvar.php" method="POST">
        <?=show_alert('OK', 'Cadastrado com sucesso.')?>
        <?=show_alert('FAIL', 'CPF já está cadastrado em nossa base de dados.')?>      
        <div class="row">
          <div class="col-md-9">
            <div class="form-group">
              <label class="control-label" for="medico-nome">Nome completo</label>
              <input type="text" class="form-control" name="nome" required>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label" for="medico-cpf">CPF</label>
              <input type="text" class="form-control cpf-mask" name="cpf" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="medico-crm">CRM</label>
              <input type="text" class="form-control" name="crm" required>
            </div>
          </div>
          <div class="form-group col-md-6">
            <label for="crm_uf">Estado emissor</label>
            <select class="form-control select2-basic busca-cep-estado" name="crm_uf" id="crm_uf" required>
              <option></option>
              <option value="AC">Acre</option>
              <option value="AL">Alagoas</option>
              <option value="AP">Amapá</option>
              <option value="AM">Amazonas</option>
              <option value="BA">Bahia</option>
              <option value="CE">Ceará</option>
              <option value="DF">Distrito Federal</option>
              <option value="ES">Espírito Santo</option>
              <option value="GO">Goiás</option>
              <option value="MA">Maranhão</option>
              <option value="MT">Mato Grosso</option>
              <option value="MS">Mato Grosso do Sul</option>
              <option value="MG">Minas Gerais</option>
              <option value="PA">Pará</option>
              <option value="PB">Paraíba</option>
              <option value="PR">Paraná</option>
              <option value="PE">Pernambuco</option>
              <option value="PI">Piauí</option>
              <option value="RJ">Rio de Janeiro</option>
              <option value="RN">Rio Grande do Norte</option>
              <option value="RS">Rio Grande do Sul</option>
              <option value="RO">Rondônia</option>
              <option value="RR">Roraima</option>
              <option value="SC">Santa Catarina</option>
              <option value="SP">SãoPaulo</option>
              <option value="SE" >Sergipe</option>
              <option value="TO">Tocantins</option>
              <option value="EX">Estrangeiro</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label" for="medico-nome">E-mail</label>
          <input type="text" class="form-control" name="email" required>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="medico-cpf">Telefone Consultório</label>
              <input type="text" class="form-control telefone-mask" name="telefone_consultorio" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="medico-cpf">Telefone Residêncial</label>
              <input type="text" class="form-control telefone-mask" name="telefone_residencial" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label" for="medico-crm">Telefone Celular</label>
              <input type="text" class="form-control telefone-mask" name="telefone_celular" required>
            </div>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-2">
            <label for="cep">CEP</label>
            <input type="text" class="form-control busca-cep cep-mask" name="cep" id="cep" required>
          </div>
          <div class="form-group col-md-6">
            <label for="logradouro">Logradouro</label>
            <input type="text" class="form-control busca-cep-rua" name="logradouro" id="logradouro" required>
          </div>
          <div class="form-group col-md-2">
            <label for="numero">Número</label>
            <input type="text" class="form-control" name="numero" id="numero" required>
          </div>
          <div class="form-group col-md-2">
            <label for="bairro">Bairro</label>
            <input type="text" class="form-control busca-cep-bairro" name="bairro" id="bairro" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="cidade">Cidade</label>
            <select class="form-control select2-basic-search busca-cep-cidade" name="cidade" id="cidade" data-search-url="<?=base_url()?>/rest/cidades.php" required>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label for="estado">Estado</label>
            <select class="form-control select2-basic busca-cep-estado" name="estado" id="estado" required>
              <option></option>
              <option value="AC">Acre</option>
              <option value="AL">Alagoas</option>
              <option value="AP">Amapá</option>
              <option value="AM">Amazonas</option>
              <option value="BA">Bahia</option>
              <option value="CE">Ceará</option>
              <option value="DF">Distrito Federal</option>
              <option value="ES">Espírito Santo</option>
              <option value="GO">Goiás</option>
              <option value="MA">Maranhão</option>
              <option value="MT">Mato Grosso</option>
              <option value="MS">Mato Grosso do Sul</option>
              <option value="MG">Minas Gerais</option>
              <option value="PA">Pará</option>
              <option value="PB">Paraíba</option>
              <option value="PR">Paraná</option>
              <option value="PE">Pernambuco</option>
              <option value="PI">Piauí</option>
              <option value="RJ">Rio de Janeiro</option>
              <option value="RN">Rio Grande do Norte</option>
              <option value="RS">Rio Grande do Sul</option>
              <option value="RO">Rondônia</option>
              <option value="RR">Roraima</option>
              <option value="SC">Santa Catarina</option>
              <option value="SP">SãoPaulo</option>
              <option value="SE" >Sergipe</option>
              <option value="TO">Tocantins</option>
              <option value="EX">Estrangeiro</option>
            </select>
          </div>
        </div>
        <div class="g-recaptcha float-left" hl="pt-BR" data-sitekey="6LdQu3YUAAAAAMv_vERzJ0G2iwCEdl23vbqhCXj4"></div>
        <div class="float-right">
          <button type="submit" class="btn btn-primary button__alcancy">Contratar</button>
        </div>
        <div class="clearfix"></div>
      </form>
    </div>
    <div class="clearfix"></div>
  </div>
<?php require_once(__DIR__.'/footer/index.php') ?>
