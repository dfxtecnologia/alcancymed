<?php require_once(__DIR__.'/header/index.php') ?>
  <div class="main__wrapper">
    <h1>Política de Cancelamento</h1>
    <p>A plataforma AlcancyMED não aplica multas ou obrigações de uso do software por tempo determinado.</p>
    <p>Contrate e cancele quando quiser.</p>
    <p>Em caso de cancelamento em até 7 (sete) dias corridos, faremos o estorno 100% do valor pago de acordo com o CDC (código de defesa do consumidor).</p>
    <p>Após o prazo de 7 (sete) dias corridos, em caso de cancelamento, não será estornado nenhum valor financeiro. A plataforma ficará disponível para acesso até o fim do período pago antecipadamente.</p>
    <p>Cada período mensal corresponde a 30 dias.</p>
  </div>
<?php require_once(__DIR__.'/footer/index.php') ?>
