<?php require_once(__DIR__.'/header/index.php') ?>
  <div class="main__wrapper">
    <h1>Termos de uso</h1>
    <p>A plataforma AlcancyMED opera na modalidade pré-paga.</p>
    <p>Realizar pagamento via cartão de crédito ou boleto.</p>
    <p>Após realizar seu cadastro e enviar sua documentação, seus dados serão analisados pela equipe da AlcancyMED para liberação do acesso em até 5 dias úteis.</p>
    <p>Após a confirmação dos dados seu acesso na plataforma é liberado.</p>
    <p>Os documentos ficarão disponíveis para validação no prazo de 6 meses contados após sua emissão.</p>
  </div>
<?php require_once(__DIR__.'/footer/index.php') ?>
