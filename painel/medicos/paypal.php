<?php require_once(dirname(dirname(__DIR__)).'/sys/functions.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/sys/conexao.php'); ?>
<?php require_once(dirname(dirname(__DIR__)).'/paypal/functions.php') ?>
<?php 
  session_start();
  $conexao = Conexao::getInstance();
  $resultset = $conexao->prepare('SELECT medicos.*, cidades.nome as cidade, planos.valor FROM medicos INNER JOIN cidades ON (cidades.id = medicos.cidade_id) INNER JOIN planos ON (planos.id = medicos.plano_id) WHERE cpf = :cpf');
  $resultset->bindParam(':cpf', $_SESSION['medico_cpf']);
  $resultset->execute();

  $medico = $resultset->fetch(PDO::FETCH_OBJ);
  
  if (isset($_GET['token'])) {
    if (isset($_GET['success']) && $_GET['success'] == 'true') {
      // if (($medico->paypal_agreemeent_id == '') || (($medico->paypal_data_cancelamento != '') && ($medico->paypal_data_cancelamento != '0000-00-00'))) {
        $apiContext = paypalApiAuth();
        $agreement = paypalExecuteAgreement($apiContext, $_GET['token']);
        $conexao->beginTransaction();
        try {
          $medicos = $conexao->prepare('UPDATE medicos SET paypal_agreemeent_id = :paypal_agreemeent_id, paypal_data_cancelamento = null, plano_id = :plano_id WHERE cpf = :cpf');
          $medicos->bindParam(':paypal_agreemeent_id', $agreement->getId());
          $medicos->bindParam(':cpf', $_SESSION['medico_cpf']);
          $medicos->bindParam(':plano_id', $_GET['plano_id']);
          $medicos->execute();
          
          setPaypalHistorico($conexao, $_SESSION['medico_id'], 'A', $agreement->getId());
          $conexao->commit();
          // echo json_encode(Array('status' => 'OK'));
          if ($_GET['alteracao']) {
            $response = sendEmail($medico->email, $medico->nome, 'Alteração de assinatura confirmada AlcancyMED', '<p>Sua assinatura do sistema AlcancyMED foi alterada com sucesso.</p>');
          } else {
            $response = sendEmail($medico->email, $medico->nome, 'Assinatura confirmada AlcancyMED', '<p>Sua assinatura do sistema AlcancyMED foi confirmada com sucesso.</p>');
          } 

          header('location: '. base_url() .'/painel/?paypal=200');
        }catch (PDOException $e) {
          $conexao->rollBack();
          // echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
          header('location: '. base_url() .'/painel/?paypal=400&msg='.$e->getMessage());
        }
      // }
    } else {
      // Pagamento não aprovado!
      header('location: '. base_url() .'/painel/?paypal=404');
    }
  } else {
    $apiContext = paypalApiAuth();

    $data = explode('-', $medico->paypal_data_assinatura);
    if ($data[2] >= date('d')) {
      $proximo_vencimento = date('Y-m-d', strtotime(date('Y').date('-m-').$data[2]));
    } else {
      $proximo_vencimento = date('Y-m-d', strtotime('+1 month', strtotime(date('Y').date('-m-').$data[2])));
    }

    $dias_ate_vencimento = date_diff(new DateTime($proximo_vencimento .' 00:00:00'), new DateTime(date('Y-m-d') .' 00:00:00'))->days;
    
    if (isSet($_GET['plano_id'])) {
      $resultset = $conexao->prepare('SELECT planos.* FROM planos WHERE id = :id');
      $resultset->bindParam(':id', $_GET['plano_id']);
      $resultset->execute();

      $plano = $resultset->fetch(PDO::FETCH_OBJ);

      if ($medico->valor == 0.00) {
        $paypal_plano = Array(
          'id_plano'        => $plano->id,
          'nome_plano'      => $plano->titulo,
          'descricao_plano' => $plano->descricao,
          'valor_plano'     => $plano->valor,
          'dias_gratuidade' => $dias_ate_vencimento,
          'mudanca_plano'   => false
        );

        $conexao->beginTransaction();
        try {
          $plan = paypalCreatePlan($apiContext, $paypal_plano);
          $agreement = paypalCreateAgreement($apiContext, $plan, $medico, $proximo_vencimento);

          $medicos = $conexao->prepare('UPDATE medicos SET paypal_plan_id = :paypal_plan_id WHERE cpf = :cpf');
          $medicos->bindParam(':cpf', $_SESSION['medico_cpf']);
          $medicos->bindParam(':paypal_plan_id', $plan->getId());
          $medicos->execute();
          $conexao->commit();

          header('location: '. $agreement->getApprovalLink());
        } catch (Exception $e) {
          $conexao->rollBack();
          header('location: '. base_url() .'/painel/?paypal=400&msg='.$e->getMessage());
        }
      } else {
        $paypal_plano = Array(
          'id_plano'        => $plano->id,
          'nome_plano'      => $plano->titulo,
          'descricao_plano' => $plano->descricao,
          'valor_plano'     => $plano->valor,
          'dias_gratuidade' => $dias_ate_vencimento,
          'mudanca_plano'   => true
        );
        
        $conexao->beginTransaction();
        try {
          $agreement = paypalCancelAgreement($apiContext, $medico->paypal_agreemeent_id);
          $plan = paypalDeletePlan($apiContext, $medico->paypal_plan_id);

          $plan = paypalCreatePlan($apiContext, $paypal_plano);
          $agreement = paypalCreateAgreement($apiContext, $plan, $medico, $proximo_vencimento);
          
          $medicos = $conexao->prepare('UPDATE medicos SET paypal_plan_id = :paypal_plan_id WHERE cpf = :cpf');
          $medicos->bindParam(':cpf', $_SESSION['medico_cpf']);
          $medicos->bindParam(':paypal_plan_id', $plan->getId());
          $medicos->execute();
          $conexao->commit();

          header('location: '. $agreement->getApprovalLink());
        } catch (Exception $e) {
          $conexao->rollBack();
          header('location: '. base_url() .'/painel/?paypal=400&msg='.$e->getMessage());
        }        
      }      
    } else {
      $conexao->beginTransaction();
      try {
        $agreement = paypalCancelAgreement($apiContext, $medico->paypal_agreemeent_id);

        $medicos = $conexao->prepare('UPDATE medicos SET paypal_agreemeent_id = \'FREE\', paypal_data_cancelamento = null, plano_id = 1 WHERE cpf = :cpf');
        $medicos->bindParam(':cpf', $_SESSION['medico_cpf']);
        $medicos->execute();

        setPaypalHistorico($conexao, $_SESSION['medico_id'], 'C', $medico->paypal_agreemeent_id);

        $conexao->commit();
        $response = sendEmail($medico->email, $medico->nome, 'Assinatura cancelada AlcancyMED', '<p>Sua assinatura do sistema AlcancyMED foi cancelada com sucesso.</p>');
        header('location: '. base_url() .'/painel/?paypal=200');
      } catch (Exception $e) {
        $conexao->rollBack();
        header('location: '. base_url() .'/painel/?paypal=400&msg='.$e->getMessage());
      }
    }
  }
?>
