<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__medico.png" alt="">
    <h4>Alterar Senha</h4>
  </div>
  <div class="dashboard__wrapper">
    <?=show_alert('OK', 'CPF e/ou senha não são válidos.')?>
    <?=show_alert('FAIL', 'CPF e/ou senha não são válidos.')?>
    <form class="form__alcancy" action="<?=base_url()?>/painel/medicos/salvar_senha.php" method="POST">
      <input type="hidden" name="medico_id" value="<?=$_SESSION['medico_id']?>">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="medico-senha-atual">Senha Atual</label>
            <input type="password" class="form-control" name="senha-atual" id="medico-senha-atual" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="medico-nova-senha">Nova Senha</label>
            <input type="password" class="form-control" name="nova-senha" id="medico-nova-senha" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="medico-repetir-senha">Repetir Senha</label>
            <input type="password" class="form-control" name="repetir-senha" id="medico-repetir-senha" required>
          </div>
        </div>
      </div>
      <div class="float-right">
        <button type="submit" class="btn btn-primary button__alcancy"><i class="fas fa-check"></i> Alterar</button>
      </div>
      <div class="clearfix"></div>
    </form>  
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
