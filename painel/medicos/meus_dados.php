<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php        
  $conexao = Conexao::getInstance();

  $query = ' SELECT medicos.*, cidades.nome AS cidade_nome, planos.titulo, planos.descricao, planos.quantidade_documentos, planos.valor '.
           '   FROM medicos '.
           '   LEFT JOIN cidades ON (cidades.id = medicos.cidade_id) '. 
           '  INNER JOIN planos ON (planos.id = medicos.plano_id) '.
           '  WHERE medicos.id = :id ';

  $resultset = $conexao->prepare( $query );
  $resultset->bindParam(':id', $_SESSION['medico_id']);
  $resultset->execute();

  $medico = $resultset->fetch(PDO::FETCH_OBJ);
  $status = getStatusConfirmacao(Conexao::getInstance(), 'medico', $_SESSION['medico_cpf']);
?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__medico.png" alt="">
    <h4>Meus Dados</h4>
  </div>
  <div class="dashboard__wrapper">
    <form class="form__alcancy" action="<?=base_url()?>/painel/medicos/salvar_meus_dados.php" method="POST" data-redirect="<?=base_url()?>/painel/medicos/meus_dados.php">
      <input type="hidden" name="medico_id" value="<?=$_SESSION['medico_id']?>">
      <div class="row">
        <div class="col-md-9">
          <div class="form-group">
            <label for="medico-nome">Nome completo</label>
            <input type="text" class="form-control" name="nome" id="medico-nome" value="<?=$medico->nome?>" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="medico-cpf">CPF</label>
            <input type="text" class="form-control cpf-mask" name="cpf" id="medico-cpf" value="<?=$medico->cpf?>" disabled>
          </div>
        </div>        
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="medico-crm">CRM</label>
            <input type="text" class="form-control" name="crm" id="medico-crm" value="<?=$medico->crm?>" disabled>
          </div>
        </div>
        <div class="form-group col-md-6">
            <label for="crm_uf">Estado emissor</label>
            <select class="form-control select2-basic busca-cep-estado" name="crm_uf" id="crm_uf" disabled>
              <option></option>
              <option value="AC" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'AC') { echo "selected"; }} ?>>Acre</option>
              <option value="AL" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'AL') { echo "selected"; }} ?>>Alagoas</option>
              <option value="AP" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'AP') { echo "selected"; }} ?>>Amapá</option>
              <option value="AM" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'AM') { echo "selected"; }} ?>>Amazonas</option>
              <option value="BA" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'BA') { echo "selected"; }} ?>>Bahia</option>
              <option value="CE" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'CE') { echo "selected"; }} ?>>Ceará</option>
              <option value="DF" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'DF') { echo "selected"; }} ?>>Distrito Federal</option>
              <option value="ES" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'ES') { echo "selected"; }} ?>>Espírito Santo</option>
              <option value="GO" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'GO') { echo "selected"; }} ?>>Goiás</option>
              <option value="MA" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'MA') { echo "selected"; }} ?>>Maranhão</option>
              <option value="MT" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'MT') { echo "selected"; }} ?>>Mato Grosso</option>
              <option value="MS" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'MS') { echo "selected"; }} ?>>Mato Grosso do Sul</option>
              <option value="MG" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'MG') { echo "selected"; }} ?>>Minas Gerais</option>
              <option value="PA" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'PA') { echo "selected"; }} ?>>Pará</option>
              <option value="PB" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'PB') { echo "selected"; }} ?>>Paraíba</option>
              <option value="PR" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'PR') { echo "selected"; }} ?>>Paraná</option>
              <option value="PE" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'PE') { echo "selected"; }} ?>>Pernambuco</option>
              <option value="PI" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'PI') { echo "selected"; }} ?>>Piauí</option>
              <option value="RJ" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'RJ') { echo "selected"; }} ?>>Rio de Janeiro</option>
              <option value="RN" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'RN') { echo "selected"; }} ?>>Rio Grande do Norte</option>
              <option value="RS" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'RS') { echo "selected"; }} ?>>Rio Grande do Sul</option>
              <option value="RO" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'RO') { echo "selected"; }} ?>>Rondônia</option>
              <option value="RR" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'RR') { echo "selected"; }} ?>>Roraima</option>
              <option value="SC" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'SC') { echo "selected"; }} ?>>Santa Catarina</option>
              <option value="SP" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'SP') { echo "selected"; }} ?>>São Paulo</option>
              <option value="SE" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'SE') { echo "selected"; }} ?>>Sergipe</option>
              <option value="TO" <?php if (isset($medico->crm_uf)) {if ($medico->crm_uf === 'TO') { echo "selected"; }} ?>>Tocantins</option>
            </select>
          </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="medico-email">E-mail</label>
            <input type="text" class="form-control" name="email" id="medico-email" value="<?=$medico->email?>" required>
          </div>
        </div>            
        <div class="col-md-3">
          <div class="form-group">
            <label for="medico-telefoneresidencial">Telefone Residêncial</label>
            <input type="text" class="form-control telefone-mask" name="telefone_residencial" id="medico-telefoneresidencial" value="<?=$medico->telefone_residencial?>" required>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="medico-telefonecelular">Telefone Celular</label>
            <input type="text" class="form-control telefone-mask" name="telefone_celular" id="medico-telefonecelular" value="<?=$medico->telefone_celular?>" required>
          </div>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="medico-cep">CEP</label>
          <input type="text" class="form-control busca-cep cep-mask" name="cep" id="medico-cep" value="<?=$medico->cep?>" required>
        </div>
        <div class="form-group col-md-4">
          <label for="medico-logradouro">Logradouro</label>
          <input type="text" class="form-control busca-cep-rua" name="logradouro" id="medico-logradouro" value="<?=$medico->logradouro?>" required>
        </div>
        <div class="form-group col-md-2">
          <label for="medico-bairro">Numero</label>
          <input type="text" class="form-control busca-cep-bairro" name="numero" id="medico-numero" value="<?=$medico->numero?>" required>
        </div>
        <div class="form-group col-md-4">
          <label for="medico-bairro">Bairro</label>
          <input type="text" class="form-control busca-cep-bairro" name="bairro" id="medico-bairro" value="<?=$medico->bairro?>" required>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="medico-cidade">Cidade</label>
          <select class="form-control select2-basic-search busca-cep-cidade" name="cidade" id="cidade" data-search-url="<?=base_url()?>/rest/cidades.php" required value="<?=$medico->cidade_id?>">
            <option value="<?=$medico->cidade_id?>"><?=$medico->cidade_nome?></option>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="medico-estado">Estado</label>
          <select class="form-control select2-basic busca-cep-estado" name="estado" id="medico-estado" required>
            <option></option>
            <option value="AC" <?php if (isset($medico->estado)) {if ($medico->estado === 'AC') { echo "selected"; }} ?>>Acre</option>
            <option value="AL" <?php if (isset($medico->estado)) {if ($medico->estado === 'AL') { echo "selected"; }} ?>>Alagoas</option>
            <option value="AP" <?php if (isset($medico->estado)) {if ($medico->estado === 'AP') { echo "selected"; }} ?>>Amapá</option>
            <option value="AM" <?php if (isset($medico->estado)) {if ($medico->estado === 'AM') { echo "selected"; }} ?>>Amazonas</option>
            <option value="BA" <?php if (isset($medico->estado)) {if ($medico->estado === 'BA') { echo "selected"; }} ?>>Bahia</option>
            <option value="CE" <?php if (isset($medico->estado)) {if ($medico->estado === 'CE') { echo "selected"; }} ?>>Ceará</option>
            <option value="DF" <?php if (isset($medico->estado)) {if ($medico->estado === 'DF') { echo "selected"; }} ?>>Distrito Federal</option>
            <option value="ES" <?php if (isset($medico->estado)) {if ($medico->estado === 'ES') { echo "selected"; }} ?>>Espírito Santo</option>
            <option value="GO" <?php if (isset($medico->estado)) {if ($medico->estado === 'GO') { echo "selected"; }} ?>>Goiás</option>
            <option value="MA" <?php if (isset($medico->estado)) {if ($medico->estado === 'MA') { echo "selected"; }} ?>>Maranhão</option>
            <option value="MT" <?php if (isset($medico->estado)) {if ($medico->estado === 'MT') { echo "selected"; }} ?>>Mato Grosso</option>
            <option value="MS" <?php if (isset($medico->estado)) {if ($medico->estado === 'MS') { echo "selected"; }} ?>>Mato Grosso do Sul</option>
            <option value="MG" <?php if (isset($medico->estado)) {if ($medico->estado === 'MG') { echo "selected"; }} ?>>Minas Gerais</option>
            <option value="PA" <?php if (isset($medico->estado)) {if ($medico->estado === 'PA') { echo "selected"; }} ?>>Pará</option>
            <option value="PB" <?php if (isset($medico->estado)) {if ($medico->estado === 'PB') { echo "selected"; }} ?>>Paraíba</option>
            <option value="PR" <?php if (isset($medico->estado)) {if ($medico->estado === 'PR') { echo "selected"; }} ?>>Paraná</option>
            <option value="PE" <?php if (isset($medico->estado)) {if ($medico->estado === 'PE') { echo "selected"; }} ?>>Pernambuco</option>
            <option value="PI" <?php if (isset($medico->estado)) {if ($medico->estado === 'PI') { echo "selected"; }} ?>>Piauí</option>
            <option value="RJ" <?php if (isset($medico->estado)) {if ($medico->estado === 'RJ') { echo "selected"; }} ?>>Rio de Janeiro</option>
            <option value="RN" <?php if (isset($medico->estado)) {if ($medico->estado === 'RN') { echo "selected"; }} ?>>Rio Grande do Norte</option>
            <option value="RS" <?php if (isset($medico->estado)) {if ($medico->estado === 'RS') { echo "selected"; }} ?>>Rio Grande do Sul</option>
            <option value="RO" <?php if (isset($medico->estado)) {if ($medico->estado === 'RO') { echo "selected"; }} ?>>Rondônia</option>
            <option value="RR" <?php if (isset($medico->estado)) {if ($medico->estado === 'RR') { echo "selected"; }} ?>>Roraima</option>
            <option value="SC" <?php if (isset($medico->estado)) {if ($medico->estado === 'SC') { echo "selected"; }} ?>>Santa Catarina</option>
            <option value="SP" <?php if (isset($medico->estado)) {if ($medico->estado === 'SP') { echo "selected"; }} ?>>São Paulo</option>
            <option value="SE" <?php if (isset($medico->estado)) {if ($medico->estado === 'SE') { echo "selected"; }} ?>>Sergipe</option>
            <option value="TO" <?php if (isset($medico->estado)) {if ($medico->estado === 'TO') { echo "selected"; }} ?>>Tocantins</option>
            <option value="EX" <?php if (isset($medico->estado)) {if ($medico->estado === 'EX') { echo "selected"; }} ?>>Estrangeiro</option>
          </select>
        </div>
      </div>      
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label for="medico-datanascimento">Data de Nascimento</label>
            <input type="text" class="form-control data-mask" name="data_nascimento" id="medico-datanascimento" value="<?=date('d/m/Y', strtotime($medico->data_nascimento));?>">
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="medico-datainscricao">Data Inscrição</label>
            <input type="text" class="form-control data-mask" name="data_inscricao" id="medico-datainscricao" value="<?=date('d/m/Y', strtotime($medico->data_inscricao));?>">
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="medico-createdat">Data Cadastro</label>
            <input type="text" class="form-control" name="data_cadastro" id="medico-createdat" value="<?=date('d/m/Y H:i:s', strtotime($medico->created_at));?>" disabled>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="medico-updateat">Data ultima atualização</label>
            <input type="text" class="form-control" name="update_at" id="medico-updateat" value="<?=date('d/m/Y H:i:s', strtotime($medico->updated_at));?>" disabled>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="medico-ativo">Ativo</label>
            <input type="text" class="form-control" name="email" id="medico-ativo" value="<?=sim_nao($medico->ativo)?>" disabled>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="medico-dadosconfirmado">Dados Confirmado</label>
            <input type="text" class="form-control" name="medico-confirmado" id="medico-dadosconfirmado" value="<?=sim_nao($medico->confirmado)?>"disabled>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="medico-status">Status Atual</label>
            <input type="text" class="form-control" id="medico-status" value="<?=$status['descricao']?>" disabled>
          </div>
        </div>
      </div>
      <?php if (($status['status'] == STATUS_MEDICO_COMPLETO) || ($status['status'] == STATUS_MEDICO_PAGAMENTO)) { ?>
        <h5>PAGAMENTO</h5>
        <div class="pagamento__medico">
          <p>Plano Ativo <strong><?=$medico->titulo?></strong> <?=($medico->valor == 0) ? '' : 'R$'. number_format($medico->valor, 2, ',', '.') .'/mês'?></p>
          <p>Quantidade de Documentos <strong><?=getQuantidadeDocumentosMes(Conexao::getInstance())['geral'] .'/'. $medico->quantidade_documentos?></strong></p>
          <?php if (($medico->paypal_agreemeent_id == '') || (($medico->paypal_data_cancelamento != '') && ($medico->paypal_data_cancelamento != '0000-00-00'))) { ?>
            <!-- <?php 
              if (($medico->paypal_data_cancelamento != '') && ($medico->paypal_data_cancelamento != '0000-00-00')) {
                $direitosUsoAposVencimento = isPeriodoUsoAposCancelamento($conexao);
                if ($direitosUsoAposVencimento['direto_uso']) { ?>
                  <p>Assinatura cancelada, direito de uso até <strong><?=implode('/',array_reverse(explode('-',$direitosUsoAposVencimento['proximo_vencimento'])))?></strong></p>
            <?php } } ?>
            <a href="<?=base_url()?>/painel/medicos/paypal.php" class="btn btn-primary button__alcancy"><i class="fas fa-dollar-sign"></i> Realizar Assinatura</a> -->
          <?php } else { ?>
            <p>Assinante desde <strong><?=implode('/',array_reverse(explode('-',$medico->paypal_data_assinatura)))?></strong></p>
            <!-- <?php if ($medico->paypal_agreemeent_id != 'FREE') { ?>
              <a href="<?=base_url()?>/painel/medicos/paypal.php" class="btn btn-danger button__alcancy"><i class="fab fa-creative-commons-nc"></i> Cancelar Assinatura</a>
            <?php } ?> -->
          <?php } ?>
          <button type="button" class="btn btn-success button__alcancy button__color__2" data-toggle="modal" data-target=".medico-planos-modal">Alterar meu Plano</button>
        </div>
      <?php } ?>
      <div class="float-right">
        <button type="submit" class="btn btn-primary button__alcancy"><i class="fas fa-check"></i> Salvar</button>
      </div>
      <div class="clearfix"></div>
    </form>  
  </div>

  <div class="modal fade medico-planos-modal" tabindex="-1" role="dialog" aria-labelledby="medicoPlanosModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Planos</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <img src="<?=base_url()?>/assets/images/paypal-logo.png">
          <button type="button" class="btn btn-sm btn-outline-primary d-block mt-2" data-toggle="popover" data-trigger="focus" title="Paypal" data-content="PayPal é uma empresa de pagamentos online mundial, de capital aberto na bolsa NASDAQ americana, muito conhecida no mundo online. Visando garantir a segurança nas transações escolhemos o PayPal para você nosso usuário.">
            <i class="fas fa-info"></i>
          </button>
          <div class="planos__display">
            <?php
              $query = ' SELECT planos.* '.
                      '   FROM planos '.
                      '  WHERE planos.id != :id ';

              $resultset = $conexao->prepare( $query );
              $resultset->bindParam(':id', $medico->plano_id);
              $resultset->execute();
              
              while($plano = $resultset->fetch(PDO::FETCH_OBJ)) {
            ?>
              <div class="plano__alteracao <?=$plano->recomendado? 'recomendado' : ''?>">
                <?php if ($plano->recomendado) { ?> <div class="plano-recomendado"><span>Recomendado<span></div> <?php } ?>
                <div class="plano-titulo"><?=$plano->titulo?></div>
                <div class="plano-documentos"><?=$plano->quantidade_documentos?> Documentos</div>
                <div class="plano-descricao">Atestados e Receitas</div>
                <div class="plano-valor"><?=$plano->valor == 0 ? 'Grátis' : '<span>R$</span>'. explode(',', number_format($plano->valor, 2, ',', '.'))[0] .'<span>,'. explode(',', number_format($plano->valor, 2, ',', '.'))[1] .'/mês</span>'?></div>
                <div class="plano-botoes"><a href="<?=base_url()?>/painel/medicos/paypal.php<?=$plano->valor == 0?'':'?plano_id='.$plano->id?>" class="btn btn-block btn-success button__alcancy">Selecionar</a></div>
              </div>
            <?php } ?>
          </div>          
        </div>
      </div>
    </div>
  </div>

<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
