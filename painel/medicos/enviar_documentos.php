<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php
  $conexao = Conexao::getInstance();
  $resultset = $conexao->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
  $resultset->bindParam(':cpf', $_SESSION['medico_cpf']);
  $resultset->execute();

  $medico = $resultset->fetchALL(PDO::FETCH_ASSOC);
  $randomletter = strtoupper(substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 4));
?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__medico.png" alt="">
    <h4>Documentos</h4>
  </div>
  <?php   
    if ($medico[0]['confirmacao_status'] == STATUS_MEDICO_ANALISE) {
      if (($medico[0]['file_selfie'] != '') && ($medico[0]['file_cpf'] != '') && ($medico[0]['file_cnh'] != '') && ($medico[0]['file_crm'] != '')) {
        echo '<div class="alert alert-warning" role="alert">';
        echo '  Seus documentos estão em Análise. Este processo pode demorar até 7 dias úteis. Aguarde a confirmação dos documentos para configurar sua assinatura e assim liberar a emissão de atestados e receitas.';
        echo '</div>';
      }
    }
  ?>  
  <div class="dashboard__wrapper">
    <form action="<?=base_url()?>/painel/medicos/upload_documentos.php" method="POST" enctype="multipart/form-data"  data-redirect="<?=base_url()?>/painel/medicos/enviar_documentos.php">
      <input type="hidden" name="codigo_selfie" value="<?=$randomletter?>">
      <input type="hidden" name="cpf" value="<?=$_SESSION['medico_cpf']?>">
      <div class="box__documentos">
        <div class="box__selfie <?=($medico[0]['file_selfie'] == '')?'nao__enviado':'enviado'?> shadow">
          <h4>Selfie</h4>
          <?php if ($medico[0]['file_selfie'] == '') { ?>
          <img src="<?=base_url()?>/assets/images//dashboard__documento__selfie.png" alt="">
          <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control documento__input__file" name="selfie" id="selfie">
          <button type="button" onclick="document.getElementById('selfie').click()" class="button__alcancy">Enviar Selfie</button>
          <?php } else { ?>
            <a href="<?=$medico[0]['file_selfie']?>" data-title="SELFIE" class="d-block link-medicos-documentos" target="_blank">
              <img src="<?=get_thumbnail_s3($medico[0]['file_selfie'])?>">
            </a>
            <?php if ($medico[0]['confirmacao_status'] != 'C') { ?>
              <?php if ($medico[0]['motivo_selfie'] != '') { ?>
                <span class="alcancy__alerta alcancy__alerta__bottom">
                  <?=$medico[0]['motivo_selfie']?>
                </span>
              <?php } ?>
              <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control documento__input__file" name="selfie" id="selfie">
              <button type="button" onclick="document.getElementById('selfie').click()" class="button__alcancy">Reenviar Selfie</button>
            <?php } ?>
          <?php }?>
        </div>
        <div class="box__cnh <?=($medico[0]['file_cnh'] == '')?'nao__enviado':'enviado'?> shadow">
          <h4>CNH/RG</h4>
          <?php if ($medico[0]['file_cnh'] == '') { ?>
            <img src="<?=base_url()?>/assets/images/dashboard__documento__CNH.png" alt="">
            <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control documento__input__file" name="cnh" id="cnh">
            <button type="button" onclick="document.getElementById('cnh').click()" class="button__alcancy">Enviar CNH/RG</button>
          <?php } else { ?>
            <a href="<?=$medico[0]['file_cnh']?>" data-title="CNH/RG" class="d-block link-medicos-documentos" target="_blank">
              <img src="<?=get_thumbnail_s3($medico[0]['file_cnh'])?>">
            </a>
            <?php if ($medico[0]['confirmacao_status'] != 'C') { ?>
              <?php if ($medico[0]['motivo_cnh'] != '') { ?>
                <span class="alcancy__alerta alcancy__alerta__bottom">
                  <?=$medico[0]['motivo_cnh']?>
                </span>
              <?php } ?>
              <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control documento__input__file" name="cnh" id="cnh">
              <button type="button" onclick="document.getElementById('cnh').click()" class="button__alcancy">Reenviar CNH/RG</button>              
            <?php } ?>
          <?php }?>
        </div>
        <div class="box__crm  <?=($medico[0]['file_crm'] == '')?'nao__enviado':'enviado'?> shadow">
          <h4>CRM - Frente</h4>
          <?php if ($medico[0]['file_crm'] == '') { ?>
            <img src="<?=base_url()?>/assets/images/dashboard__documento__CRM.png" alt="">
            <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control documento__input__file" name="crm" id="crm">
            <button type="button" onclick="document.getElementById('crm').click()"  class="button__alcancy">Enviar CRM - Frente</button>
          <?php } else { ?>
            <a href="<?=$medico[0]['file_crm']?>" data-title="CRM - Frente" class="d-block link-medicos-documentos" target="_blank">
              <img src="<?=get_thumbnail_s3($medico[0]['file_crm'])?>">
            </a>
            <?php if ($medico[0]['confirmacao_status'] != 'C') { ?>
              <?php if ($medico[0]['motivo_crm'] != '') { ?>
                <span class="alcancy__alerta alcancy__alerta__bottom">
                  <?=$medico[0]['motivo_crm']?>
                </span>
              <?php } ?>
              <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control documento__input__file" name="crm" id="crm">
              <button type="button" onclick="document.getElementById('crm').click()"  class="button__alcancy">Reenviar CRM - Frente</button>              
            <?php } ?>
          <?php }?>
        </div>
        <div class="box__cpf <?=($medico[0]['file_cpf'] == '')?'nao__enviado':'enviado'?> shadow">
          <h4>CRM - Verso</h4>
          <?php if ($medico[0]['file_cpf'] == '') { ?>
            <img src="<?=base_url()?>/assets/images//dashboard__documento__CPF.png" alt="">
            <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control documento__input__file" name="cpf" id="cpf">
            <button type="button" onclick="document.getElementById('cpf').click()" class="button__alcancy">Enviar CRM - Verso</button>
          <?php } else { ?>
            <a href="<?=$medico[0]['file_cpf']?>" data-title="CRM - Verso" class="d-block link-medicos-documentos" target="_blank">
              <img src="<?=get_thumbnail_s3($medico[0]['file_cpf'])?>">
            </a>
            <?php if ($medico[0]['confirmacao_status'] != 'C') { ?>
              <?php if ($medico[0]['motivo_cpf'] != '') { ?>
                <span class="alcancy__alerta alcancy__alerta__bottom">
                  <?=$medico[0]['motivo_cpf']?>
                </span>
              <?php } ?>
              <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control documento__input__file" name="cpf" id="cpf">
              <button type="button" onclick="document.getElementById('cpf').click()" class="button__alcancy">Reenviar CRM - Verso</button>              
            <?php } ?>
          <?php }?>
        </div>
      </div>
    </form>
    <div class="clearfix"></div>
  </div>  
  <!-- <div class="container">
    <?=show_alert('FAIL', 'Flaha ao enviar os documentos, tente novamente. São permitidos apenas o envio de imagens.')?>
    <?=show_alert('OK', 'Documentos enviados com sucesso. Aguarde até seu que seu acesso seja liberado.')?>
    <div class="card">
      <h5 class="card-header">Enviar Documentos</h5>
      <div class="card-body">
        <form action="<?=base_url()?>/painel/medicos/upload_documentos.php" method="POST" enctype="multipart/form-data"  data-redirect="<?=base_url()?>/painel/medicos/enviar_documentos.php">
          <input type="hidden" name="codigo_selfie" value="<?=$randomletter?>">
          <input type="hidden" name="cpf" value="<?=$_SESSION['medico_cpf']?>">
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Selfie</label>
            <div class="col-sm-10">
              <?php if ($medico[0]['file_selfie'] == '') { ?>
                <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control" name="selfie">
                <small id="passwordHelpBlock" class="form-text text-muted">
                  Informe as seguintes letras na selfie <strong><?=$randomletter?></strong>.
                </small>
              <?php } else { ?>
                <a href="<?=$medico[0]['file_selfie']?>" data-title="Selfie" class="d-block link-medicos-documentos" target="_blank">
                  <img src="<?=get_thumbnail_s3($medico[0]['file_selfie'])?>">
                </a>
              <?php } ?>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">CNH</label>
            <div class="col-sm-10">
              <?php if ($medico[0]['file_cnh'] == '') { ?>
                <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control" name="cnh">
              <?php } else { ?>
                <a href="<?=$medico[0]['file_cnh']?>" data-title="CNH" class="d-block link-medicos-documentos" target="_blank">
                  <img src="<?=get_thumbnail_s3($medico[0]['file_cnh'])?>">
                </a>
              <?php } ?>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">CPF</label>
            <div class="col-sm-10">
              <?php if ($medico[0]['file_cpf'] == '') { ?>
                <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control" name="cpf">
              <?php } else { ?>
                <a href="<?=$medico[0]['file_cpf']?>" data-title="CPF" class="d-block link-medicos-documentos" target="_blank">
                  <img src="<?=get_thumbnail_s3($medico[0]['file_cpf'])?>">
                </a>
              <?php } ?>
            </div>
          </div>
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">CRM</label>
            <div class="col-sm-10">
              <?php if ($medico[0]['file_crm'] == '') { ?>
                <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control" name="crm">
              <?php } else { ?>
                <a href="<?=$medico[0]['file_crm']?>" data-title="CRM" class="d-block link-medicos-documentos" target="_blank">
                  <img src="<?=get_thumbnail_s3($medico[0]['file_crm'])?>">
                </a>
              <?php } ?>
            </div>
          </div>
          <div class="float-right">
            <?php if (($medico[0]['file_selfie'] == '') || ($medico[0]['file_cnh'] == '') || ($medico[0]['file_cpf'] == '') || ($medico[0]['file_crm'] == '') || ($medico[0]['file_crm'] == '')) { ?>
              <button type="submit" class="btn btn-primary">Enviar</button>
            <?php } ?>
          </div>
        </form>
      </div>
    </div>
  </div> -->
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
