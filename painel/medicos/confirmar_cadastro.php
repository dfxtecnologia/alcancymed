<?php if (!verifica_confirmacao(Conexao::getInstance(), 'medico', $_SESSION['medico_cpf'])) { ?>
  <div class="alert alert-secondary" role="alert">
    <?php if (!arquivos_enviados(Conexao::getInstance(), 'medico', $_SESSION['medico_cpf'])) { ?>
      <p class="mb-0"><a href="<?=base_url()?>/painel/medicos/enviar_documentos.php">Clique aqui</a> para enviar os documentos para confirmação.</p>
    <?php } else { ?>
      <p class="mb-0">Status da confirmação de cadastro: <strong><?=getStatusConfirmacao(Conexao::getInstance(), 'medico', $_SESSION['medico_cpf'])?></strong> <a href="<?=base_url()?>/painel/medicos/enviar_documentos.php">Clique aqui</a> para visualizar os documentos enviados.</p>
    <?php } ?>
  </div>
<?php } ?>
