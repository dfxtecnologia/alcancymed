<?php require_once(dirname(dirname(__DIR__)).'/sys/functions.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/sys/conexao.php'); ?>
<?php require_once(dirname(dirname(__DIR__)).'/paypal/functions.php') ?>
<?php 
  if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
     throw new Exception('Request method must be POST!');
  }
  
  //Make sure that the content type of the POST request has been set to application/json
  $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
  if (strcasecmp($contentType, 'application/json') != 0) {
    throw new Exception('Content type must be: application/json');
  }
  
  //Receive the RAW post data.
  $content = trim(file_get_contents("php://input"));
  
  //Attempt to decode the incoming RAW post data from JSON.
  $decoded = json_decode($content, true);
  
  //If json_decode failed, the JSON is invalid.
  if(!is_array($decoded)){
    throw new Exception('Received content contained invalid JSON!');
  }
  
  if (($decoded['event_type'] == 'BILLING.SUBSCRIPTION.CANCELLED') || ($decoded['event_type'] == 'BILLING.SUBSCRIPTION.SUSPENDED')) {
    $conexao = Conexao::getInstance();

    $resultset = $conexao->prepare('SELECT medicos.*, cidades.nome as cidade FROM medicos INNER JOIN cidades ON (cidades.id = medicos.cidade_id) WHERE paypal_agreemeent_id = :paypal_agreemeent_id');
    $resultset->bindParam(':paypal_agreemeent_id', $decoded['resource']['id']);
    $resultset->execute();

    $medico = $resultset->fetch(PDO::FETCH_OBJ);

    $conexao->beginTransaction();
    try {
      $agreement = paypalCancelAgreement($apiContext, $medico->paypal_agreemeent_id);

      // $medicos = $conexao->prepare('UPDATE medicos SET paypal_data_cancelamento = CURDATE() WHERE id = :id');
      $medicos = $conexao->prepare('UPDATE medicos SET paypal_agreemeent_id = \'FREE\', paypal_data_cancelamento = null, plano_id = 1 WHERE id = :id');
      $medicos->bindParam(':id', $medico->id);
      $medicos->execute();

      setPaypalHistorico($conexao, $_SESSION['medico_id'], (($decoded['event_type'] == 'BILLING.SUBSCRIPTION.CANCELLED') ? 'C' : 'S'), $decoded['resource']['id']);

      $conexao->commit();
      // $response = sendEmail($medico->email, $medico->nome, 'Assinatura cancelada AlcancyMED', '<p>Sua assinatura do sistema AlcancyMED foi cancelada com sucesso.</p>');
    } catch (Exception $e) {
      $conexao->rollBack();
      echo $e->getMessage();
    }
  } else if ($decoded['event_type'] == 'BILLING.SUBSCRIPTION.RE-ACTIVATED') { 
    $conexao = Conexao::getInstance();

    $resultset = $conexao->prepare('SELECT medicos.*, cidades.nome as cidade FROM medicos INNER JOIN cidades ON (cidades.id = medicos.cidade_id) WHERE paypal_agreemeent_id = :paypal_agreemeent_id');
    $resultset->bindParam(':paypal_agreemeent_id', $decoded['resource']['id']);
    $resultset->execute();

    
    $medico = $resultset->fetch(PDO::FETCH_OBJ);
    
    $conexao->beginTransaction();
    try {
      $agreement = paypalCancelAgreement($apiContext, $medico->paypal_agreemeent_id);
      
      $medicos = $conexao->prepare('UPDATE medicos SET paypal_data_assinatura = CURDATE(), paypal_data_cancelamento = null WHERE id = :id');
      $medicos->bindParam(':id', $medico->id);
      $medicos->execute();
      setPaypalHistorico($conexao, $_SESSION['medico_id'], 'R', $decoded['resource']['id']);
      
      $conexao->commit();
      $response = sendEmail($medico->email, $medico->nome, 'Assinatura confirmada AlcancyMED', '<p>Sua assinatura do sistema AlcancyMED foi confirmada com sucesso.</p>');
    } catch (Exception $e) {
      $conexao->rollBack();
      echo $e->getMessage();
    }
  }
 
