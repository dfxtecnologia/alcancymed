<?php 
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $conexao->beginTransaction();
    try {          
      $medico = $conexao->prepare('UPDATE medicos set nome = :nome, data_inscricao = :data_inscricao, data_nascimento = :data_nascimento, email = :email, telefone_residencial = :telefone_residencial, telefone_celular = :telefone_celular, cep = :cep, logradouro = :logradouro,numero = :numero, bairro = :bairro, cidade_id = :cidade_id, estado = :estado, updated_at = :updated_at WHERE id = :id');
      
      $medico->bindParam(':nome', $_POST['nome']);
      $medico->bindValue(':data_inscricao', converte_data($_POST['data_inscricao']));
      $medico->bindValue(':data_nascimento', converte_data($_POST['data_nascimento']));
      $medico->bindParam(':email', $_POST['email']);
      $medico->bindParam(':telefone_residencial', $_POST['telefone_residencial']);
      $medico->bindParam(':telefone_celular', $_POST['telefone_celular']);
      $medico->bindParam(':cep', $_POST['cep']);
      $medico->bindParam(':logradouro', $_POST['logradouro']);
      $medico->bindParam(':numero', $_POST['numero']);
      $medico->bindParam(':bairro', $_POST['bairro']);
      $medico->bindParam(':cidade_id', $_POST['cidade']);
      $medico->bindParam(':estado', $_POST['estado']);      
      $medico->bindValue(':updated_at', date('Y-m-d H:i:s'));
      $medico->bindParam(':id', $_POST['medico_id']);
      $medico->execute();


      $conexao->commit();

      echo json_encode(Array('status' => "OK"));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }