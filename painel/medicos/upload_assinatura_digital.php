<?php
  session_start();
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');
  
    $uploaddir = dirname(dirname(__DIR__)) .'/arquivos/medicos/';
    if(!is_dir($uploaddir)) {
      mkdir($uploaddir);
    }

    $file_width = '300';
    $thumbnail_width = '100';

    if (isSet($_FILES['assinatura'])) {
      $assinatura = explode('.', $_FILES['assinatura']['name']);
      $assinatura_file = md5(uniqid(rand(), true)) . '.' . end($assinatura);
      $ext = pathinfo($assinatura_file, PATHINFO_EXTENSION);
      $filename = pathinfo($assinatura_file, PATHINFO_FILENAME);
      $thumbnail = $filename . '_t.' . $ext;
      make_thumb($_FILES['assinatura']['tmp_name'], $uploaddir . $assinatura_file, $file_width);
      make_thumb($_FILES['assinatura']['tmp_name'], $uploaddir . $thumbnail, $thumbnail_width);
      $assinatura_file = upload_to_s3($uploaddir . $assinatura_file);
      upload_to_s3($uploaddir . $thumbnail);
      $assinatura_motivo = '';
      $link = $assinatura_file;
      $documento = 'assinatura';
    }

    // var_dump($_POST);
    // die();
    $conexao = Conexao::getInstance();
    $conexao->beginTransaction();  
    try {
      $medicos = $conexao->prepare('UPDATE medicos SET file_assinatura_digital = :file_assinatura_digital WHERE cpf = :cpf');
      $medicos->bindParam(':file_assinatura_digital', $assinatura_file);
      $medicos->bindParam(':cpf', $_SESSION['medico_cpf']);
      $medicos->execute();

      $historico = $conexao->prepare('INSERT INTO documento_historicos (medico_id, acao, documento, link) VALUES (:medico_id, \'U\', :documento, :link)');
      $historico->bindParam(':medico_id', $_SESSION['medico_id']);
      $historico->bindParam(':documento', $documento);
      $historico->bindParam(':link', $link);
      $historico->execute();

      $conexao->commit();

      echo json_encode(Array('status' => 'OK'));
    }catch (PDOException $e) {
      $conexao->rollBack();
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }