<?php
  session_start();
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $arquivos_validos = TRUE;

    if (isSet($_FILES['selfie'])) {
      $arquivos_validos = ($arquivos_validos && isImage($_FILES['selfie']));
    }

    if (isSet($_FILES['cnh'])) {
      $arquivos_validos =  ($arquivos_validos && isImage($_FILES['cnh']));
    }

    if (isSet($_FILES['cpf'])) {
      $arquivos_validos =  ($arquivos_validos && isImage($_FILES['cpf']));
    }

    if (isSet($_FILES['crm'])) {
      $arquivos_validos =  ($arquivos_validos && isImage($_FILES['crm']));
    }

    if (!$arquivos_validos) {
      echo json_encode(Array('status' => 'FAIL', 'message' => 'Arquivos enviados não são válidos.'));
      exit();
    }

    $uploaddir = dirname(dirname(__DIR__)) .'/arquivos/medicos/';

    if(!is_dir($uploaddir)) {
      mkdir($uploaddir);
    }

    $conexao = Conexao::getInstance();
    $resultset = $conexao->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
    $resultset->bindParam(':cpf', $_POST['cpf']);
    $resultset->execute();

    $medico = $resultset->fetch(PDO::FETCH_OBJ);

    $selfie_file = $medico->file_selfie;
    $cnh_file    = $medico->file_cnh;
    $cpf_file    = $medico->file_cpf;
    $crm_file    = $medico->file_crm;

    $selfie_motivo = $medico->motivo_selfie;
    $cnh_motivo    = $medico->motivo_cnh;
    $cpf_motivo    = $medico->motivo_cpf;
    $crm_motivo    = $medico->motivo_crm;

    $link = '';
    $documento = '';


    $file_width = '300';
    $thumbnail_width = '100';

    if (isSet($_FILES['selfie'])) {
      $selfie = explode('.', $_FILES['selfie']['name']);
      $selfie_file = md5(uniqid(rand(), true)) . '.' . end($selfie);
      $ext = pathinfo($selfie_file, PATHINFO_EXTENSION);
      $filename = pathinfo($selfie_file, PATHINFO_FILENAME);
      $thumbnail = $filename . '_t.' . $ext;
      make_thumb($_FILES['selfie']['tmp_name'], $uploaddir . $selfie_file, $file_width);
      make_thumb($_FILES['selfie']['tmp_name'], $uploaddir . $thumbnail, $thumbnail_width);
      $selfie_file = upload_to_s3($uploaddir . $selfie_file);
      upload_to_s3($uploaddir . $thumbnail);
      $selfie_motivo = '';
      $link = $selfie_file;
      $documento = 'selfie';
    }

    if (isSet($_FILES['cnh'])) {
      $cnh = explode('.', $_FILES['cnh']['name']);
      $cnh_file = md5(uniqid(rand(), true)) . '.' . end($cnh);
      $ext = pathinfo($cnh_file, PATHINFO_EXTENSION);
      $filename = pathinfo($cnh_file, PATHINFO_FILENAME);
      $thumbnail = $filename . '_t.' . $ext;
      make_thumb($_FILES['cnh']['tmp_name'], $uploaddir . $cnh_file, $file_width);
      make_thumb($_FILES['cnh']['tmp_name'], $uploaddir . $thumbnail, $thumbnail_width);      
      $cnh_file = upload_to_s3($uploaddir . $cnh_file);
      upload_to_s3($uploaddir . $thumbnail);
      $cnh_motivo = '';
      $link = $cnh_file;
      $documento = 'cnh';
    }

    if (isSet($_FILES['cpf'])) {
      $cpf = explode('.', $_FILES['cpf']['name']);
      $cpf_file = md5(uniqid(rand(), true)) . '.' . end($cpf);
      $ext = pathinfo($cpf_file, PATHINFO_EXTENSION);
      $filename = pathinfo($cpf_file, PATHINFO_FILENAME);
      $thumbnail = $filename . '_t.' . $ext;
      make_thumb($_FILES['cpf']['tmp_name'], $uploaddir . $cpf_file, $file_width);
      make_thumb($_FILES['cpf']['tmp_name'], $uploaddir . $thumbnail, $thumbnail_width); 
      $cpf_file = upload_to_s3($uploaddir . $cpf_file);
      upload_to_s3($uploaddir . $thumbnail);
      $cpf_motivo = '';
      $link = $cpf_file;
      $documento = 'crm - verso';
    }

    if (isSet($_FILES['crm'])) {
      $crm = explode('.', $_FILES['crm']['name']);
      $crm_file = md5(uniqid(rand(), true)) . '.' . end($crm);
      $ext = pathinfo($crm_file, PATHINFO_EXTENSION);
      $filename = pathinfo($crm_file, PATHINFO_FILENAME);
      $thumbnail = $filename . '_t.' . $ext;
      make_thumb($_FILES['crm']['tmp_name'], $uploaddir . $crm_file, $file_width);
      make_thumb($_FILES['crm']['tmp_name'], $uploaddir . $thumbnail, $thumbnail_width);
      $crm_file = upload_to_s3($uploaddir . $crm_file);
      upload_to_s3($uploaddir . $thumbnail);
      $crm_motivo = '';
      $link = $crm_file;
      $documento = 'crm - frente';
    }

    $conexao->beginTransaction();

    try {
      $medicos = $conexao->prepare('UPDATE medicos SET file_selfie = :file_selfie, file_cnh = :file_cnh, file_crm = :file_crm, file_cpf = :file_cpf, codigo_selfie = :codigo_selfie, motivo_selfie = :motivo_selfie, motivo_cnh = :motivo_cnh, motivo_cpf = :motivo_cpf, motivo_crm = :motivo_crm, confirmacao_status = \'A\' WHERE cpf = :cpf');
      $medicos->bindParam(':file_selfie',   $selfie_file);
      $medicos->bindParam(':file_crm',      $crm_file);
      $medicos->bindParam(':file_cnh',      $cnh_file);
      $medicos->bindParam(':file_cpf',      $cpf_file);
      $medicos->bindParam(':motivo_selfie', $selfie_motivo);
      $medicos->bindParam(':motivo_crm',    $crm_motivo);
      $medicos->bindParam(':motivo_cnh',    $cnh_motivo);
      $medicos->bindParam(':motivo_cpf',    $cpf_motivo);
      $medicos->bindParam(':codigo_selfie', $_POST['codigo_selfie']);
      $medicos->bindParam(':cpf', $_POST['cpf']);
      $medicos->execute();

      $historico = $conexao->prepare('INSERT INTO documento_historicos (medico_id, acao, documento, link) VALUES (:medico_id, \'U\', :documento, :link)');
      $historico->bindParam(':medico_id', $_SESSION['medico_id']);
      $historico->bindParam(':documento', $documento);
      $historico->bindParam(':link', $link);
      $historico->execute();

      $conexao->commit();

      $usuarios = $conexao->prepare('SELECT nome, email FROM usuarios');
      $usuarios->execute();

      $email  = '';
      $email .= '<p>Acesso <a href="'.base_url().'/root">painel ROOT</a> para aprovar os documentos enviados</p>';
      $email .= '<p>'. $_SESSION['medico_nome'] .' enviou '. $documento .'</p>';

      while($usuario = $usuarios->fetch(PDO::FETCH_OBJ)) { 
        sendEmail($usuario->email, $usuario->nome, $_SESSION['medico_nome'] .' enviou '. $documento.' para aprovação AlcancyMED', $email);
      }

      echo json_encode(Array('status' => 'OK'));
    }catch (PDOException $e) {
      $conexao->rollBack();
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }
