<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php
  $conexao = Conexao::getInstance();
  $resultset = $conexao->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
  $resultset->bindParam(':cpf', $_SESSION['medico_cpf']);
  $resultset->execute();

  $medico = $resultset->fetch(PDO::FETCH_OBJ);
?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__medico.png" alt="">
    <h4>Assinatura Digital</h4>
  </div>
  <div class="dashboard__wrapper">    
    <div id="assinatura" data-image="<?=$medico->file_assinatura_digital?>">
      <canvas></canvas>
    </div>
    <div>
      <button id="btn-assinatura-digital-on" class="btn btn-primary button__alcancy float-right" data-url="<?=base_url()?>/painel/medicos/upload_assinatura_digital.php"><i class="fas fa-check"></i> Salvar</button>
      <button id="btn-assinatura-digital-clear" class="btn btn-outline-danger float-right"><i class="fas fa-eraser"></i> Limpar</button>
    </div>
    <div class="clearfix"></div>
  </div>  
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
