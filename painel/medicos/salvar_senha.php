<?php 
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $validaMedico = $conexao->prepare('SELECT * FROM medicos WHERE id = :id');
    $validaMedico->bindParam(':id', $_POST['medico_id']);
    $validaMedico->execute();

    $medico = $validaMedico->fetch(PDO::FETCH_OBJ);

    if ($medico->senha != md5($_POST['senha-atual'])) {
      echo json_encode(Array('status' => 'FAIL', 'message' => 'Senha atual não confere.'));
      exit();
    }

    if (md5($_POST['nova-senha']) != md5($_POST['repetir-senha'])) {
      echo json_encode(Array('status' => 'FAIL', 'message' => 'Novas senhas informadas não conferem.'));
      exit();
    }    

    $conexao->beginTransaction();
    try {
      $atualizaMedico = $conexao->prepare('UPDATE medicos set senha = :senha WHERE id = :id');
      
      $atualizaMedico->bindParam(':senha', md5($_POST['nova-senha']));
      $atualizaMedico->bindParam(':id', $_POST['medico_id']);
      $atualizaMedico->execute();

      $conexao->commit();
      $response = sendEmail($medico->email, $medico->nome, 'Senha Alterada AlcancyMED', '<p>Sua senha de acesso no AlcancyMED foi alterada.</p>');
      echo json_encode(Array('status' => "OK", 'message' => 'Senha alterada com sucesso.'));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }