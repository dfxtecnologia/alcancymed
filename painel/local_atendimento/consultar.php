<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php
  $localatendimento = isset($_GET['local-atendimento']) ? trim(strtoupper($_GET['local-atendimento'])) : '';
  $logradouro       = isset($_GET['logradouro']) ? trim(strtoupper($_GET['logradouro'])) : '';
  $cidade           = isset($_GET['cidade']) ? trim(strtoupper($_GET['cidade'])) : '';

  $link = '';

  if (($localatendimento != '') || ($logradouro != '') || ($cidade != '')) {
    $link = '&local-atendimento='.$localatendimento.'&logradouro='.$logradouro.'&cidade='.$cidade;
  }
?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__locais__atendimento.png" alt="">
    <h4>Locais de Atendimento</h4>
  </div>
  <div class="dashboard__wrapper">
    <div class="filtro">
      <div class="filtro__titulo">
        <i class="fas fa-filter"></i>
        <h4>Filtrar por:</h4>
      </div>
      <div class="filtro__dados">
        <form class="form-inline" method="GET">
          <input type="text" class="form-control mr-2" name="local-atendimento" id="local-atendimento" placeholder="Local de Atendimento" value="<?=$localatendimento?>">
          <input type="text" class="form-control mr-2" name="logradouro" id="logradouro" placeholder="Logradouro" value="<?=$logradouro?>">
          <input type="text" class="form-control mr-2" name="cidade" id="cidade" placeholder="Cidade" value="<?=$cidade?>">
          <button type="submit" class="btn btn-primary button__alcancy">Filtrar</button>
          <button type="button" class="close button__alcancy__close__filtro" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </form>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-borderless table-striped datatable__alcancy">
        <thead>
          <tr>
            <th></th>
            <th>Local Atendimentos</th>
            <th>Logradouro</th>
            <th>Cidade</th>
            <th>Telefone</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php
            $conexao = Conexao::getInstance();
            $query = ' SELECT COUNT(consultorio_medicos.id) total '.
                     '  FROM consultorio_medicos '.                     
                     '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.                     
                     '  INNER JOIN consultorios ON (consultorio_medicos.consultorio_id = consultorios.id) '.
                     '  INNER JOIN cidades ON (consultorios.cidade_id = cidades.id )'.
                     '  WHERE medicos.cpf = :cpf';
            
            if ($localatendimento != '') {
              $query .= ' AND UPPER(consultorios.descricao) LIKE \'%'. $localatendimento .'%\'';
            }
            if ($logradouro != '') {
              $query .= ' AND UPPER(consultorios.logradouro) LIKE \'%'. $logradouro .'%\'';
            }
            if ($cidade != '') {
              $query .= ' AND UPPER(cidades.nome) LIKE \'%'. $cidade .'%\'';
            }

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':cpf', $_SESSION['medico_cpf']);
            $resultset->execute();
            $consultorio = $resultset->fetch(PDO::FETCH_OBJ);

            $total_registros = $consultorio->total;
            $total_paginas   = ceil($total_registros / PAGE_LIMIT);            
            $pagina          = isSet($_GET['pagina']) ? $_GET['pagina'] : 1;
            $limite          = ($pagina - 1) * PAGE_LIMIT;

            $query = ' SELECT consultorios.*, cidades.nome AS cidade '.
                     '   FROM consultorio_medicos '.
                     '  INNER JOIN consultorios ON (consultorio_medicos.consultorio_id = consultorios.id) '.
                     '  INNER JOIN cidades ON (consultorios.cidade_id = cidades.id) '.
                     '  WHERE consultorio_medicos.medico_id = :id ';
             
            if ($localatendimento != '') {
              $query .= ' AND UPPER(consultorios.descricao) LIKE \'%'. $localatendimento .'%\'';
            }
            if ($logradouro != '') {
              $query .= ' AND UPPER(consultorios.logradouro) LIKE \'%'. $logradouro .'%\'';
            }
            if ($cidade != '') {
              $query .= ' AND UPPER(cidades.nome) LIKE \'%'. $cidade .'%\'';
            }

            $query .= '  ORDER BY consultorios.descricao '.
                      '  LIMIT '. $limite .','. PAGE_LIMIT;

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':id', $_SESSION['medico_id']);
            $resultset->execute();
            while($consultorio = $resultset->fetch(PDO::FETCH_OBJ)) { ?>            
              <tr>
                <td>
                  <?php if ($consultorio->file_logo != '') { ?>
                    <img src="<?=get_thumbnail_s3($consultorio->file_logo)?>" width="50px" height="50px">
                  <?php } ?>            
                </td>
                <td><?=$consultorio->descricao?></td>
                <td><?=$consultorio->logradouro?>,<?=$consultorio->numero?></td>
                <td><?=$consultorio->cidade?></td>
                <td><?=$consultorio->telefone?></td>
                <td><?=($consultorio->ativo == 0)? 'Cancelado':'Ativo'?></td>
                <td>
                  <?php if ($consultorio->ativo) { ?>
                    <a href="<?=base_url()?>/painel/local_atendimento/cadastrar.php?id=<?=$consultorio->id?>" data-chave="<?=$consultorio->id?>" class="local-cancelar"><i class="fas fa-edit"></i></a>
                  <?php } ?>
                </td>
              </tr>
            <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="float-right">
      <a href="<?=base_url()?>/painel/local_atendimento/cadastrar.php" class="btn btn-primary button__alcancy">
        <i class="fas fa-file-alt"></i> Novo
      </a>
    </div>
    <div class="clearfix"></div>
    <nav aria-label="Page navigation example">
      <small>página <?=$pagina?> de <?=$total_paginas?> - <?=$total_registros?> locais de atendimento</small>
      <ul class="pagination justify-content-end">
        <?php for($i = 1; $i <= $total_paginas; $i++) { ?>
          <li class="page-item <?=($pagina==$i)?'active':''?>">
            <a class="page-link" href="<?=base_url()?>/painel/local_atendimento/consultar.php?pagina=<?=$i.$link?>"><?=$i?></a>
          </li>
        <?php } ?>
      </ul>
    </nav>
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
