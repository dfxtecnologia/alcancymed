<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__locais__atendimento.png" alt="">
    <h4>Local de Atendimento</h4>
  </div>
  <div class="dashboard__wrapper">
    <form class="form__alcancy" action="<?=base_url()?>/painel/local_atendimento/efetivar_cadastro.php" method="POST" data-redirect="<?=base_url()?>/painel/local_atendimento/consultar.php">
      <input type="hidden" name="medico_id" value="<?=$_SESSION['medico_id']?>">
      <?php
        if (isset($_GET['id'])) {
          $conexao = Conexao::getInstance();

          $query  = ' SELECT consultorios.*, cidades.nome AS cidade_nome ';
          $query .= '   FROM consultorios ';
          $query .= '   LEFT JOIN cidades ON (cidades.id = consultorios.cidade_id )';
          $query .= '  WHERE consultorios.id = :id';
          $LocalQry = $conexao->prepare($query);
          $LocalQry->bindValue(':id', $_GET['id']);
          $LocalQry->execute();
          $local = $LocalQry->fetch(PDO::FETCH_OBJ);
      ?>
      <input type="hidden" name="local_id" value="<?=$local->id?>">
      <?php } else { ?>
        <input type="hidden" name="local_id" value="0">
      <?php } ?>
      <div class="form-row">
        <div class="form-group col-md-2">
          <input type="file" accept="image/x-png,image/gif,image/jpeg" class="form-control file-style" name="logo" id="local-logo">
          <label for="local-logo">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
              <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
            </svg>
            <span>Logo</span>
            <?php if (isset($local->file_logo) && ($local->file_logo != '')) { ?>
              <img src="<?=get_thumbnail_s3($local->file_logo)?>" width="50px" height="50px">
            <?php } ?>
          </label>
        </div>          
        <div class="form-group col-md-6">
          <label for="local-descricao">Nome do Local</label>
          <input type="text" class="form-control" name="descricao" id="local-descricao" aria-describedby="nome-local-ajuda" value="<?=(isset($local->descricao))?$local->descricao:''?>">
          <small id="nome-local-ajuda" class="form-text text-muted">Exemplo: Consultório, UPA, AME, Santa Casa</small>
        </div>
        <div class="form-group col-md-2">
          <label for="local-telefone">Telefone </label>
          <input type="text" class="form-control telefone-mask" placeholder="Telefone" name="telefone" id="local-telefone" value="<?=(isset($local->telefone))?$local->telefone:''?>">
        </div>
        <div class="form-group col-md-2">
          <label for="local-ativo">Ativo</label>
          <select class="form-control select2-basic" name="ativo" id="local-ativo" required>
            <option></option>
            <option value="1" <?php if (isset($local->ativo)) {if ($local->ativo == "1") { echo "selected"; }} ?>>Sim</option>
            <option value="0" <?php if (isset($local->ativo)) {if ($local->ativo == "0") { echo "selected"; }} ?>>Não</option>
          </select>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="local-cep">CEP</label>
          <input type="text" class="form-control busca-cep cep-mask" placeholder="CEP" name="cep" id="local-cep" value="<?=(isset($local->cep))?$local->cep:''?>">
        </div>
        <div class="form-group col-md-6">
          <label for="local-logradouro">Logradouro</label>
          <input type="text" class="form-control busca-cep-rua" placeholder="Logradouro" name="logradouro" id="local-logradouro" value="<?=(isset($local->logradouro))?$local->logradouro:''?>">
        </div>
        <div class="form-group col-md-2">
          <label for="local-numero">Número</label>
          <input type="text" class="form-control busca-cep-numero" placeholder="Número" name="numero" id="local-numero" value="<?=(isset($local->numero))?$local->numero:''?>">
        </div>        
        <div class="form-group col-md-2">
          <label for="local-bairro">Bairro</label>
          <input type="text" class="form-control busca-cep-bairro" placeholder="Bairro" name="bairro" id="local-bairro" value="<?=(isset($local->bairro))?$local->bairro:''?>">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="local-cidade">Cidade</label>
          <select class="form-control select2-basic-search busca-cep-cidade" name="cidade" id="cidade" data-search-url="<?=base_url()?>/rest/cidades.php" value="<?=isSet($local)?$local->cidade_id:''?>" required>
            <?php if (isSet($local->cidade_id)) { ?><option value="<?=$local->cidade_id?>"><?=$local->cidade_nome?></option><?php } ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="local-estado">Estado</label>
          <select class="form-control select2-basic busca-cep-estado" name="estado" id="local-estado" required>
            <option></option>
            <option value="AC" <?php if (isset($local->estado)) {if ($local->estado === 'AC') { echo "selected"; }} ?>>Acre</option>
            <option value="AL" <?php if (isset($local->estado)) {if ($local->estado === 'AL') { echo "selected"; }} ?>>Alagoas</option>
            <option value="AP" <?php if (isset($local->estado)) {if ($local->estado === 'AP') { echo "selected"; }} ?>>Amapá</option>
            <option value="AM" <?php if (isset($local->estado)) {if ($local->estado === 'AM') { echo "selected"; }} ?>>Amazonas</option>
            <option value="BA" <?php if (isset($local->estado)) {if ($local->estado === 'BA') { echo "selected"; }} ?>>Bahia</option>
            <option value="CE" <?php if (isset($local->estado)) {if ($local->estado === 'CE') { echo "selected"; }} ?>>Ceará</option>
            <option value="DF" <?php if (isset($local->estado)) {if ($local->estado === 'DF') { echo "selected"; }} ?>>Distrito Federal</option>
            <option value="ES" <?php if (isset($local->estado)) {if ($local->estado === 'ES') { echo "selected"; }} ?>>Espírito Santo</option>
            <option value="GO" <?php if (isset($local->estado)) {if ($local->estado === 'GO') { echo "selected"; }} ?>>Goiás</option>
            <option value="MA" <?php if (isset($local->estado)) {if ($local->estado === 'MA') { echo "selected"; }} ?>>Maranhão</option>
            <option value="MT" <?php if (isset($local->estado)) {if ($local->estado === 'MT') { echo "selected"; }} ?>>Mato Grosso</option>
            <option value="MS" <?php if (isset($local->estado)) {if ($local->estado === 'MS') { echo "selected"; }} ?>>Mato Grosso do Sul</option>
            <option value="MG" <?php if (isset($local->estado)) {if ($local->estado === 'MG') { echo "selected"; }} ?>>Minas Gerais</option>
            <option value="PA" <?php if (isset($local->estado)) {if ($local->estado === 'PA') { echo "selected"; }} ?>>Pará</option>
            <option value="PB" <?php if (isset($local->estado)) {if ($local->estado === 'PB') { echo "selected"; }} ?>>Paraíba</option>
            <option value="PR" <?php if (isset($local->estado)) {if ($local->estado === 'PR') { echo "selected"; }} ?>>Paraná</option>
            <option value="PE" <?php if (isset($local->estado)) {if ($local->estado === 'PE') { echo "selected"; }} ?>>Pernambuco</option>
            <option value="PI" <?php if (isset($local->estado)) {if ($local->estado === 'PI') { echo "selected"; }} ?>>Piauí</option>
            <option value="RJ" <?php if (isset($local->estado)) {if ($local->estado === 'RJ') { echo "selected"; }} ?>>Rio de Janeiro</option>
            <option value="RN" <?php if (isset($local->estado)) {if ($local->estado === 'RN') { echo "selected"; }} ?>>Rio Grande do Norte</option>
            <option value="RS" <?php if (isset($local->estado)) {if ($local->estado === 'RS') { echo "selected"; }} ?>>Rio Grande do Sul</option>
            <option value="RO" <?php if (isset($local->estado)) {if ($local->estado === 'RO') { echo "selected"; }} ?>>Rondônia</option>
            <option value="RR" <?php if (isset($local->estado)) {if ($local->estado === 'RR') { echo "selected"; }} ?>>Roraima</option>
            <option value="SC" <?php if (isset($local->estado)) {if ($local->estado === 'SC') { echo "selected"; }} ?>>Santa Catarina</option>
            <option value="SP" <?php if (isset($local->estado)) {if ($local->estado === 'SP') { echo "selected"; }} ?>>São Paulo</option>
            <option value="SE" <?php if (isset($local->estado)) {if ($local->estado === 'SE') { echo "selected"; }} ?>>Sergipe</option>
            <option value="TO" <?php if (isset($local->estado)) {if ($local->estado === 'TO') { echo "selected"; }} ?>>Tocantins</option>
            <option value="EX" <?php if (isset($local->estado)) {if ($local->estado === 'ES') { echo "selected"; }} ?>>Estrangeiro</option>
          </select>
        </div>
      </div>
      <div class="float-right">
        <button type="submit" class="btn btn-primary button__alcancy"><i class="fas fa-check"></i> Salvar</button>
      </div>
    </form>  
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
