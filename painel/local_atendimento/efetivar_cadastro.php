<?php 
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');
  
  if ($_POST) {
    $conexao = Conexao::getInstance();
    $conexao->beginTransaction();
    try {
      $uploaddir = dirname(dirname(__DIR__)) .'/arquivos/consultorios/';

      if(!is_dir($uploaddir)) {
        mkdir($uploaddir);
      }

      if (isSet($_FILES['logo'])) {
        $file_width = '300';
        $thumbnail_width = '100';

        $logo = explode('.', $_FILES['logo']['name']);
        $logo_file = md5(uniqid(rand(), true)) . '.' . end($logo);
        $ext = pathinfo($logo_file, PATHINFO_EXTENSION);
        $filename = pathinfo($logo_file, PATHINFO_FILENAME);
        $thumbnail = $filename . '_t.' . $ext;
        make_thumb($_FILES['logo']['tmp_name'], $uploaddir . $logo_file, $file_width);
        make_thumb($_FILES['logo']['tmp_name'], $uploaddir . $thumbnail, $thumbnail_width);      
        $logo_file = upload_to_s3($uploaddir . $logo_file);
        upload_to_s3($uploaddir . $thumbnail);
      }
      
      if ($_POST['local_id'] > 0) {
        $sql = 'UPDATE consultorios '.
               '    SET descricao = :descricao, '.
               '        logradouro = :logradouro, '.
               '        numero = :numero, '.
               '        cidade_id = :cidade_id, '.
               '        telefone = :telefone, '. 
               '        ativo = :ativo, ';

        if (isSet($_FILES['logo'])) {
          $sql .= '        file_logo = :file_logo, ';
        }

        $sql .= '        cep = :cep, '.
                '        bairro = :bairro, '. 
                '        estado = :estado '.
                '  WHERE id = :id';

        $localUpd = $conexao->prepare($sql);
        $localUpd->bindValue(':descricao', $_POST['descricao']);
        $localUpd->bindParam(':logradouro', $_POST['logradouro']);
        $localUpd->bindParam(':numero', $_POST['numero']);
        $localUpd->bindParam(':cidade_id', $_POST['cidade']);
        $localUpd->bindParam(':telefone', $_POST['telefone']);
        $localUpd->bindParam(':ativo', $_POST['ativo']);
        
        if (isSet($_FILES['logo'])) {
          $localUpd->bindParam(':file_logo', $logo_file);
        }

        $localUpd->bindParam(':cep', $_POST['cep']);
        $localUpd->bindParam(':bairro', $_POST['bairro']);
        $localUpd->bindParam(':estado', $_POST['estado']);
        $localUpd->bindParam(':id', $_POST['local_id']);

        $localUpd->execute();  
      } else {
        $sql = 'INSERT INTO consultorios ('.
               '  descricao, '.
               '  logradouro, '.
               '  numero, '.
               '  cidade_id, '.
               '  telefone, '.
               '  ativo, ';

        if (isSet($_FILES['logo'])) {
          $sql .= '  file_logo, ';
        }

        $sql .= '  cep, '.
                '  bairro, '.
                '  estado '.
                ' ) VALUES ( '.
                '  :descricao, '.
                '  :logradouro,'.
                '  :numero,'.
                '  :cidade_id, '.
                '  :telefone, '.
                '  :ativo, ';

        if (isSet($_FILES['logo'])) {
          $sql .= '  :file_logo, ';
        }

        $sql .= '  :cep, '.
                '  :bairro, '.
                '  :estado '.
                ' )';

        $local = $conexao->prepare($sql);
        $local->bindValue(':descricao', $_POST['descricao']);
        $local->bindParam(':logradouro', $_POST['logradouro']);
        $local->bindParam(':numero', $_POST['numero']);
        $local->bindParam(':cidade_id', $_POST['cidade']);
        $local->bindParam(':telefone', $_POST['telefone']);
        $local->bindParam(':ativo', $_POST['ativo']);

        if (isSet($_FILES['logo'])) {
          $local->bindParam(':file_logo', $logo_file);
        }
        
        $local->bindParam(':cep', $_POST['cep']);
        $local->bindParam(':bairro', $_POST['bairro']);
        $local->bindParam(':estado', $_POST['estado']);
        $local->execute();

        $consultorio_id = $conexao->lastInsertId();

        $local_medico = $conexao->prepare('INSERT INTO consultorio_medicos (medico_id,consultorio_id) VALUES (:medico, :consultorio)');
        $local_medico->bindValue(':medico', $_POST['medico_id']);
        $local_medico->bindParam(':consultorio', $consultorio_id);
        $local_medico->execute();
      }

      $conexao->commit(); 
      echo json_encode(Array('status' => "OK"));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }