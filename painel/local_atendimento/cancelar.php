<?php 
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');
 
  if ($_POST) {
    $conexao = Conexao::getInstance();
    
    $conexao->beginTransaction();
    try {
      $query = ' SELECT ativo'.
               '  FROM consultorios '.
               '  WHERE id = :chave ';
      $resultset = $conexao->prepare( $query );
      $resultset->bindParam(':id', $_SESSION['medico_id']);
      $resultset->execute();
      
      $consultorio = $resultset->fetch(PDO::FETCH_OBJ);

      $valor = $consultorio->ativo;
      
      if ($valor == "0") {
        $atestadoUpdate = $conexao->prepare('UPDATE consultorios SET ativo = 1 WHERE id = :chave');  
      }
      else if ($valor == "1") {
        $atestadoUpdate = $conexao->prepare('UPDATE consultorios SET ativo = 0 WHERE id = :chave');
      }

      $atestadoUpdate = $conexao->prepare('UPDATE consultorios SET ativo = 0 WHERE id = :chave');
      $atestadoUpdate->bindParam(':chave', $_POST['chave']);
      $atestadoUpdate->execute();
  
      $conexao->commit();
  
      echo json_encode(Array('status' => $atestadoUpdate));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }