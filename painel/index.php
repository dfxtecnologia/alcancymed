<?php require_once(dirname(__DIR__).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(__DIR__).'/header/index.php') ?>
<?php $total = getQuantidadeDocumentosMes(Conexao::getInstance()); ?>
<?php $status = getStatusConfirmacao(Conexao::getInstance(), 'medico', $_SESSION['medico_cpf']); ?>
<?php $plano = getPlanoAtivo(Conexao::getInstance()); ?>
<?php   
  if (!isMedicoConfirmacaoCadastro(Conexao::getInstance())) {
    if ((!strpos($_SERVER['REQUEST_URI'], 'enviar_documentos.php')) && (!strpos($_SERVER['REQUEST_URI'], 'meus_dados.php'))) {
      echo '<div class="alert alert-warning" role="alert">';
      if ($status['status'] == STATUS_MEDICO_ANALISE) {
        echo '  Seus documentos estão em Análise. Este processo pode demorar até 5 dias úteis. Aguarde a confirmação dos documentos para configurar sua assinatura e assim liberar a emissão de atestados e receitas.';
      // } else if ($status['status'] == STATUS_MEDICO_PAGAMENTO) { 
        // echo '  A emissão de atestados e receitas serão liberados somente após a confirmação de assinatura do AlcancyMED no Paypal. Para confirmar a assinatura no Paypal acesse a opção <a href="'.base_url().'/painel/medicos/meus_dados.php" class="alert-link">Meus Dados</a> opção "Pagamento".';
      } else {
        echo '  Antes de realizar qualquer operação é necessário confirmar o cadastro enviando os documentos solicitados e configurando o método de pagamento';
      }

      echo '</div>';
    }
  }
?>
  <div class="dashboard__wrapper">
    <div class="box__informativos">
      <div class="box__medico shadow">
        <img src="<?=base_url()?>/assets/images/menu__dashboard__medico.png">
        <h4><?=$_SESSION['medico_nome']?></h4>
        <hr/>
        <small>Status da Confirmação do Cadastro</small>
        <span class="<?=$status['class']?>"><?=$status['descricao']?></span>
        <a href="<?=base_url()?>/painel/medicos/enviar_documentos.php" class="visualizar__documentos">Visualizar Documentos</a>

        <a href="<?=base_url()?>/painel/medicos/meus_dados.php" class="btn btn-primary button__alcancy">Meus Dados</a>
        <a href="<?=base_url()?>/painel/local_atendimento/consultar.php" class="btn btn-primary button__alcancy button__color__2">Meus Locais de Atendimento</a>
        <div class="plano">
          <div class="plano_detalhes">
            <p><strong><?=$plano['plano_valor'] == 0.00 ? $plano['plano_titulo']: 'R$ '. number_format($plano['plano_valor'], 2, ',', '.') .'/mês'?></strong></p>
            <p><?=$plano['plano_descricao']?></p>
            <p>Emissão de até <?=$plano['plano_quantidade_documentos']?> documentos por mês</p>
          </div>
          <span><?=$plano['plano_titulo']?></span>
        </div>
      </div>
      <div class="box__atestados shadow">
        <img src="<?=base_url()?>/assets/images/menu__dashboard__atestados.png">
        <h4>Atestados</h4>
        <hr/>
        <a href="<?=base_url()?>/painel/atestados/emitir.php" class="btn btn-primary button__alcancy">Emitir Atestado</a>
        <a href="<?=base_url()?>/painel/atestados/consultar.php" class="btn btn-primary button__alcancy button__color__2">Consultar Atestados</a>
        <small class="indicador"><?=$total['atestado']?> atestados emitidos</small>
      </div>
      <div class="box__receitas shadow">
        <img src="<?=base_url()?>/assets/images/menu__dashboard__receitas.png">
        <h4>Receitas</h4>
        <hr/>
        <a href="<?=base_url()?>/painel/receitas/emitir.php" class="btn btn-primary button__alcancy">Emitir Receita</a>
        <a href="<?=base_url()?>/painel/receitas/consultar.php" class="btn btn-primary button__alcancy button__color__2">Consultar Receitas</a>
        <small class="indicador"><?=$total['receita']?> receitas emitidas</small>
      </div>
    </div>
  </div>
<?php require_once(dirname(__DIR__).'/footer/index.php') ?>
