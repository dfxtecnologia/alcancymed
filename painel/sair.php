<?php 
  session_start();
  require_once(dirname(__DIR__).'/sys/functions.php');

  unset($_SESSION['medico_id']);
  unset($_SESSION['medico_cpf']);
  unset($_SESSION['medico_nome']);
  unset($_SESSION['local_atendimento']);

  header('location: ' . base_url());