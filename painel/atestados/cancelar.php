<?php 
  require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php');
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');
 
  if ($_POST) {
    $conexao = Conexao::getInstance();
    
    $conexao->beginTransaction();
    try {
      $atestadoUpdate = $conexao->prepare('UPDATE atestados SET ativo = 0 WHERE chave = :chave');
      $atestadoUpdate->bindParam(':chave', $_POST['chave']);
      $atestadoUpdate->execute();
  
      $conexao->commit();
  
      echo json_encode(Array('status' => $atestadoUpdate));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }