<?php //require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/print.php') ?>
<?php
  $conexao = Conexao::getInstance();

  $query = ' SELECT atestados.*,'.
           '        pacientes.cpf paciente_cpf, '.
           '        pacientes.nome paciente_nome, '.
           '        pacientes.endereco paciente_endereco, '.
           '        pacientes.bairro paciente_bairro, '.
           '        cidade_paciente.nome paciente_cidade, '.
           '        pacientes.estado paciente_estado, '.
           '        medicos.crm medico_crm, '.
           '        medicos.nome medico_nome, '.
           '        medicos.cpf medico_cpf, '.
           '        medicos.email medico_email, '.
           '        medicos.file_assinatura_digital medico_assinatura_digital, '.
           '        consultorios.file_logo consultorio_logo, '.
           '        consultorios.descricao consultorio_descricao, '.
           '        consultorios.logradouro consultorio_logradouro, '.
           '        consultorios.telefone consultorio_telefone, '.
           '        consultorios.bairro consultorio_bairro, '.
           '        consultorios.cep consultorio_cep, '.
           '        cidades.nome consultorio_cidade, '.
           '        consultorios.estado consultorio_estado '.
           '  FROM atestados '.
           '  INNER JOIN pacientes ON (pacientes.id = atestados.paciente_id) '.
           '   LEFT JOIN cidades cidade_paciente ON (cidade_paciente.id = pacientes.cidade_id) '.
           '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = atestados.consultorio_medico_id) '.           
           '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
           '  INNER JOIN consultorios ON (consultorios.id = consultorio_medicos.consultorio_id) '.
           '   LEFT JOIN cidades ON (cidades.id = consultorios.cidade_id) '.
           '  WHERE atestados.chave = :chave '.
           '  ORDER BY atestados.data DESC ';

  $resultset = $conexao->prepare( $query );
  $resultset->bindParam(':chave', $_GET['chave']);
  $resultset->execute();

  $atestado = $resultset->fetch(PDO::FETCH_OBJ);
?>
  <div class="header__documento__emitido">    
    <?php if ($atestado->consultorio_logo != '') { ?>
      <img src="<?=get_thumbnail_s3($atestado->consultorio_logo)?>">
    <?php } else { ?>
      <img src="<?=base_url()?>/assets/images/logo-medicina.png">
    <?php } ?>    
    <div class="informacoes__documento">
      <div class="logo__alcancy">
        <img src="<?=base_url()?>/assets/images/logo.png" alt="">
      </div>
      <p><?=date('d/m/Y', strtotime($atestado->data))?></p>
      <p class="chave"><?=$atestado->chave?></p>
      <small>Verifique a autenticidade no site</small>
      <small>http://www.alcancymed.com.br</small>
    </div>
  </div>
  <div class="documento__emitido">
    <h1>ATESTADO MÉDICO</h1>
    <?php if (!$atestado->ativo) { ?>
      <h1 class="cancelado">DOCUMENTO CANCELADO</h1>
    <?php } ?>    
    <p>Atesto para os devidos fins, a pedido, que o(a) paciente <?=$atestado->paciente_nome?>, CPF: <?=$atestado->paciente_cpf?>, paciente sob meus cuidados, foi atendido no dia <?=date('d/m/Y', strtotime($atestado->data))?>,  <?=$atestado->descricao?></p>
    <p class="cid">CID: <?=$atestado->cid?></p>
    <p class="assinatura"><?=$atestado->medico_crm .' '. $atestado->medico_nome?></p>
    <div class="clearfix"></div>
  </div>
  <div class="footer__documento__emitido">
    <div class="informacoes__medico">
      <p><?=$atestado->medico_nome?></p>
      <p><?=$atestado->medico_email?></p>
      <p>CPF <?=$atestado->medico_cpf?></p>
      <p>CRM <?=$atestado->medico_crm?></p>
    </div>
    <div class="informacoes__consultorio">
      <p><?=$atestado->consultorio_logradouro?></p>
      <p><?=$atestado->consultorio_bairro. ' - '. $atestado->consultorio_cidade.'/'.$atestado->consultorio_estado?></p>
      <p><?=$atestado->consultorio_cep?></p>
      <p><?=$atestado->consultorio_telefone?></p>
    </div>
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/print.php') ?>
