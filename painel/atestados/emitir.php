<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php $conexao = Conexao::getInstance(); ?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__atestados.png" alt="">
    <h4>Atestados - Emitir</h4>
  </div>
  <div class="dashboard__wrapper">
    <form class="form__alcancy" action="<?=base_url()?>/painel/atestados/efetivar_emissao.php" method="POST" enctype="multipart/form-data" data-redirect="<?=base_url()?>/painel/atestados/consultar.php">
      <?=show_alert('FAIL', 'CPF Inválido.')?>      
      <input type="hidden" name="cpf" value="<?=$_SESSION['medico_cpf']?>">
      <?php if (!showConsultorioSelect(Conexao::getInstance(), $_SESSION['medico_cpf'])) { ?>
        <input type="hidden" name="consultorio_medico" value="<?=getConsultorioID(Conexao::getInstance(), $_SESSION['medico_cpf'])?>">
      <?php } else {        
        $query = ' SELECT consultorios.*,cidades.nome as cidade, consultorio_medicos.id as id_consultorio '.
                  '  FROM consultorios '.
                  '  INNER JOIN consultorio_medicos ON (consultorios.id = consultorio_medicos.consultorio_id) '.
                  '  INNER JOIN cidades ON (consultorios.cidade_id = cidades.id) '.
                  '  WHERE consultorios.ativo = 1 '.
                  '    AND consultorio_medicos.medico_id = :medico_id '.
                  '  ORDER BY consultorios.descricao';
        $resultset = $conexao->prepare( $query );
        $resultset->bindParam(':medico_id', $_SESSION['medico_id']);
        $resultset->execute();            
      ?>
        <h5>Local do Atendimento</h5>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="atestado-tipo">Local do Atendimento</label>
            <select class="form-control select2-basic" name="consultorio_medico" id="consultorio_medico" required autofocus>
              <option></option>
              <?php while ($consultorio = $resultset->fetch(PDO::FETCH_OBJ)) { ?>
                <option value="<?=$consultorio->id_consultorio?>" <?=($_SESSION['local_atendimento'] == $consultorio->id_consultorio ? 'selected' : '')?>><?=$consultorio->descricao?> - <?=$consultorio->logradouro?> - <?=$consultorio->cidade?></option>
              <?php } ?>
            </select>
          </div>
        </div>  
      <?php } ?>
      <h5>Paciente</h5>
      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="paciente-cpf">CPF do Paciente*</label>
          <input type="text" class="form-control cpf-mask" placeholder="CPF do Paciente" name="cpf" id="paciente-cpf" data-search-url="<?=base_url()?>/rest/pacientes.php" required autofocus>
          <div class="valid-feedback">
            CPF válido!
          </div>
          <div class="invalid-feedback">
            CPF inválido!!
          </div>
        </div>
        <div class="form-group col-md-10">
          <label for="paciente-nome">Nome do Paciente*</label>
          <input type="text" class="form-control" placeholder="Nome do Paciente" name="nome" id="paciente-nome" required>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="paciente-celular">Telefone Celular</label>
          <input type="text" class="form-control telefone-mask" placeholder="Celular" name="celular" id="paciente-celular">
        </div>
        <div class="form-group col-md-10">
          <label for="paciente-email">e-mail</label>
          <input type="text" class="form-control" placeholder="e-mail" name="email" id="paciente-email">
        </div>
      </div><!--
      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="paciente-cep">CEP</label>
          <input type="text" class="form-control busca-cep cep-mask" placeholder="CEP" name="cep" id="paciente-cep" required>
        </div>
        <div class="form-group col-md-6">
          <label for="paciente-endereco">Endereço</label>
          <input type="text" class="form-control busca-cep-rua" placeholder="Endereço" name="endereco" id="paciente-endereco" required>
        </div>
        <div class="form-group col-md-2">
          <label for="paciente-numero">Número</label>
          <input type="text" class="form-control" placeholder="Número" name="numero" id="paciente-numero" required>
        </div>        
        <div class="form-group col-md-2">
          <label for="paciente-bairro">Bairro</label>
          <input type="text" class="form-control busca-cep-bairro" placeholder="Bairro" name="bairro" id="paciente-bairro" required>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="paciente-cidade">Cidade</label>
          <select class="form-control select2-basic-search busca-cep-cidade" name="cidade" id="cidade" data-search-url="<?=base_url()?>/rest/cidades.php" required>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="paciente-estado">Estado</label>
          <select class="form-control select2-basic busca-cep-estado" name="estado" id="paciente-estado" required>
            <option></option>
            <option value="AC">Acre</option>
            <option value="AL">Alagoas</option>
            <option value="AP">Amapá</option>
            <option value="AM">Amazonas</option>
            <option value="BA">Bahia</option>
            <option value="CE">Ceará</option>
            <option value="DF">Distrito Federal</option>
            <option value="ES">Espírito Santo</option>
            <option value="GO">Goiás</option>
            <option value="MA">Maranhão</option>
            <option value="MT">Mato Grosso</option>
            <option value="MS">Mato Grosso do Sul</option>
            <option value="MG">Minas Gerais</option>
            <option value="PA">Pará</option>
            <option value="PB">Paraíba</option>
            <option value="PR">Paraná</option>
            <option value="PE">Pernambuco</option>
            <option value="PI">Piauí</option>
            <option value="RJ">Rio de Janeiro</option>
            <option value="RN">Rio Grande do Norte</option>
            <option value="RS">Rio Grande do Sul</option>
            <option value="RO">Rondônia</option>
            <option value="RR">Roraima</option>
            <option value="SC">Santa Catarina</option>
            <option value="SP">São Paulo</option>
            <option value="SE">Sergipe</option>
            <option value="TO">Tocantins</option>
            <option value="ES">Estrangeiro</option>
          </select>
        </div>
      </div>-->
      <h5>Dados do Atestado</h5>
      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="atestado-tipo">Tipo de Atestado</label>
          <select class="form-control select2-basic" name="tipo" id="atestado-tipo" required>
            <option></option>
            <option value="H">Horas</option>
            <option value="D">Dias</option>
          </select>
        </div>
        <div class="form-group col-md-2">
          <label for="atestado-cid">CID</label>
          <select class="form-control select2-form-search" name="cid" id="atestado-cid" data-search-url="<?=base_url()?>/rest/cids.php"></select>
        </div>
        <div class="form-group col-md-8">
          <label for="atestado-descricao">Descrição do Atestado</label>
          <input type="text" class="form-control" placeholder="Descrição" name="descricao" id="atestado-descricao">
        </div>
      </div>
      <div class="float-right">
        <button type="submit" class="btn btn-primary button__alcancy"><i class="fas fa-check"></i> Emitir Atestado</button>
      </div>
    </form>  
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
