<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php $conexao = Conexao::getInstance(); ?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__atestados.png" alt="">
    <h4>Novo Modelo de Atestado</h4>
  </div>
  <div class="dashboard__wrapper">
    <form class="form__alcancy" action="<?=base_url()?>/painel/atestados/modelo_efetivar_emissao.php" method="POST" enctype="multipart/form-data" data-redirect="<?=base_url()?>/painel/atestados/modelo.php">
      <input type="hidden" name="cpf" value="<?=$_SESSION['medico_cpf']?>">      
      <h5>Modelo</h5>
      <div class="form-row">
        <div class="form-group col-md-12">
          <label for="descricao">Descrição*</label>
          <input type="text" class="form-control" placeholder="Descrição" name="titulo" id="titulo" required>
        </div>
      </div> 
      <h5>Dados do Atestado</h5>
      <div class="form-row">
        <div class="form-group col-md-12">
          <label for="atestado-descricao">Descrição do Atestado*</label>
          <input type="text" class="form-control" placeholder="Descrição" name="descricao" id="atestado-descricao" required>
        </div>
      </div>
      <div class="float-right">
        <button type="submit" class="btn btn-primary button__alcancy"><i class="fas fa-check"></i> Salvar Modelo</button>
      </div>
    </form>  
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
