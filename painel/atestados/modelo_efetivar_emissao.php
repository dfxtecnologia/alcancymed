<?php 
  session_start();
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $conexao->beginTransaction();
    try {            
      $atestado = $conexao->prepare('INSERT INTO atestados (titulo, medico_id, descricao, modelo) VALUES (:titulo, :medico_id, :descricao, 1)');
      $atestado->bindParam(':titulo', $_POST['titulo']);
      $atestado->bindParam(':medico_id', $_SESSION['medico_id']);
      $atestado->bindParam(':descricao', $_POST['descricao']);
      $atestado->execute();

      $conexao->commit();
      echo json_encode(Array('status' => "OK"));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }