<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php
  $localatendimento = isset($_GET['local-atendimento']) ? trim(strtoupper($_GET['local-atendimento'])) : '';
  $nomepaciente     = isset($_GET['nome-paciente']) ? trim(strtoupper($_GET['nome-paciente'])) : '';
  $cpf              = isset($_GET['cpf']) ? trim($_GET['cpf']) : '';
  $dataemissao      = isset($_GET['data-emissao']) ? trim(implode('-',array_reverse(explode('/',$_GET['data-emissao'])))) : '';
  $cid              = isset($_GET['cid']) ? trim(strtoupper($_GET['cid'])) : '';

  $link = '';

  if (($localatendimento != '') || ($nomepaciente != '') || ($cpf != '') || ($dataemissao != '') || ($cid != '')) {
    $link = '&local-atendimento='.$localatendimento.'&nome-paciente='.$nomepaciente.'&cpf='.$cpf.'&data-emissao='.$dataemissao.'&cid='.$cid;
  }
?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__atestados.png" alt="">
    <h4>Atestados - Consultar</h4>
  </div>
  <div class="dashboard__wrapper">
    <div class="filtro">
      <div class="filtro__titulo">
        <i class="fas fa-filter"></i>
        <h4>Filtrar por:</h4>
      </div>
      <div class="filtro__dados">
        <form class="form-inline" method="GET">
          <input type="text" class="form-control mr-2" name="local-atendimento" id="local-atendimento" placeholder="Local de Atendimento" value="<?=$localatendimento?>">
          <input type="text" class="form-control mr-2" name="nome-paciente" id="nome-paciente" placeholder="Nome do Paciente" value="<?=$nomepaciente?>">
          <input type="text" class="form-control mr-2 cpf-mask" name="cpf"  id="cpf" placeholder="CPF" value="<?=$cpf?>">
          <input type="text" class="form-control mr-2 data-mask" name="data-emissao" id="data-emissao" placeholder="Data de Emissão" value="<?=implode('/',array_reverse(explode('-',$dataemissao)))?>">
          <input type="text" class="form-control mr-2" name="cid"  id="cid" placeholder="CID" value="<?=$cid?>">
          <button type="submit" class="btn btn-primary button__alcancy">Filtrar</button>
          <button type="button" class="close button__alcancy__close__filtro" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </form>
      </div>
    </div>  
    <div class="table-responsive">
      <table class="table table-borderless table-striped datatable__alcancy">
        <thead>
          <tr>
            <th>Local Atendimentos</th>
            <th>Paciente</th>
            <th>CPF</th>
            <th>Data de Emissão</th>
            <th>CID</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php
            $conexao = Conexao::getInstance();

            $query = ' SELECT COUNT(atestados.id) total '.
                     '  FROM atestados '.
                     '  INNER JOIN pacientes ON (pacientes.id = atestados.paciente_id) '.
                     '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = atestados.consultorio_medico_id) '.
                     '  INNER JOIN consultorios ON (consultorios.id = consultorio_medicos.consultorio_id) '.
                     '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
                     '  WHERE medicos.cpf = :cpf';

            if ($localatendimento != '') {
              $query .= ' AND UPPER(consultorios.descricao) LIKE \'%'. $localatendimento .'%\'';
            }
            if ($nomepaciente != '') {
              $query .= ' AND UPPER(pacientes.nome) LIKE \'%'. $nomepaciente .'%\'';
            }
            if ($cpf != '') {
              $query .= ' AND pacientes.cpf = \'' .$cpf .'\'';
            }
            if ($dataemissao != '') {
              $query .= ' AND DATE_FORMAT(atestados.data, \'%Y-%m-%d\') = \'' .$dataemissao .'\'';
            }
            if ($cid != '') {
              $query .= ' AND UPPER(atestados.cid) = \'' .$cid .'\'';
            }

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':cpf', $_SESSION['medico_cpf']);
            $resultset->execute();
            $atestado = $resultset->fetch(PDO::FETCH_OBJ);

            $total_registros = $atestado->total;
            $total_paginas   = ceil($total_registros / PAGE_LIMIT);            
            $pagina          = isSet($_GET['pagina']) ? $_GET['pagina'] : 1;
            $limite          = ($pagina - 1) * PAGE_LIMIT;

            $query = ' SELECT atestados.*,'.
                     '        pacientes.cpf paciente_cpf, '.
                     '        pacientes.nome paciente_nome, '.
                     '        medicos.nome medico_nome, '.
                     '        consultorios.descricao consultorio_descricao '.
                     '  FROM atestados '.
                     '  INNER JOIN pacientes ON (pacientes.id = atestados.paciente_id) '.
                     '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = atestados.consultorio_medico_id) '.
                     '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
                     '  INNER JOIN consultorios ON (consultorios.id = consultorio_medicos.consultorio_id) '.
                     '  WHERE medicos.cpf = :cpf ';

            if ($localatendimento != '') {
              $query .= ' AND UPPER(consultorios.descricao) LIKE \'%'. $localatendimento .'%\'';
            }
            if ($nomepaciente != '') {
              $query .= ' AND UPPER(pacientes.nome) LIKE \'%'. $nomepaciente .'%\'';
            }
            if ($cpf != '') {
              $query .= ' AND pacientes.cpf = \'' .$cpf .'\'';
            }
            if ($dataemissao != '') {
              $query .= ' AND DATE_FORMAT(atestados.data, \'%Y-%m-%d\') = \'' .$dataemissao .'\'';
            }
            if ($cid != '') {
              $query .= ' AND UPPER(atestados.cid) = \'' .$cid .'\'';
            }

            $query .= '  ORDER BY atestados.data DESC '.
                      '  LIMIT '. $limite .','. PAGE_LIMIT;

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':cpf', $_SESSION['medico_cpf']);
            $resultset->execute();

            while($atestado = $resultset->fetch(PDO::FETCH_OBJ)) { ?>
            <tr>
              <td><?=$atestado->consultorio_descricao?></td>
              <td><?=$atestado->paciente_nome?></td>
              <td><?=$atestado->paciente_cpf?></td>
              <td><?=date('d/m/Y H:i:s', strtotime($atestado->data))?></td>
              <td><?=$atestado->cid?></td>
              <td><?=($atestado->ativo == 0)? 'Cancelado':'Ativo'?></td>
              <td>
                <a class="alcancy__mobile" href="<?=base_url()?>/painel/atestados/pdf.php?chave=<?=$atestado->chave?>"><i class="fas fa-print"></i></a>
                <a class="alcancy__desktop" href="<?=base_url()?>/painel/atestados/pdf.php?chave=<?=$atestado->chave?>" target="_blank"><i class="fas fa-print"></i></a>
                <?php if (($atestado->ativo) && isMedicoConfirmacaoCadastro(Conexao::getInstance())) { ?>
                  <a href="<?=base_url()?>/painel/atestados/cancelar.php" data-chave="<?=$atestado->chave?>" class="documento-cancelar"><i class="fas fa-trash-alt"></i></a>
                <?php } ?>  
              </td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
    </div>
    <nav aria-label="Page navigation example">
      <small>página <?=$pagina?> de <?=$total_paginas?> - <?=$total_registros?> atestados emitidos</small>
      <ul class="pagination justify-content-end">
        <?php for($i = 1; $i <= $total_paginas; $i++) { ?>
          <li class="page-item <?=($pagina==$i)?'active':''?>">
            <a class="page-link" href="<?=base_url()?>/painel/atestados/consultar.php?pagina=<?=$i.$link?>"><?=$i?></a>
          </li>
        <?php } ?>
      </ul>
    </nav>      
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
