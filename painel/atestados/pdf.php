<?php
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/pdf/alcancy_pdf.php');
  $conexao = Conexao::getInstance();
    
  $query = ' SELECT atestados.*,'.
           '        pacientes.cpf paciente_cpf, '.
           '        pacientes.nome paciente_nome, '.
           '        pacientes.endereco paciente_endereco, '.
           '        pacientes.bairro paciente_bairro, '.
           '        cidade_paciente.nome paciente_cidade, '.
           '        pacientes.estado paciente_estado, '.
           '        medicos.crm medico_crm, '.
           '        medicos.nome medico_nome, '.
           '        medicos.cpf medico_cpf, '.
           '        medicos.email medico_email, '.
           '        medicos.file_assinatura_digital medico_assinatura_digital, '.
           '        consultorios.file_logo consultorio_logo, '.
           '        consultorios.descricao consultorio_descricao, '.
           '        consultorios.logradouro consultorio_logradouro, '.
           '        consultorios.numero consultorio_numero, '.
           '        consultorios.telefone consultorio_telefone, '.
           '        consultorios.bairro consultorio_bairro, '.
           '        consultorios.cep consultorio_cep, '.
           '        cidades.nome consultorio_cidade, '.
           '        consultorios.estado consultorio_estado '.
           '  FROM atestados '.
           '  INNER JOIN pacientes ON (pacientes.id = atestados.paciente_id) '.
           '   LEFT JOIN cidades cidade_paciente ON (cidade_paciente.id = pacientes.cidade_id) '.
           '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = atestados.consultorio_medico_id) '.           
           '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
           '  INNER JOIN consultorios ON (consultorios.id = consultorio_medicos.consultorio_id) '.
           '   LEFT JOIN cidades ON (cidades.id = consultorios.cidade_id) '.
           '  WHERE atestados.chave = :chave '.
           '  ORDER BY atestados.data DESC ';

  $resultset = $conexao->prepare( $query );
  $resultset->bindParam(':chave', $_GET['chave']);
  $resultset->execute();

  $atestado = $resultset->fetch(PDO::FETCH_OBJ);

  $pdf = new PDF('P', 'mm', 'A4', $atestado);
  $pdf->AliasNbPages();
  $pdf->AddPage();
  $pdf->SetFont('Arial', '', 8);

  $pdf->SetY(70);
  $pdf->Write(5, utf8_decode('Atesto para os devidos fins, a pedido, que o(a) paciente '. $atestado->paciente_nome .' CPF: '. $atestado->paciente_cpf .' esteve sob meus cuidados, foi atendido no dia '. date('d/m/Y', strtotime($atestado->data)) .".\n". $atestado->descricao));

  $pdf->SetFont('Arial', 'B', 8);
  $pdf->Text(10, 150, utf8_decode('CID: '. $atestado->cid));

  if (isSet($_GET['_'])) {
    if ($atestado->ativo) {
      $pdf->SetFont('Arial','B',40);
      $pdf->SetTextColor(255,192,203);
      $pdf->RotatedText(30,190, utf8_decode('DOCUMENTO SEM VALIDADE'),45);
      $pdf->RotatedText(55,190, utf8_decode('APENAS CONFERÊNCIA'),45);
      $pdf->SetFont('Arial','',8);
      $pdf->SetTextColor(0,0,0);
    }
  }
    
  $pdf->Output('I', $atestado->chave.'.pdf');