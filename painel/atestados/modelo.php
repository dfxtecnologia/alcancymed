<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php
  $descricao    = isset($_GET['local-atendimento']) ? trim(strtoupper($_GET['local-atendimento'])) : '';
  $datacadastro = isset($_GET['data-cadastro']) ? trim(implode('-',array_reverse(explode('/',$_GET['data-cadastro'])))) : '';

  $link = '';

  if (($descricao != '') || ($datacadastro != '')) {
    $link = '&descricao='.$descricao.'&data-cadastro='.$datacadastro;
  }
?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__atestados.png" alt="">
    <h4>Modelo de Atestados - Consultar</h4>
  </div>
  <div class="dashboard__wrapper">
    <div class="filtro">
      <div class="filtro__titulo">
        <i class="fas fa-filter"></i>
        <h4>Filtrar por:</h4>
      </div>
      <div class="filtro__dados">
        <form class="form-inline" method="GET">
          <input type="text" class="form-control mr-2" name="descricao" id="descricao" placeholder="Descrição" value="<?=$descricao?>">
          <input type="text" class="form-control mr-2 data-mask" name="data-cadastro" id="data-cadastro" placeholder="Data de Cadastro" value="<?=implode('/',array_reverse(explode('-',$datacadastro)))?>">
          <button type="submit" class="btn btn-primary button__alcancy">Filtrar</button>
          <button type="button" class="close button__alcancy__close__filtro" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </form>
      </div>
    </div>  
    <div class="table-responsive">
      <table class="table table-borderless table-striped datatable__alcancy">
        <thead>
          <tr>
            <th>Descrição</th>
            <th>Data de Cadastro</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php
            $conexao = Conexao::getInstance();

            $query = ' SELECT COUNT(atestados.id) total '.
                     '  FROM atestados '.
                     '  WHERE atestados.medico_id = :medico_id '.
                     '    AND atestados.modelo    = 1 ';

            if ($descricao != '') {
              $query .= ' AND UPPER(atestados.titulo) LIKE \'%'. $descricao .'%\'';
            }
            if ($datacadastro != '') {
              $query .= ' AND DATE_FORMAT(atestados.data, \'%Y-%m-%d\') = \'' .$datacadastro .'\'';
            }

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':medico_id', $_SESSION['medico_id']);
            $resultset->execute();
            $atestado = $resultset->fetch(PDO::FETCH_OBJ);

            $total_registros = $atestado->total;
            $total_paginas   = ceil($total_registros / PAGE_LIMIT);            
            $pagina          = isSet($_GET['pagina']) ? $_GET['pagina'] : 1;
            $limite          = ($pagina - 1) * PAGE_LIMIT;

            $query = ' SELECT atestados.* '.
                     '  FROM atestados '.
                     '  WHERE atestados.medico_id = :medico_id '.
                     '    AND atestados.modelo    = 1 ';

            if ($descricao != '') {
              $query .= ' AND UPPER(atestados.titulo) LIKE \'%'. $descricao .'%\'';
            }
            if ($datacadastro != '') {
              $query .= ' AND DATE_FORMAT(atestados.data, \'%Y-%m-%d\') = \'' .$datacadastro .'\'';
            }

            $query .= '  ORDER BY atestados.titulo ASC '.
                      '  LIMIT '. $limite .','. PAGE_LIMIT;

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':medico_id', $_SESSION['medico_id']);
            $resultset->execute();

            while($atestado = $resultset->fetch(PDO::FETCH_OBJ)) { ?>
            <tr>
              <td><?=$atestado->titulo?></td>
              <td><?=date('d/m/Y H:i:s', strtotime($atestado->data))?></td>
              <td><?=($atestado->ativo == 0)? 'Cancelado':'Ativo'?></td>
              <td>
                <?php if (($atestado->ativo) && isMedicoConfirmacaoCadastro(Conexao::getInstance())) { ?>
                  <a href="<?=base_url()?>/painel/atestados/cancelar.php" data-id="<?=$atestado->id?>" class="documento-cancelar"><i class="fas fa-trash-alt"></i></a>
                <?php } ?>  
              </td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="float-right">
      <a href="<?=base_url()?>/painel/atestados/modelo_cadastrar.php" class="btn btn-primary button__alcancy">
        <i class="fas fa-file-alt"></i> Novo Modelo
      </a>
    </div>
    <div class="clearfix"></div>
    <nav aria-label="Page navigation example">
      <small>página <?=$pagina?> de <?=$total_paginas?> - <?=$total_registros?> modelos de atestados</small>
      <ul class="pagination justify-content-end">
        <?php for($i = 1; $i <= $total_paginas; $i++) { ?>
          <li class="page-item <?=($pagina==$i)?'active':''?>">
            <a class="page-link" href="<?=base_url()?>/painel/atestados/modelo.php?pagina=<?=$i.$link?>"><?=$i?></a>
          </li>
        <?php } ?>
      </ul>
    </nav>      
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
