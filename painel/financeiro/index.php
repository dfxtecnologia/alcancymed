<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/paypal/functions.php') ?>
<?php
  $conexao = Conexao::getInstance();
  $resultset = $conexao->prepare('SELECT * FROM medicos WHERE cpf = :cpf');
  $resultset->bindParam(':cpf', $_SESSION['medico_cpf']);
  $resultset->execute();

  $medico = $resultset->fetch(PDO::FETCH_OBJ);

  if ($medico->paypal_agreemeent_id != '') { 
    $apiContext = paypalApiAuth();
    $agreement  = paypalGetAgreement($apiContext, $medico->paypal_agreemeent_id);
    $agreementTransactions = paypalGetAgreementTransactions($apiContext, $agreement);

    echo '<pre>';
    var_export($agreementTransactions);
    echo '</pre>';
  }
?>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>