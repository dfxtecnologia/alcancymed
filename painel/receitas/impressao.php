<?php //require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/print.php') ?>
<?php
  $conexao = Conexao::getInstance();
    
  $query = ' SELECT receitas.*,'.
           '        pacientes.cpf paciente_cpf, '.
           '        pacientes.nome paciente_nome, '.
           '        pacientes.cep paciente_cep, '.
           '        pacientes.endereco paciente_endereco, '.
           '        pacientes.bairro paciente_bairro, '.
           '        pacientes.estado paciente_estado, '.
           '        cidade_paciente.nome paciente_cidade, '.
           '        medicos.crm medico_crm, '.
           '        medicos.nome medico_nome, '.
           '        medicos.cpf medico_cpf, '.
           '        medicos.email medico_email, '.
           '        medicos.file_assinatura_digital medico_assinatura_digital, '.
           '        consultorios.file_logo consultorio_logo, '.           
           '        consultorios.descricao consultorio_descricao, '.
           '        consultorios.logradouro consultorio_logradouro, '.
           '        consultorios.telefone consultorio_telefone, '.
           '        consultorios.bairro consultorio_bairro, '.
           '        consultorios.cep consultorio_cep, '.
           '        cidades.nome consultorio_cidade, '.
           '        consultorios.estado consultorio_estado '.
           '  FROM receitas '.
           '  INNER JOIN pacientes ON (pacientes.id = receitas.paciente_id) '.
           '   LEFT JOIN cidades cidade_paciente ON (cidade_paciente.id = pacientes.cidade_id) '.
           '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = receitas.consultorio_medico_id) '.
           '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
           '  INNER JOIN consultorios ON (consultorios.id = consultorio_medicos.consultorio_id) '.
           '   LEFT JOIN cidades ON (cidades.id = consultorios.cidade_id) '.
           '  WHERE receitas.chave = :chave '.
           '  ORDER BY receitas.data DESC ';

  $resultset = $conexao->prepare( $query );
  $resultset->bindParam(':chave', $_GET['chave']);
  $resultset->execute();

  $receita = $resultset->fetch(PDO::FETCH_OBJ);
    
  $query = ' SELECT * '.
           '   FROM receita_medicamentos '.
           '  INNER JOIN receitas ON (receitas.id = receita_medicamentos.receitas_id) '.
           '  INNER JOIN medicamentos ON (medicamentos.id = receita_medicamentos.medicamentos_id) '.
           '  WHERE receitas.chave =  :chave '.
           '  ORDER BY medicamentos.nome_comercial DESC ';
  
  $resultsetMedicamentos = $conexao->prepare( $query );
  $resultsetMedicamentos->bindParam(':chave', $receita->chave);
  $resultsetMedicamentos->execute();

  $tipo_documento = isSet($_GET['tipo']) ? ($_GET['tipo'] == '' ? 'S' : $_GET['tipo']) : 'S';
  $conexao->beginTransaction();
  try {
    $atualizaMedico = $conexao->prepare('UPDATE receitas set tipo = :tipo WHERE chave = :chave');
    
    $atualizaMedico->bindParam(':tipo', $tipo_documento);
    $atualizaMedico->bindParam(':chave', $_GET['chave']);
    $atualizaMedico->execute();

    $conexao->commit();
  }catch (PDOException $e) {
    $conexao->rollBack();    
  }
?>
  <div class="header__documento__emitido">    
    <?php if ($receita->consultorio_logo != '') { ?>
      <img src="<?=get_thumbnail_s3($receita->consultorio_logo)?>">
    <?php } else { ?>
      <img src="<?=base_url()?>/assets/images/logo-medicina.png">
    <?php } ?>    
    <div class="informacoes__documento">
      <div class="logo__alcancy">
        <img src="<?=base_url()?>/assets/images/logo.png" alt="">
      </div>
      <p><?=date('d/m/Y', strtotime($receita->data))?></p>
      <p class="chave"><?=$receita->chave?></p>
      <small>Verifique a autenticidade no site</small>
      <small>http://www.alcancymed.com.br</small>
    </div>
  </div>
  <div class="documento__emitido">
    <h1><?=$tipo_documento == 'C' ? 'RECEITUÁRIO DE CONTROLE ESPECIAL' : 'RECEITA MÉDICA'?></h1>
    <?php if (!$receita->ativo) { ?>
      <h1 class="cancelado">DOCUMENTO CANCELADO</h1>
    <?php } ?>
    <table class="table table-borderless">
      <tbody>
        <?php
          $rowColor = '#f1f1f1';
          while($medicamento = $resultsetMedicamentos->fetch(PDO::FETCH_OBJ)) {

          $rowColor = ($rowColor == '#f1f1f1' ? '#fff' : '#f1f1f1');
        ?>
          <tr>
            <td><?=$medicamento->nome_comercial?></td>
            <td><?=$medicamento->quantidade?></td>
          </tr>
          <tr style="border-bottom: 1px solid rgba(0,0,0,.1)">
            <td colspan="2"><?=$medicamento->forma_uso?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <p class="assinatura"><?=$receita->medico_crm .' '. $receita->medico_nome?></p>
    <div class="clearfix"></div>
  </div>
  <?php if ($tipo_documento == 'C') { ?>
    <div class="documento__controlado">
      <div class="identificacao__comprador">
        <h4>identificação do comprador</h4>
        <ul>
          <li><span>Nome</span></li>
          <li>&nbsp;</li>
          <li><span>Ident:</span> <span>Órgão Emissor</span></li>
          <li><span>Endereço</span></li>
          <li><span>Cidade</span><span>UF</span></li>
          <li><span>Telefone</span></li>
        </ul>
      </div>
      <div class="identificacao__fornecedor">
        <h4>identificação do fornecedor</h4>
        <div class="assinaturas">
          <span>assinatura do farmacêutico</span>
          <span>data</span>
        </div>
      </div>
    </div>  
  <?php } ?>
  <div class="footer__documento__emitido">
    <div class="informacoes__medico">
      <p><?=$receita->medico_nome?></p>
      <p><?=$receita->medico_email?></p>
      <p>CPF <?=$receita->medico_cpf?></p>
      <p>CRM <?=$receita->medico_crm?></p>
    </div>
    <div class="informacoes__consultorio">
      <p><?=$receita->consultorio_logradouro?></p>
      <p><?=$receita->consultorio_bairro. ' - '. $receita->consultorio_cidade.'/'.$receita->consultorio_estado?></p>
      <p><?=$receita->consultorio_cep?></p>
      <p><?=$receita->consultorio_telefone?></p>
    </div>
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/print.php') ?>
