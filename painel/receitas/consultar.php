<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php
  $localatendimento = isset($_GET['local-atendimento']) ? trim(strtoupper($_GET['local-atendimento'])) : '';
  $nomepaciente     = isset($_GET['nome-paciente']) ? trim(strtoupper($_GET['nome-paciente'])) : '';
  $cpf              = isset($_GET['cpf']) ? trim($_GET['cpf']) : '';
  $dataemissao      = isset($_GET['data-emissao']) ? trim(implode('-',array_reverse(explode('/',$_GET['data-emissao'])))) : '';

  $link = '';

  if (($localatendimento != '') || ($nomepaciente != '') || ($cpf != '') || ($dataemissao != '')) {
    $link = '&local-atendimento='.$localatendimento.'&nome-paciente='.$nomepaciente.'&cpf='.$cpf.'&data-emissao='.$dataemissao;
  }
?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__receitas.png" alt="">
    <h4>Receitas - Consultar</h4>
  </div>
  <div class="dashboard__wrapper">
    <div class="filtro">
      <div class="filtro__titulo">
        <i class="fas fa-filter"></i>
        <h4>Filtrar por:</h4>
      </div>
      <div class="filtro__dados">
        <form class="form-inline" method="GET">
          <input type="text" class="form-control mr-2" name="local-atendimento" id="local-atendimento" placeholder="Local de Atendimento" value="<?=$localatendimento?>">
          <input type="text" class="form-control mr-2" name="nome-paciente" id="nome-paciente" placeholder="Nome do Paciente" value="<?=$nomepaciente?>">
          <input type="text" class="form-control mr-2 cpf-mask" name="cpf"  id="cpf" placeholder="CPF" value="<?=$cpf?>">
          <input type="text" class="form-control mr-2 data-mask" name="data-emissao" id="data-emissao" placeholder="Data de Emissão" value="<?=implode('/',array_reverse(explode('-',$dataemissao)))?>">
          <button type="submit" class="btn btn-primary button__alcancy">Filtrar</button>
          <button type="button" class="close button__alcancy__close__filtro" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </form>
      </div>
    </div>  
    <div class="table-responsive">
      <table class="table table-borderless table-striped datatable__alcancy">
        <thead>
          <tr>
            <th></th>
            <th>Local Atendimento</th>
            <th>Paciente</th>
            <th>CPF</th>
            <th>Data Emissão</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php
            $conexao = Conexao::getInstance();
            $query = ' SELECT COUNT(receitas.id) total '.
                     '  FROM receitas '.
                     '  INNER JOIN pacientes ON (pacientes.id = receitas.paciente_id) '.
                     '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = receitas.consultorio_medico_id) '.
                     '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
                     '  INNER JOIN consultorios ON (consultorios.id = consultorio_medicos.consultorio_id) '.
                     '  WHERE medicos.cpf = :cpf '.
                     '    AND receitas.modelo = 0 ';

            if ($localatendimento != '') {
              $query .= ' AND UPPER(consultorios.descricao) LIKE \'%'. $localatendimento .'%\'';
            }
            if ($nomepaciente != '') {
              $query .= ' AND UPPER(pacientes.nome) LIKE \'%'. $nomepaciente .'%\'';
            }
            if ($cpf != '') {
              $query .= ' AND pacientes.cpf = \'' .$cpf .'\'';
            }
            if ($dataemissao != '') {
              $query .= ' AND DATE_FORMAT(receitas.data, \'%Y-%m-%d\') = \'' .$dataemissao .'\'';
            }

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':cpf', $_SESSION['medico_cpf']);
            $resultset->execute();
            $receita = $resultset->fetch(PDO::FETCH_OBJ);

            $total_registros = $receita->total;
            $total_paginas   = ceil($total_registros / PAGE_LIMIT);            
            $pagina          = isSet($_GET['pagina']) ? $_GET['pagina'] : 1;
            $limite          = ($pagina - 1) * PAGE_LIMIT;

            $query = ' SELECT receitas.*,'.
                     '        pacientes.cpf paciente_cpf, '.
                     '        pacientes.nome paciente_nome, '.
                     '        medicos.nome medico_nome, '.
                     '        consultorios.descricao consultorio_descricao '.
                     '  FROM receitas '.
                     '  INNER JOIN pacientes ON (pacientes.id = receitas.paciente_id) '.
                     '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = receitas.consultorio_medico_id) '.
                     '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
                     '  INNER JOIN consultorios ON (consultorios.id = consultorio_medicos.consultorio_id) '.
                     '  WHERE medicos.cpf = :cpf '.
                     '    AND receitas.modelo = 0 ';

            if ($localatendimento != '') {
              $query .= ' AND UPPER(consultorios.descricao) LIKE \'%'. $localatendimento .'%\'';
            }
            if ($nomepaciente != '') {
              $query .= ' AND UPPER(pacientes.nome) LIKE \'%'. $nomepaciente .'%\'';
            }
            if ($cpf != '') {
              $query .= ' AND pacientes.cpf = \'' .$cpf .'\'';
            }
            if ($dataemissao != '') {
              $query .= ' AND DATE_FORMAT(receitas.data, \'%Y-%m-%d\') = \'' .$dataemissao .'\'';
            }

            $query .= '  ORDER BY receitas.data DESC '.
                      '  LIMIT '. $limite .','. PAGE_LIMIT;

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':cpf', $_SESSION['medico_cpf']);
            $resultset->execute();

            while($receita = $resultset->fetch(PDO::FETCH_OBJ)) { ?>
            <tr>
              <td><a class="receita-detalhes" href="<?=base_url()?>/rest/receitas.php" data-id=<?=$receita->id?> title="Detalhes"><i class="far fa-plus-square"></i></a></td>
              <td><?=$receita->consultorio_descricao?></td>
              <td><?=$receita->paciente_nome?></td>
              <td><?=$receita->paciente_cpf?></td>
              <td><?=date('d/m/Y', strtotime($receita->data))?></td>
              <td><?=($receita->ativo == 0)? 'Cancelada':'Ativa'?></td>
              <td>

                <div class="dropdown d-inline-block">
                  <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-print"></i>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item alcancy__mobile" href="<?=base_url()?>/painel/receitas/pdf.php?chave=<?=$receita->chave?>&tipo=S"><?=$receita->tipo=='S'? '<i class="fas fa-check"></i>':''?> Simples</a>
                    <a class="dropdown-item alcancy__mobile" href="<?=base_url()?>/painel/receitas/pdf.php?chave=<?=$receita->chave?>&tipo=C"><?=$receita->tipo=='C'? '<i class="fas fa-check"></i>':''?> Controlada</a>                  
                    <a class="dropdown-item alcancy__desktop" href="<?=base_url()?>/painel/receitas/pdf.php?chave=<?=$receita->chave?>&tipo=S" target="_blank"><?=$receita->tipo=='S'? '<i class="fas fa-check"></i>':''?> Simples</a>
                    <a class="dropdown-item alcancy__desktop" href="<?=base_url()?>/painel/receitas/pdf.php?chave=<?=$receita->chave?>&tipo=C" target="_blank"><?=$receita->tipo=='C'? '<i class="fas fa-check"></i>':''?> Controlada</a>
                  </div>
                </div>
                <!-- <a href="<?=base_url()?>/painel/receitas/impressao.php?chave=<?=$receita->chave?>&tipo=<?=$receita->tipo?>" target="_blank"><i class="fas fa-print"></i></a> -->
                <?php if (($receita->ativo) && isMedicoConfirmacaoCadastro(Conexao::getInstance())) { ?>
                  <a href="<?=base_url()?>/painel/receitas/cancelar.php" data-id="<?=$receita->id?>" class="documento-cancelar"><i class="fas fa-trash-alt"></i></i></a>
                <?php } ?>
              </td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
    </div>
    <nav aria-label="Page navigation example">
      <small>página <?=$pagina?> de <?=$total_paginas?> - <?=$total_registros?> receitas emitidas</small>
      <ul class="pagination justify-content-end">
        <?php for($i = 1; $i <= $total_paginas; $i++) { ?>
          <li class="page-item <?=($pagina==$i)?'active':''?>">
            <a class="page-link" href="<?=base_url()?>/painel/receitas/consultar.php?pagina=<?=$i.$link?>"><?=$i?></a>
          </li>
        <?php } ?>
      </ul>
    </nav>
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
