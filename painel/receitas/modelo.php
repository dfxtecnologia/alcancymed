<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php
  $descricao    = isset($_GET['descricao']) ? trim(strtoupper($_GET['descricao'])) : '';
  $datacadastro = isset($_GET['data-cadastro']) ? trim(implode('-',array_reverse(explode('/',$_GET['data-cadastro'])))) : '';

  $link = '';

  if (($descricao != '') || ($datacadastro != '')) {
    $link = '&descricao='.$descricao.'&data-cadastro='.$datacadastro;
  }
?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__receitas.png" alt="">
    <h4>Modelo de Receitas - Consultar</h4>
  </div>
  <div class="dashboard__wrapper">
    <div class="filtro">
      <div class="filtro__titulo">
        <i class="fas fa-filter"></i>
        <h4>Filtrar por:</h4>
      </div>
      <div class="filtro__dados">
        <form class="form-inline" method="GET">
          <input type="text" class="form-control mr-2" name="descricao" id="descricao" placeholder="Descrição" value="<?=$descricao?>">
          <input type="text" class="form-control mr-2 data-mask" name="data-cadastro" id="data-cadastro" placeholder="Data de Cadastro" value="<?=implode('/',array_reverse(explode('-',$datacadastro)))?>">
          <button type="submit" class="btn btn-primary button__alcancy">Filtrar</button>
          <button type="button" class="close button__alcancy__close__filtro" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </form>
      </div>
    </div>  
    <div class="table-responsive">
      <table class="table table-borderless table-striped datatable__alcancy">
        <thead>
          <tr>
            <th></th>
            <th>Descrição</th>
            <th>Data Cadastro</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php
            $conexao = Conexao::getInstance();
            $query = ' SELECT COUNT(receitas.id) total '.
                     '  FROM receitas '.
                     '  WHERE receitas.medico_id = :medico_id '.
                     '    AND receitas.modelo = 1';

            if ($descricao != '') {
              $query .= ' AND UPPER(receitas.descricao) LIKE \'%'. $descricao .'%\'';
            }
            if ($datacadastro != '') {
              $query .= ' AND DATE_FORMAT(receitas.data, \'%Y-%m-%d\') = \'' .$datacadastro .'\'';
            }

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':medico_id', $_SESSION['medico_id']);
            $resultset->execute();
            $receita = $resultset->fetch(PDO::FETCH_OBJ);

            $total_registros = $receita->total;
            $total_paginas   = ceil($total_registros / PAGE_LIMIT);            
            $pagina          = isSet($_GET['pagina']) ? $_GET['pagina'] : 1;
            $limite          = ($pagina - 1) * PAGE_LIMIT;

            $query = ' SELECT receitas.* '.
                     '  FROM receitas '.
                     '  WHERE receitas.medico_id = :medico_id '.
                     '    AND receitas.modelo = 1';

            if ($descricao != '') {
              $query .= ' AND UPPER(receitas.descricao) LIKE \'%'. $descricao .'%\'';
            }
            if ($datacadastro != '') {
              $query .= ' AND DATE_FORMAT(receitas.data, \'%Y-%m-%d\') = \'' .$datacadastro .'\'';
            }

            $query .= '  ORDER BY receitas.descricao ASC '.
                      '  LIMIT '. $limite .','. PAGE_LIMIT;

            $resultset = $conexao->prepare( $query );
            $resultset->bindParam(':medico_id', $_SESSION['medico_id']);
            $resultset->execute();

            while($receita = $resultset->fetch(PDO::FETCH_OBJ)) { ?>
            <tr>
              <td><a class="receita-detalhes" href="<?=base_url()?>/rest/receitas.php" data-id=<?=$receita->id?> title="Detalhes"><i class="far fa-plus-square"></i></a></td>
              <td><?=$receita->descricao?></td>
              <td><?=date('d/m/Y', strtotime($receita->data))?></td>
              <td><?=($receita->ativo == 0)? 'Cancelada':'Ativa'?></td>
              <td>
                <?php if (($receita->ativo) && isMedicoConfirmacaoCadastro(Conexao::getInstance())) { ?>
                  <a href="<?=base_url()?>/painel/receitas/cancelar.php" data-id="<?=$receita->id?>" class="documento-cancelar"><i class="fas fa-trash-alt"></i></i></a>
                <?php } ?>
              </td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
    </div>
    <div class="float-right">
      <a href="<?=base_url()?>/painel/receitas/modelo_cadastrar.php" class="btn btn-primary button__alcancy">
        <i class="fas fa-file-alt"></i> Novo Modelo
      </a>
    </div>
    <div class="clearfix"></div>
    <nav aria-label="Page navigation example">
      <small>página <?=$pagina?> de <?=$total_paginas?> - <?=$total_registros?> modelos de receitas </small>
      <ul class="pagination justify-content-end">
        <?php for($i = 1; $i <= $total_paginas; $i++) { ?>
          <li class="page-item <?=($pagina==$i)?'active':''?>">
            <a class="page-link" href="<?=base_url()?>/painel/receitas/modelo.php?pagina=<?=$i.$link?>"><?=$i?></a>
          </li>
        <?php } ?>
      </ul>
    </nav>
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
