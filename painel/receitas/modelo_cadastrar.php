<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php $conexao = Conexao::getInstance(); ?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__receitas.png" alt="">
    <h4>Novo Modelo de Receita</h4>
  </div>
  <div class="dashboard__wrapper">
    <form class="form__alcancy" action="<?=base_url()?>/painel/receitas/modelo_efetivar_emissao.php" method="POST" enctype="multipart/form-data" data-redirect="<?=base_url()?>/painel/receitas/modelo.php">
      <input type="hidden" name="cpf" value="<?=$_SESSION['medico_cpf']?>">
      <h5>Modelo</h5>
      <div class="form-row">
        <div class="form-group col-md-12">
          <label for="descricao">Descrição*</label>
          <input type="text" class="form-control" placeholder="Descrição" name="descricao" id="descricao" required>
        </div>
      </div> 
      <h5>Medicamentos</h5>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="paciente-medicamento">Medicamento</label>
          <select class="form-control select2-form-search" id="paciente-medicamento" data-search-url="<?=base_url()?>/rest/medicamentos.php">
          </select>
        </div>
        <div class="form-group col-md-2">
          <label for="paciente-quantidade">Quantidade</label>
          <input type="text" class="form-control" placeholder="Quantidade" id="paciente-quantidade" aria-describedby="paciente-quantidade-ajuda">
          <small id="paciente-quantidade-ajuda" class="form-text text-muted">Exemplo: 1 cx, 1 amp, 10 comp.</small>
        </div>
        <div class="form-group col-md-5">
          <label for="paciente-orientacoes">Orientações</label>
          <input type="text" class="form-control" placeholder="Orientações" id="paciente-orientacoes">
        </div>
        <div class="form-group col-md-1">
          <button id="btn-adicionar-receita-medicamento" class="btn btn-outline-success btn-block mt-1" data-toggle="tooltip" data-placement="top" title="Adicionar Medicamento"><i class="fas fa-plus"></i></button>
        </div>
      </div>          
      <div class="form-row">
        <table id="table-receita-medicamentos" class="table table-borderless table-responsive-md">
          <thead>
            <tr>
              <th class="d-none d-sm-block text-center">#</th>
              <th>Medicamento</th>
              <th class="text-center">Quantidade</th>
              <th>Orientações</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr id="receita-nenhum-medicamento">
              <td colspan="5" class="text-center">Nenhum medicamento informado</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="float-right">
        <button type="submit" class="btn btn-primary button__alcancy"><i class="fas fa-check"></i> Salvar Modelo</button>
      </div>
      <div class="clearfix"></div>
    </form>  
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
