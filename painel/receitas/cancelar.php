<?php 
  require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php');
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();
  
    $conexao->beginTransaction();
    try {
      $receitaUpdate = $conexao->prepare('UPDATE receitas SET ativo = 0 WHERE id = :id');
      $receitaUpdate->bindParam(':id', $_POST['id']);
      $receitaUpdate->execute();
  
      $conexao->commit();
  
      echo json_encode(Array('status' => 'OK'));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }