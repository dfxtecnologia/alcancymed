<?php require_once(dirname(dirname(__DIR__)).'/sys/verifica_acesso_medico.php') ?>
<?php require_once(dirname(dirname(__DIR__)).'/header/index.php') ?>
<?php $conexao = Conexao::getInstance(); ?>
  <div class="header__dashboard">
    <img src="<?=base_url()?>/assets/images/header__receitas.png" alt="">
    <h4>Receitas - Emitir</h4>
  </div>
  <div class="dashboard__wrapper">
    <form class="form__alcancy" action="<?=base_url()?>/painel/receitas/efetivar_emissao.php" method="POST" enctype="multipart/form-data" data-redirect="<?=base_url()?>/painel/receitas/consultar.php">
      <input type="hidden" name="cpf" value="<?=$_SESSION['medico_cpf']?>">
      <?php if (!showConsultorioSelect(Conexao::getInstance(), $_SESSION['medico_cpf'])) { ?>
        <input type="hidden" name="consultorio_medico" value="<?=getConsultorioID(Conexao::getInstance(), $_SESSION['medico_cpf'])?>">
      <?php } else {         
        $query = ' SELECT consultorios.*,cidades.nome as cidade, consultorio_medicos.id as id_consultorio '.
                  '  FROM consultorios '.
                  '  INNER JOIN consultorio_medicos ON (consultorios.id = consultorio_medicos.consultorio_id) '.
                  '  INNER JOIN cidades ON (consultorios.cidade_id = cidades.id) '.
                  '  WHERE consultorios.ativo = 1 '.
                  '    AND consultorio_medicos.medico_id = :medico_id '.
                  '  ORDER BY consultorios.descricao';
        $resultset = $conexao->prepare( $query );
        $resultset->bindParam(':medico_id', $_SESSION['medico_id']);
        $resultset->execute();
      ?>
        <h5>Local do Atendimento</h5>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="atestado-tipo">Local do Atendimento</label>
            <select class="form-control select2-basic" name="consultorio_medico" id="consultorio_medico" required autofocus>
              <option></option>
              <?php while ($consultorio = $resultset->fetch(PDO::FETCH_OBJ)) { ?>
                <option value="<?=$consultorio->id_consultorio?>" <?=($_SESSION['local_atendimento'] == $consultorio->id_consultorio ? 'selected' : '')?>><?=$consultorio->descricao?> - <?=$consultorio->logradouro?> - <?=$consultorio->cidade?></option>
              <?php } ?>
            </select>
          </div>
        </div> 
      <?php } ?>      
      <h5>Paciente</h5>
      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="paciente-cpf">CPF do Paciente*</label>
          <input type="text" class="form-control cpf-mask" placeholder="CPF do Paciente" name="cpf" id="paciente-cpf" data-search-url="<?=base_url()?>/rest/pacientes.php" required autofocus>
          <div class="valid-feedback">
            CPF válido!
          </div>
          <div class="invalid-feedback">
            CPF inválido!!
          </div>
        </div>
        <div class="form-group col-md-10">
          <label for="paciente-nome">Nome do Paciente*</label>
          <input type="text" class="form-control" placeholder="Nome do Paciente" name="nome" id="paciente-nome" required>
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="paciente-celular">Telefone Celular</label>
          <input type="text" class="form-control telefone-mask" placeholder="Celular" name="celular" id="paciente-celular">
        </div>
        <div class="form-group col-md-10">
          <label for="paciente-email">e-mail</label>
          <input type="text" class="form-control" placeholder="e-mail" name="email" id="paciente-email">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="paciente-cep">CEP</label>
          <input type="text" class="form-control busca-cep cep-mask" placeholder="CEP" name="cep" id="paciente-cep">
        </div>
        <div class="form-group col-md-6">
          <label for="paciente-endereco">Endereço</label>
          <input type="text" class="form-control busca-cep-rua" placeholder="Endereço" name="endereco" id="paciente-endereco">
        </div>
        <div class="form-group col-md-2">
          <label for="paciente-numero">Número</label>
          <input type="text" class="form-control" placeholder="Número" name="numero" id="paciente-numero">
        </div>        
        <div class="form-group col-md-2">
          <label for="paciente-bairro">Bairro</label>
          <input type="text" class="form-control busca-cep-bairro" placeholder="Bairro" name="bairro" id="paciente-bairro">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="paciente-cidade">Cidade</label>
          <select class="form-control select2-basic-search busca-cep-cidade" name="cidade" id="cidade" data-search-url="<?=base_url()?>/rest/cidades.php">
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="paciente-estado">Estado</label>
          <select class="form-control select2-basic busca-cep-estado" name="estado" id="paciente-estado">
            <option></option>
            <option value="AC">Acre</option>
            <option value="AL">Alagoas</option>
            <option value="AP">Amapá</option>
            <option value="AM">Amazonas</option>
            <option value="BA">Bahia</option>
            <option value="CE">Ceará</option>
            <option value="DF">Distrito Federal</option>
            <option value="ES">Espírito Santo</option>
            <option value="GO">Goiás</option>
            <option value="MA">Maranhão</option>
            <option value="MT">Mato Grosso</option>
            <option value="MS">Mato Grosso do Sul</option>
            <option value="MG">Minas Gerais</option>
            <option value="PA">Pará</option>
            <option value="PB">Paraíba</option>
            <option value="PR">Paraná</option>
            <option value="PE">Pernambuco</option>
            <option value="PI">Piauí</option>
            <option value="RJ">Rio de Janeiro</option>
            <option value="RN">Rio Grande do Norte</option>
            <option value="RS">Rio Grande do Sul</option>
            <option value="RO">Rondônia</option>
            <option value="RR">Roraima</option>
            <option value="SC">Santa Catarina</option>
            <option value="SP">São Paulo</option>
            <option value="SE">Sergipe</option>
            <option value="TO">Tocantins</option>
            <option value="ES">Estrangeiro</option>
          </select>
        </div>
      </div>
      <?php 
        $query = ' SELECT receitas.* '.
                  '  FROM receitas '.
                  '  WHERE receitas.medico_id = :medico_id '.
                  '    AND receitas.modelo    = 1 '.
                  '  ORDER BY receitas.descricao ASC';
        $resultset = $conexao->prepare( $query );
        $resultset->bindParam(':medico_id', $_SESSION['medico_id']);
        $resultset->execute();

        if ( $resultset->rowCount() > 0 ) {
      ?>
        <h5>Modelo de Receita</h5>
        <div class="form-row">
          <div class="form-group col-md-12">
            <select class="form-control select2-basic" name="modelo_receita" id="modelo-receita"  data-search-url="<?=base_url()?>/rest/receitas.php">
              <option></option>
              <?php while ($modelo = $resultset->fetch(PDO::FETCH_OBJ)) { ?>
                <option value="<?=$modelo->id?>"><?=$modelo->descricao?></option>
              <?php } ?>
            </select>
          </div>
        </div>
      <?php } ?>      
      <h5>Medicamentos</h5>
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="paciente-medicamento">Medicamento</label>
          <select class="form-control select2-form-search" id="paciente-medicamento" data-search-url="<?=base_url()?>/rest/medicamentos.php">
          </select>
        </div>
        <div class="form-group col-md-2">
          <label for="paciente-quantidade">Quantidade</label>
          <input type="text" class="form-control" placeholder="Quantidade" id="paciente-quantidade" aria-describedby="paciente-quantidade-ajuda">
          <small id="paciente-quantidade-ajuda" class="form-text text-muted">Exemplo: 1 cx, 1 amp, 10 comp.</small>
        </div>
        <div class="form-group col-md-5">
          <label for="paciente-orientacoes">Orientações</label>
          <input type="text" class="form-control" placeholder="Orientações" id="paciente-orientacoes">
        </div>
        <div class="form-group col-md-1">
          <button id="btn-adicionar-receita-medicamento" class="btn btn-outline-success btn-block mt-1" data-toggle="tooltip" data-placement="top" title="Adicionar Medicamento"><i class="fas fa-plus"></i></button>
        </div>
      </div>          
      <div class="form-row">
        <table id="table-receita-medicamentos" class="table table-borderless table-responsive-md">
          <thead>
            <tr>
              <th class="d-none d-sm-block text-center">#</th>
              <th>Medicamento</th>
              <th class="text-center">Quantidade</th>
              <th>Orientações</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr id="receita-nenhum-medicamento">
              <td colspan="5" class="text-center">Nenhum medicamento informado</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="float-right">
        <button type="submit" class="btn btn-primary button__alcancy"><i class="fas fa-check"></i> Emitir Receita</button>
      </div>
      <div class="clearfix"></div>
    </form>  
  </div>
<?php require_once(dirname(dirname(__DIR__)).'/footer/index.php') ?>
