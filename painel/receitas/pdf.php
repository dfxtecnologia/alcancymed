<?php
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/pdf/alcancy_pdf.php');
  $conexao = Conexao::getInstance();
    
  $tipo_documento = isSet($_GET['tipo']) ? ($_GET['tipo'] == '' ? 'S' : $_GET['tipo']) : 'S';
  $conexao->beginTransaction();
  try {
    $atualizaMedico = $conexao->prepare('UPDATE receitas set tipo = :tipo WHERE chave = :chave');
    
    $atualizaMedico->bindParam(':tipo', $tipo_documento);
    $atualizaMedico->bindParam(':chave', $_GET['chave']);
    $atualizaMedico->execute();

    $conexao->commit();
  }catch (PDOException $e) {
    $conexao->rollBack();    
  }

  $query = ' SELECT receitas.*,'.
           '        pacientes.cpf paciente_cpf, '.
           '        pacientes.nome paciente_nome, '.
           '        pacientes.cep paciente_cep, '.
           '        pacientes.endereco paciente_endereco, '.
           '        pacientes.numero paciente_numero, '.
           '        pacientes.bairro paciente_bairro, '.
           '        pacientes.estado paciente_estado, '.
           '        cidade_paciente.nome paciente_cidade, '.
           '        medicos.crm medico_crm, '.
           '        medicos.nome medico_nome, '.
           '        medicos.cpf medico_cpf, '.
           '        medicos.email medico_email, '.
           '        medicos.file_assinatura_digital medico_assinatura_digital, '.
           '        consultorios.file_logo consultorio_logo, '.           
           '        consultorios.descricao consultorio_descricao, '.
           '        consultorios.logradouro consultorio_logradouro, '.
           '        consultorios.numero consultorio_numero, '.
           '        consultorios.telefone consultorio_telefone, '.
           '        consultorios.bairro consultorio_bairro, '.
           '        consultorios.cep consultorio_cep, '.
           '        cidades.nome consultorio_cidade, '.
           '        consultorios.estado consultorio_estado '.
           '  FROM receitas '.
           '  INNER JOIN pacientes ON (pacientes.id = receitas.paciente_id) '.
           '   LEFT JOIN cidades cidade_paciente ON (cidade_paciente.id = pacientes.cidade_id) '.
           '  INNER JOIN consultorio_medicos ON (consultorio_medicos.id = receitas.consultorio_medico_id) '.
           '  INNER JOIN medicos ON (medicos.id = consultorio_medicos.medico_id) '.
           '  INNER JOIN consultorios ON (consultorios.id = consultorio_medicos.consultorio_id) '.
           '   LEFT JOIN cidades ON (cidades.id = consultorios.cidade_id) '.
           '  WHERE receitas.chave = :chave '.
           '  ORDER BY receitas.data DESC ';

  $resultset = $conexao->prepare( $query );
  $resultset->bindParam(':chave', $_GET['chave']);
  $resultset->execute();

  $receita = $resultset->fetch(PDO::FETCH_OBJ);
    
  $query = ' SELECT * , concat(nome_comercial," (",apresentacao,")") as nome_apresentacao'.
           '   FROM receita_medicamentos '.
           '  INNER JOIN receitas ON (receitas.id = receita_medicamentos.receitas_id) '.
           '  INNER JOIN medicamentos ON (medicamentos.id = receita_medicamentos.medicamentos_id) '.
           '  WHERE receitas.chave =  :chave '.
           '  ORDER BY medicamentos.nome_comercial DESC ';
  
  $resultsetMedicamentos = $conexao->prepare( $query );
  $resultsetMedicamentos->bindParam(':chave', $receita->chave);
  $resultsetMedicamentos->execute();

  $pdf = new PDF('P', 'mm', 'A4', $receita);
  $pdf->AliasNbPages();
  $pdf->AddPage();
  $pdf->SetFont('Arial', '', 8);

  $pdf->Text(10, 70, 'Para:');
  $pdf->SetFont('Arial', 'B', 8);
  $pdf->Text(10, 75, utf8_decode($receita->paciente_cpf));
  $pdf->Text(35, 75, utf8_decode($receita->paciente_nome));
  $pdf->SetY(76);
  $pdf->SetX(9);
  $pdf->Write(5, utf8_decode($receita->paciente_endereco.', '. $receita->paciente_numero .' '. $receita->paciente_bairro .'  '. $receita->paciente_cidade.'/'.$receita->paciente_estado));

  $pdf->SetFont('Arial', '', 8);
  $linha = 95;
  while($medicamento = $resultsetMedicamentos->fetch(PDO::FETCH_OBJ)) { 
    $pdf->Text(10, $linha, utf8_decode($medicamento->nome_apresentacao));
    for ($i = 60; $i <= 160; $i += 3) {$pdf->Text($i, $linha, '.');}
    $pdf->Text(170, $linha, utf8_decode($medicamento->quantidade));
    $linha += 5;
    $pdf->Text(10, $linha, utf8_decode($medicamento->forma_uso));
    $linha += 10;
  }

  if (isSet($_GET['_'])) {
    if ($receita->ativo) {
      $pdf->SetFont('Arial','B',40);
      $pdf->SetTextColor(255,192,203);
      $pdf->RotatedText(30,190, utf8_decode('DOCUMENTO SEM VALIDADE'),45);
      $pdf->RotatedText(55,190, utf8_decode('APENAS CONFERÊNCIA'),45);
      $pdf->SetFont('Arial','',8);
      $pdf->SetTextColor(0,0,0);
    }
  }

  $pdf->Output('I', $receita->chave.'.pdf');