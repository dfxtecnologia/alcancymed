<?php 
  session_start();
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $conexao->beginTransaction();
    try {
      $receita = $conexao->prepare('INSERT INTO receitas (descricao, medico_id, modelo) VALUES (:descricao, :medico_id, 1)');
      $receita->bindParam(':descricao', $_POST['descricao']);
      $receita->bindParam(':medico_id', $_SESSION['medico_id']);
      $receita->execute();

      $receita_id = $conexao->lastInsertId();

      for($i = 0; $i < count($_POST['medicamento_id']); $i++ ) {
        $receita_medicamento = $conexao->prepare('INSERT INTO receita_medicamentos (receitas_id, medicamentos_id, forma_uso, quantidade) VALUES (:receitas_id, :medicamentos_id, :forma_uso, :quantidade)');
        $receita_medicamento->bindParam(':receitas_id', $receita_id);
        $receita_medicamento->bindParam(':medicamentos_id', $_POST['medicamento_id'][$i]);
        $receita_medicamento->bindParam(':forma_uso', $_POST['forma_uso'][$i]);
        $receita_medicamento->bindParam(':quantidade', $_POST['quantidade'][$i]);
        $receita_medicamento->execute();
      }

      $conexao->commit();
      echo json_encode(Array('status' => 'OK'));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }