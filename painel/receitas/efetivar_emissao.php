<?php 
  session_start();
  require_once(dirname(dirname(__DIR__)).'/sys/conexao.php');
  require_once(dirname(dirname(__DIR__)).'/sys/functions.php');

  if ($_POST) {
    $conexao = Conexao::getInstance();

    $conexao->beginTransaction();
    try {
      $valida_cpf = validaCPF($_POST['cpf']);
      if ($valida_cpf == false) {
        echo json_encode(Array('status' => 'FAIL', 'message' => 'CPF inválido.'));
        exit(); 
      }

      $pacienteQry = $conexao->prepare('SELECT * FROM pacientes WHERE cpf = :cpf');
      $pacienteQry->bindParam(':cpf', $_POST['cpf']);
      $pacienteQry->execute();
    
      $paciente = $pacienteQry->fetch(PDO::FETCH_OBJ);

      if ($paciente) {
        $pacienteUpdate = $conexao->prepare('UPDATE pacientes SET nome = :nome, cep = :cep, endereco = :endereco, numero = :numero, bairro = :bairro, cidade_id = :cidade, estado = :estado, celular = :celular, email = :email WHERE id = :id');
        $pacienteUpdate->bindParam(':nome', $_POST['nome']);
        $pacienteUpdate->bindParam(':cep', $_POST['cep']);
        $pacienteUpdate->bindParam(':endereco', $_POST['endereco']);
        $pacienteUpdate->bindParam(':numero', $_POST['numero']);
        $pacienteUpdate->bindParam(':bairro', $_POST['bairro']);
        $pacienteUpdate->bindParam(':cidade', $_POST['cidade']);
        $pacienteUpdate->bindParam(':estado', $_POST['estado']);
        $pacienteUpdate->bindParam(':celular', $_POST['celular']);
        $pacienteUpdate->bindParam(':email', $_POST['email']);
        $pacienteUpdate->bindParam(':id', $paciente->id);
        $pacienteUpdate->execute();
      } else {
        $pacienteUpdate = $conexao->prepare('INSERT INTO pacientes (nome, cpf, cep, endereco, numero, bairro, cidade_id, estado, celular, email) VALUES (:nome, :cpf, :cep, :endereco, :numero, :bairro, :cidade, :estado, :celular, :email)');
        $pacienteUpdate->bindParam(':nome', $_POST['nome']);
        $pacienteUpdate->bindParam(':cpf', $_POST['cpf']);
        $pacienteUpdate->bindParam(':cep', $_POST['cep']);
        $pacienteUpdate->bindParam(':endereco', $_POST['endereco']);
        $pacienteUpdate->bindParam(':numero', $_POST['numero']);
        $pacienteUpdate->bindParam(':bairro', $_POST['bairro']);
        $pacienteUpdate->bindParam(':cidade', $_POST['cidade']);
        $pacienteUpdate->bindParam(':estado', $_POST['estado']);
        $pacienteUpdate->bindParam(':celular', $_POST['celular']);
        $pacienteUpdate->bindParam(':email', $_POST['email']);        
        $pacienteUpdate->execute();

        $pacienteQry = $conexao->prepare('SELECT * FROM pacientes WHERE cpf = :cpf');
        $pacienteQry->bindParam(':cpf', $_POST['cpf']);
        $pacienteQry->execute();
      
        $paciente = $pacienteQry->fetch(PDO::FETCH_OBJ);
      }

      $chave_receita = create_guid();
      $receita = $conexao->prepare('INSERT INTO receitas (data, paciente_id, chave, consultorio_medico_id) VALUES (:data, :paciente_id, :chave, :consultorio_medico_id)');
      $receita->bindValue(':data', date('Y-m-d H:i:s'));
      $receita->bindParam(':paciente_id', $paciente->id);
      $receita->bindParam(':chave', $chave_receita);
      $receita->bindParam(':consultorio_medico_id', $_POST['consultorio_medico']);
      $receita->execute();

      $receita_id = $conexao->lastInsertId();

      for($i = 0; $i < count($_POST['medicamento_id']); $i++ ) {
        $receita_medicamento = $conexao->prepare('INSERT INTO receita_medicamentos (receitas_id, medicamentos_id, forma_uso, quantidade) VALUES (:receitas_id, :medicamentos_id, :forma_uso, :quantidade)');
        $receita_medicamento->bindParam(':receitas_id', $receita_id);
        $receita_medicamento->bindParam(':medicamentos_id', $_POST['medicamento_id'][$i]);
        $receita_medicamento->bindParam(':forma_uso', $_POST['forma_uso'][$i]);
        $receita_medicamento->bindParam(':quantidade', $_POST['quantidade'][$i]);
        $receita_medicamento->execute();
      }

      $conexao->commit();
      $_SESSION['local_atendimento'] = $_POST['consultorio_medico'];
      echo json_encode(Array('status' => 'OK'));
    }catch (PDOException $e) {
      $conexao->rollBack();    
      echo json_encode(Array('status' => 'FAIL', 'message' => $e->getMessage()));
    }
  }